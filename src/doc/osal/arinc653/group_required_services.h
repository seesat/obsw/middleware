/**
 * \defgroup osal_arinc653_required_services Required Services
 * \{   
 *      \brief This section contains types and specifications related to
 *             the required services of ARINC 653.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      \ingroup osal_arinc653
 * \}
 */