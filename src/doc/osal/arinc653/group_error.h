/**
 * \defgroup osal_arinc653_error Health Monitoring
 * \{   
 *      \brief This section contains types and specifications related to
 *             health monitoring.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      The Health Monitor (HM) is invoked by an application calling the
 *      \ref RAISE_APPLICATION_ERROR service or by the O/S or hardware
 *      detecting an error. The recovery action is dependent on the error
 *      level. The recovery actions for each module and each partition
 *      level error are specified in the HM tables.
 *      The recovery actions for process level errors are defined by the
 *      application programmer in a special error handler process.
 *
 *      \ingroup osal_arinc653_required_services
 * \}
 */