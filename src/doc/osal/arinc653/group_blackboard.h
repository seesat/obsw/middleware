/**
 * \defgroup osal_arinc653_blackboard Blackboard
 * \{   
 *      \brief This section contains types and specifications related to
 *             blackboard.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      A blackboard is a communication object used by processes of a same
 *      partition to send or receive messages. A blackboard does not use
 *      message queues. Each new occurrence of a message overwrites any other.
 *      The blackboard message size may be variable, but the maximum size is
 *      defined at blackboard creation.
 *
 *      A blackboard must be created during the partition initialization phase
 *      before it can be used. The memory size given in the configuration table
 *      should include the memory size necessary to manage all the blackboards
 *      of a partition. A name is given at blackboard creation. This name is
 *      local to the partition and is not an attribute of the partition
 *      configuration table. The creation of blackboards (e.g., names used,
 *      number of blackboards) for one partition has no impact on the creation
 *      of blackboards for other partitions.             
 *
 *      \ingroup osal_arinc653_intrapartition_communication
 * \}
 */