/**
 * \defgroup osal_arinc653_time Time Management
 * \{
 *       \brief This section contains types and specifications related to time
 *              management.
 *
 *       \author Aeronautical Radio Incorporated (ARINC)
 *
 *       Processes that are required to execute at a particular frequency are
 *       known as periodic processes. Similarly, those processes that execute
 *       only after a particular event are known as aperiodic or event-driven
 *       processes. The Time Management services provide a means to control
 *       periodic and aperiodic processes.
 *
 *       For periodic processes, each process within a partition is able to
 *       specify a maximum elapsed time for executing its processing cycle.
 *       This time capacity is used to set a processing deadline time. The
 *       deadline time is then periodically evaluated by the operating system
 *       to determine whether the process is satisfactorily completing its
 *       processing within the allotted time. It is expected that at the end of
 *       each processing cycle a process will call the \ref PERIODIC_WAIT
 *       service to get a new deadline. The new deadline is calculated from the
 *       next periodic release point for that process.
 *
 *       For all processes, the \ref TIMED_WAIT service allows the process to
 *       suspend itself for a minimum amount of elapsed time. After the wait
 *       time has elapsed, the process becomes available to be scheduled.
 *
 *       The \ref REPLENISH service allows a process to postpone its current
 *       deadline by the amount of time which has already passed.
 *
 *       \ingroup osal_arinc653_required_services
 * \}
 */