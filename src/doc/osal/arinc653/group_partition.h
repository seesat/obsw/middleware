/**
 * \defgroup osal_arinc653_partition Partition Management
 * \{
 *       \brief This section contains types and specifications related to
 *              partition management.
 *
 *       \author Aeronautical Radio Incorporated (ARINC)
 *
 *       \ingroup osal_arinc653_required_services
 * \}
 */