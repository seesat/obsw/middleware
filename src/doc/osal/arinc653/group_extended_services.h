/**
 * \defgroup osal_arinc653_extended_services Extended Services
 * \{   
 *      \brief This section contains types and specifications related to
 *             the extended services of ARINC 653.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      \ingroup osal_arinc653
 * \}
 */