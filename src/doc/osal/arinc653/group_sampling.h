/**
 * \defgroup osal_arinc653_sampling Sampling Port
 * \{   
 *      \brief This section contains types and specifications related to
 *             sampling port.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      A sampling port is a communication object allowing a partition to
 *      access a channel of communication configured to operate in sampling
 *      mode. In sampling mode, each new occurrence of a message overwrites the
 *      previous one. Messages may have a variable length. A refresh period
 *      attribute applies to receive ports. A validity output parameter
 *      indicates whether the age of the read message is consistent with the
 *      required refresh period attribute of the port. The REFRESH_PERIOD
 *      attribute indicates the maximum acceptable age of a valid message, from
 *      the time it was received in the port.
 *
 *      A sampling port must be created during the partition initialization
 *      phase before it can be used.
 *
 *      \ingroup osal_arinc653_interpartition_communication
 * \}
 */