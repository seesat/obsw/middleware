/**
 * \defgroup osal_arinc653_interpartition_communication Interpartition
 *                                                      Communication
 * \{   
 *      \brief This section contains types and specifications related to
 *             interpartition communication.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      The interpartition communication services are divided into two groups,
 *      sampling port services and queuing port services.
 *
 *      \ingroup osal_arinc653_required_services
 * \}
 */