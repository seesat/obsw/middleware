/**
 * \defgroup osal_arinc653_buffer Buffer
 * \{   
 *      \brief This section contains types and specifications related to
 *             buffer.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      A buffer is a communication object used by processes of a same
 *      partition to send or receive messages. Multiple messages may be queued
 *      in a buffer, but the maximum number of messages supported is defined at
 *      buffer creation. Within a buffer, the messages are queued in FIFO
 *      order. The buffer message size may be variable, but the maximum size is
 *      defined at buffer creation.
 *
 *      A buffer must be created during the partition initialization phase
 *      before it can be used. A name is given at buffer creation. This name is
 *      local to the partition and is not an attribute of the partition
 *      configuration table. The creation of buffers (e.g., names used, number
 *      of buffers) for one partition has no impact on the creation of buffers
 *      for other partitions.             
 *
 *      \ingroup osal_arinc653_intrapartition_communication
 * \}
 */