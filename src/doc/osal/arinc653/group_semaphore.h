/**
 * \defgroup osal_arinc653_semaphore Semaphore
 * \{   
 *      \brief This section contains types and specifications related to
 *             semaphore.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      Counting semaphore services are supported. A counting semaphore is a
 *      synchronization object commonly used to provide access to partition
 *      resources.
 *
 *      A semaphore must be created during the partition’s initialization phase
 *      before it can be used. A name is given at semaphore creation. This name
 *      is local to the partition and is not an attribute of the partition
 *      configuration table. The creation of semaphores (e.g., names used,
 *      number of semaphores) for one partition has no impact on the creation
 *      of semaphores for other partitions.
 *
 *      \ingroup osal_arinc653_intrapartition_communication
 * \}
 */