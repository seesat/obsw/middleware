/**
 * \defgroup osal_arinc653_types Constant And Type Definitions
 * \{   
 *      \brief This section contains global constant and type definitions.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      \ingroup osal_arinc653_required_services
 * \}
 */