/**
 * \defgroup osal_arinc653_queuing Queuing Port
 * \{   
 *      \brief This section contains types and specifications related to
 *             queuing port.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      A queuing port is a communication object allowing a partition to access
 *      a channel of communication configured to operate in queuing mode.
 *      Messages are stored in FIFO order. Messages may have a variable length.
 *      In queuing mode, each new instance of a message cannot overwrite the
 *      previous one stored in the send or receive FIFO. However, if the
 *      receiving FIFO is full, new messages may be discarded. An appropriate
 *      value of RETURN_CODE (i.e., INVALID_CONFIG) will be returned on the
 *      next invocation of the \ref RECEIVE_QUEUING_MESSAGE service.
 *
 *      A send/respond protocol can be implemented by the applications to guard
 *      against communication failures and to ensure flow control between
 *      source and destination. The destination partition determines when it
 *      will read its messages.
 *
 *      A queuing port must be created during the partition initialization
 *      phase before it can be used.
 *
 *      \ingroup osal_arinc653_interpartition_communication
 * \}
 */