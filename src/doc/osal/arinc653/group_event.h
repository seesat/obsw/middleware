/**
 * \defgroup osal_arinc653_event Event
 * \{   
 *      \brief This section contains types and specifications related to
 *             event.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      An event is a synchronization object used to notify the occurrence of a
 *      condition to processes that may be waiting for condition to occur.
 *
 *      An event must be created during the partition’s initialization phase
 *      before it can be used. A name is given at event creation. This name is
 *      local to the partition and is not an attribute of the partition
 *      configuration table. The creation of events (e.g., names used, number
 *      of events) for one partition has no impact on the creation of events
 *      for other partitions.
 *
 *      \ingroup osal_arinc653_intrapartition_communication
 * \}
 */