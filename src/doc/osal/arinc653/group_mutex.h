/**
 * \defgroup osal_arinc653_mutex Mutex
 * \{   
 *      \brief This section contains types and specifications related to
 *             mutex.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      Mutex services are supported. A mutex is a synchronization object
 *      commonly used to control access to partition resources. Only one
 *      process at a time can own a specific mutex. Each mutex includes a
 *      priority value that a process’s current priority is raised to when the
 *      mutex is acquired. When a process releases a mutex, the priority of
 *      the process is restored to the process priority value it had when it
 *      acquired the mutex.
 *
 *      A mutex must be created during the partition initialization phase
 *      before it can be used. A name is given at mutex creation. This name is
 *      local to the partition and is not an attribute of the partition
 *      configuration table. The creation of mutexes (e.g., names used, number
 *      of mutexes) for one partition has no impact on the creation of mutexes
 *      for other partitions.
 *
 *      \ingroup osal_arinc653_intrapartition_communication
 * \}
 */