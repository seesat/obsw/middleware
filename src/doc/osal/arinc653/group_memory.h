/**
 * \defgroup osal_arinc653_memory Memory Management
 * \{
 *       \brief This section contains types and specifications related to
 *              memory management.
 *
 *       \author Aeronautical Radio Incorporated (ARINC)
 *
 *       There are no memory management services, because partitions and their
 *       associated memory spaces are defined as part of system configuration
 *       activities. It is expected that the adequacy of the allocated memory
 *       space is confirmed as part of system build activities or at run-time
 *       before the first application process becomes running (e.g., memory
 *       resources should not be exhausted before transitioning to NORMAL).
 *
 *       All processes of a partition will have access to the same memory
 *       space. This includes processes associated with the same partition that
 *       are running concurrently on different processor cores.
 *
 *       \ingroup osal_arinc653_required_services
 * \}
 */