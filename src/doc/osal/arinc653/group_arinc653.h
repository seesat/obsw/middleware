/**
 * \defgroup osal_arinc653 ARINC 653
 * \{   
 *      \brief This section contains types and specifications of ARINC 653.
 *      
 *      \author Aeronautical Radio Incorporated (ARINC)
 *      
 *      \ingroup interface_osal
 * \}
 */