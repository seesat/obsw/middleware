/**
 * \defgroup osal_arinc653_process Process Management
 * \{   
 *      \brief This section contains types and specifications related to
 *             process management.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      The scope of a process management service is restricted to a partition.
 *
 *      \ingroup osal_arinc653_required_services
 * \}
 */