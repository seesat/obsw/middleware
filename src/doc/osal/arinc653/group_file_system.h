/**
 * \defgroup osal_arinc653_file_system File System
 * \{   
 *      \brief This section specifies the types, file naming conventions and
 *             service requests related to the ARINC 653 File System.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      \ingroup osal_arinc653_extended_services
 * \}
 */