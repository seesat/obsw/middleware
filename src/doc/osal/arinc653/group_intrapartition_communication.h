/**
 * \defgroup osal_arinc653_intrapartition_communication Intrapartition
 *                                                      Communication
 * \{   
 *      \brief This section contains types and specifications related to
 *             intrapartition communication.
 *
 *      \author Aeronautical Radio Incorporated (ARINC)
 *
 *      Three communication mechanisms are available for intrapartition
 *      communication. The first mechanism allows inter-process communication
 *      and synchronization via partition-defined buffers and blackboards. The
 *      second allows inter-process synchronization via partition-defined
 *      counting semaphores and events. The third allows inter-process mutual
 *      exclusion via partition-defined mutexes.
 *
 *      \ingroup osal_arinc653_required_services
 * \}
 */