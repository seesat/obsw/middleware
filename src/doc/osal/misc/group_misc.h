/**
 * \defgroup osal_misc Miscellaneous (MISC)
 * \{   
 *      \brief This section contains types and specifications for Miscellaneous
 *             functions.
 * 
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 * 
 *      \ingroup interface_osal
 * \}
 */