/**
 * \defgroup osal_misc_byte_order Byte Order (Endianness)
 * \{
 *       \brief This section contains types and specifications related to
 *              Byte Order conversion.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *
 *       \ingroup osal_misc
 * \}
 */