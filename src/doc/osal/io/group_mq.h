/**
 * \defgroup osal_io_mq Message Queue
 * \{
 *       \brief This section contains types and specifications related to
 *              Message Queue IO.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *
 *       \ingroup osal_io
 * \}
 */