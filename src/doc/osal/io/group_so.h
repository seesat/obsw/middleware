/**
 * \defgroup osal_io_so Socket
 * \{
 *       \brief This section contains types and specifications related to
 *              Socket IO.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *
 *       \ingroup osal_io
 * \}
 */