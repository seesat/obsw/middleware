/**
 * \defgroup osal_io Input/Output (IO)
 * \{   
 *      \brief This section contains types and specifications for Input/Output
 *             (IO).
 *      
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      The IO API represents access of different IO devices, such as message
 *      queues, sockets, ...
 *      
 *      Therefore, this layer uses an object oriented model to provide access
 *      to different IO devices. The general and common properties are
 *      collected in the base class \ref DEVICE_TYPE. These general properties
 *      of all devices are inherited to the specific sub classes. This is
 *      realized by aggregating the base class to each sub class.
 *      The specific methods are reached with dynamic polymorphism. The
 *      function pointers of each device point to the specific device
 *      implementation specified by the \ref DEVICE_TYPE at creation step.
 *      
 *      The IO device layer provides the following function interface to
 *      implement different IO devices:
 *        - \ref IO_CREATE_FUNCTION "IO_CREATE": Initializes the device
 *                                               structure.
 *        - \ref IO_OPEN_FUNCTION "IO_OPEN": Opens a connection to the device.
 *        - \ref IO_CLOSE_FUNCTION "IO_CLOSE": Closes the connection to the
 *                                             device.
 *        - \ref IO_READ_FUNCTION "IO_READ": Read byte stream from the device.
 *        - \ref IO_WRITE_FUNCTION "IO_WRITE": Write byte stream to the device.
 *        - \ref IO_IOCTL_FUNCTION "IO_IOCTL": Controls device specific
 *                                             properties or functions.
 *
 *      Special settings for creation step can be transmitted with the device
 *      specific settings structure. For each device must exists such a
 *      settings structure, like \ref DEVICE_MQ_SETTING_TYPE, to give a device
 *      a specified name and more information that are needed during device
 *      creation step.
 *      
 *      The following code shows how to create and use a simple message queue
 *      device:
 *      \code{.c}
 *        #include <osal/osal.h>
 *        #include <osal/io/io.h>
 *        #include <osal/io/mq.h>
 *      
 *        DEVICE_MQ_TYPE mq;
 *        DEVICE_MQ_SETTING_TYPE mq_settings = {
 *            .MQ_NAME         = "MESSAGE_QUEUE_1",
 *            .MQ_SIZE         = 4096,
 *            .MQ_MAX_MESSAGES = 256
 *        }  
 *        RETURN_CODE_TYPE ret;
 *        APEX_BYTE buffer[1024];
 *        MESSAGE_SIZE_TYPE recv_bytes;
 *        
 *        // initialize the OSAL layer
 *        INIT_OSAL(&ret);
 *        
 *        // create a new message queue device
 *        CREATE_DEVICE(&mq, MQ, &mq_settings, &ret);
 *          
 *        // open the message queue device
 *        OPEN_DEVICE(&mq, &ret);
 *          
 *        // try to read a message from the device as blocking function call
 *        READ_DEVICE(&mq, BLOCKING, buffer, sizeof(buffer), &recv_bytes, &ret);
 *        // write data as message to the device
 *        WRITE_DEVICE(&mq, NON_BLOCKING, buffer, sizeof(buffer), &ret);
 *          
 *        // close the message queue device after usage
 *        CLOSE_DEVICE(&mq, &ret);
 *      \endcode
 *      
 *      \ingroup interface_osal
 * \}
 */
