/**
 * \defgroup osal_crypt_hash HASH Functions
 * \{
 *       \brief This section contains types and specifications related to HASH
 *              functions.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *
 *       \ingroup osal_crypt
 * \}
 */