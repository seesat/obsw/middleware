/**
 * \defgroup osal_crypt_aes AES Encryption
 * \{
 *       \brief This section contains types and specifications related to AES
 *              encryption.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *       
 *       \author Lawrence Bassham
 *
 *       \ingroup osal_crypt
 * \}
 */