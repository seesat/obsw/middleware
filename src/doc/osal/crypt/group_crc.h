/**
 * \defgroup osal_crypt_crc CRC Calculation
 * \{
 *       \brief This section contains types and specifications related to CRC
 *              calculation.
 *
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 *
 *       \ingroup osal_crypt
 * \}
 */