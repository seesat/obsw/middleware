/**
 * \defgroup osal_crypt Cryptology
 * \{   
 *      \brief This section contains types and specifications for cryptology.
 *      
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      \ingroup interface_osal
 * \}
 */