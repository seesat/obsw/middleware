/**
 * \defgroup mw_types Constant And Type Definitions
 * \{   
 *      \brief This section contains global constant and type definitions.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      \ingroup interface_mw
 * \}
 */