/**
 * \defgroup mw_service_state_vector State Vector
 * \{   
 *      \brief This section contains types and specifications for state vector
 *             services.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      A state vector is an internal state of an application. Applications can
 *      change their states with the \ref CHANGE_STATE wrapper function to
 *      change the state and report the state change by the supervisor app.
 *      
 *      Furthermore, the supervisor app is able to force an internal state
 *      change of an application with the \ref FORCE_CHANGE_STATE. Normal
 *      apps should not use this dangerous function.
 *
 *      \ingroup mw_service
 * \}
 */