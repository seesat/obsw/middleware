/**
 * \defgroup mw_service_logging Logging
 * \{   
 *      \brief This section contains types and specifications for logging
 *             services.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      The logging service is used to send log entries to the logger app.
 *      The logger app save the log entries in log files on a persistent
 *      memory. This service is a mandatory service of the middleware.
 *
 *      \ingroup mw_service
 * \}
 */