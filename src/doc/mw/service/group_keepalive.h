/**
 * \defgroup mw_service_keepalive Keepalive
 * \{   
 *      \brief This section contains types and specifications for keepalive
 *             services.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      The keepalive service implements the health check for the apps. This
 *      service is used by all apps and is a mandatory middleware service.
 *      Apps must send keepvalive packets in a fix defined interval to the
 *      supervisor to show their connectivity. The supervisor app evaluates
 *      the result of these pings and determines the health state of the apps.
 *
 *      \ingroup mw_service
 * \}
 */