/**
 * \defgroup mw_service Services
 * \{   
 *      \brief This section contains types and specifications for services.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      Services in the middleware describe the data structure of the different
 *      messages that are used to communicate between the apps. Each app can
 *      register own services to react for own incoming messages.
 *      
 *      Therefore, each service implementation should contain a
 *      \ref SERIALZE_SERVICE_FUNCTION "serializing" and
 *      \ref DESERIALZE_SERVICE_FUNCTION "dezerializing" function.
 *      These functions process the \ref MESSAGE_TYPE::DATA part of a message
 *      and provide the data rehashed in structures of the programming language
 *      C to the application developers. It is very important to use the
 *      READ_MESSAGE_DATAXX and WRITE_MESSAGE_DATAXX (see \ref mw_com_message
 *      module) functions to preserve the correct byte order.
 *
 *      \ingroup interface_mw
 * \}
 */