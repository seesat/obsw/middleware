/**
 * \defgroup mw_com_processing Processing
 * \{   
 *      \brief This section contains types and specifications for message
 *             processing.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      \ingroup mw_com
 * \}
 */