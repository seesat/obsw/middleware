/**
 * \defgroup mw_com_message Message
 * \{   
 *      \brief This section contains types and specifications for messages
 *             that are used for communication.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      The messages are used to realize the middleware communication between
 *      the different applications. \ref MESSAGE_TYPE defines the common
 *      structure for a communication protocol.
 *      
 *      The message can be \ref SERIALIZE_MESSAGE "serialized" and
 *      \ref DESERIALIZE_MESSAGE "deserialized" from an to a byte stream.
 *      These functions automatically provide the correct byte order of data
 *      fields greater than one byte. If the developer want to access data
 *      fields in the \ref MESSAGE_TYPE::DATA part he or she should use the
 *      READ_MESSAGE_DATAXX and WRITE_MESSAGE_DATAXX functions to preserve the
 *      correct byte order in the data part of the message as well.
 *      
 *      \ingroup mw_com
 * \}
 */