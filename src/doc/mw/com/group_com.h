/**
 * \defgroup mw_com Communication (COM)
 * \{   
 *      \brief This section contains types and specifications for
 *             communication.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      \ingroup interface_mw
 * \}
 */