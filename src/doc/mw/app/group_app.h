/**
 * \defgroup mw_app Application (APP)
 * \{   
 *      \brief This section contains types and specifications for Applications
 *             (APPs).
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *      
 *      An application (APP) represents a component in the middleware software
 *      layer model. An app uses the services and functionality of the
 *      middleware layer to communicate with other apps or access hardware
 *      devices through the \ref mw_periphery "peripheral IO".
 *
 *      \ingroup interface_mw
 * \}
 */