/**
 * \defgroup mw_periphery Peripheral Input/Output (IO)
 * \{   
 *      \brief This section contains types and specifications for peripheral
 *             Input/Output (IO).
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      The Middleware IO Module provides access to hardware devices in an
 *      abstract way. A \a Periphery is in this abstract way a part of an lower
 *      level \ref DEVICE_TYPE "OSAL IO Device". The new higher level Middleware
 *      peripheral IO allows the application developer to avoid raw access on
 *      devices or bus systems. Peripheral IOs are used to access hardware
 *      sensors and actuators.
 *      
 *      The Middleware IO module uses an object oriented model to provide
 *      access to different types of periphery. There is a base class, called
 *      \ref PERIPHERY_TYPE, which contains common properties of all peripheral
 *      IOs. Moreover, this base class contains function pointers for dynamic
 *      polymorphism. The function pointers point after initialization to the
 *      specific implementation of each periphery.
 *      
 *      The Middleware peripheral IO layer provides the following function
 *      interface to implement different peripheral IOs:
 *        - \ref CREATE_PERIPHERY_FUNCTION "CREATE_PERIPHERY": Initializes the
 *                                                             peripheral IO.
 *        - \ref GET_PERIPHERY_VALUE_FUNCTION "GET_PERIPHERY_VALUE": Read a
 *                                                                   value from
 *                                                                   an
 *                                                                   peripheral
 *                                                                   IO.
 *        - \ref SET_PERIPHERY_VALUE_FUNCTION "SET_PERIPHERY_VALUE": Set a
 *                                                                   value of
 *                                                                   an
 *                                                                   Peripheral
 *                                                                   IO
 *      
 *      The \ref GET_PERIPHERY_VALUE "GET" and \ref SET_PERIPHERY_VALUE "SET"
 *      functions provides the application programmer the possibility to access
 *      different values of an peripheral IO. The structure of the complex and
 *      different values are defined in \ref mw_periphery_value. The data
 *      structures provides easy access to special values, e.g. acceleration
 *      values of the physical x axis.
 *      
 *      \ingroup interface_mw
 * \}
 */