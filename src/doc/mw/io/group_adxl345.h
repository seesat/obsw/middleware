/**
 * \defgroup mw_periphery_adxl345 ADXL345 Periphery
 * \{   
 *      \brief This section contains types and specifications for ADXL345
 *             peripheral IO acceleration sensor.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      \ingroup mw_periphery
 * \}
 */