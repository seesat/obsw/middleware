/**
 * \defgroup mw_periphery_value Peripheral Values
 * \{   
 *      \brief This section contains types and specifications for peripheral
 *             values.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      \ingroup mw_periphery
 * \}
 */