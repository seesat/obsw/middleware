/**
 * \defgroup mw_periphery_htu21d HTU21D Periphery
 * \{   
 *      \brief This section contains types and specifications for HTU21D
 *             peripheral IO temperature and humidity sensor.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *
 *      \ingroup mw_periphery
 * \}
 */