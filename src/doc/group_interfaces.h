/**
 * \defgroup interface_osal OSAL Interface
 * \{   
 *      \brief This section contains the specification of the Operating System
 *             Abstraction Layer (OSAL) interface.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 *             
 *      The OSAL Interface is located between the Middleware and the Operating
 *      System (OS). Therefore, the ARINC 653 specification is used to avoid a
 *      redefinition of an avionics real-time interface specification.
 *
 *      The OSAL interface implements all \ref osal_arinc653_required_services
 *      of ARINC 653.
 *
 *      In addition to the \ref osal_arinc653_required_services the
 *      interface uses the \ref osal_arinc653_file_system services from the
 *      \ref osal_arinc653_extended_services of ARINC 653. All other services
 *      and extensions from the \ref osal_arinc653_extended_services of
 *      ARINC 653 are not implemented.
 * \}
 * 
 * \defgroup interface_mw Middleware Interface
 * \{   
 *      \brief This section contains the specification of the Middleware
 *             interface.
 *
 *      \author Manuel Bentele
 *      \author Dominik Schwarzbauer
 *      \author Robin Windey
 * \}
 */