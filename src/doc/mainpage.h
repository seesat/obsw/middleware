/**----------------------------------------------------------------------------
 * \mainpage SeeSat Middleware Documentation
 * \{
 *       \section mainpage_interfaces Interface Documentation
 *
 *       \subsection mainpage_interface_osal OSAL Interface
 *       \ref interface_osal
 *
 *       \subsection mainpage_interface_mw Middleware Interface
 *       \ref interface_mw
 *       
 *       \author Manuel Bentele
 *       \author Dominik Schwarzbauer
 *       \author Robin Windey
 * \}
 *---------------------------------------------------------------------------*/