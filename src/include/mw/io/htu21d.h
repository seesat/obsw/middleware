/**----------------------------------------------------------------------------
 * \file htu21d.h
 * 
 * HTU21D peripheral IO temperature and humidity sensor constant and type
 * definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_periphery_htu21d
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_periphery_htu21d
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/io/periphery.h>

#ifndef MW_PERIPHERY_HTU21D
#define MW_PERIPHERY_HTU21D

/** Specific representation of the HTU21D temperature and humidity sensor */
typedef struct
{
    PERIPHERY_TYPE base;              /**< Instance of the base class */
} PERIPHERY_HTU21D_TYPE;

typedef MW_BYTE I2C_ADDRESS_TYPE;

/** Specific representation of the HTU21D settings */
typedef struct
{
    PERIPHERY_NAME_TYPE HTU21D_NAME;   /**< Name of the HTU21D sensor */
    MW_DEVICE_NAME_TYPE IO_DEVICE_NAME;/**< Name of the used OSAL IO device to
                                        *   communicate with the sensor */
    I2C_ADDRESS_TYPE HTU21D_ADDRESS;   /**< I2C bus address of the sensor */
} PERIPHERY_HTU21D_SETTING_TYPE;

/**
 * \see CREATE_PERIPHERY
 */
extern MW_ERROR_TYPE CREATE_HTU21D_PERIPHERY(
        PERIPHERY_HTU21D_TYPE *PERIPHERY,
        PERIPHERY_IO_TYPE TYPE,
        PERIPHERY_HTU21D_SETTING_TYPE *SETTING);

/**
 * \see GET_PERIPHERY_VALUE
 */
extern MW_ERROR_TYPE GET_HTU21D_PERIPHERY_VALUE(
        PERIPHERY_HTU21D_TYPE *PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

/**
 * \see SET_PERIPHERY_VALUE
 */
extern MW_ERROR_TYPE SET_HTU21D_PERIPHERY_VALUE(
        PERIPHERY_HTU21D_TYPE *PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

#endif

/** \} */
