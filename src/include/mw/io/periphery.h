/**----------------------------------------------------------------------------
 * \file periphery.h
 * 
 * Peripheral IO constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_periphery
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_periphery
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/types.h>
#include <osal/io/io.h>

#ifndef MW_PERIPHERY
#define MW_PERIPHERY

/** Maximum number of value types supported by a periphery */
#define NUM_SUPPORTED_VALUES           10

/** Maximum length of a periphery name */
#define PERIPHERY_MAX_NAME_LENGTH      32

typedef APEX_BYTE PERIPHERY_NAME_TYPE[PERIPHERY_MAX_NAME_LENGTH];

/** Name of the used OSAL IO device to communicate with the periphery */
typedef DEVICE_NAME_TYPE MW_DEVICE_NAME_TYPE;

/** Types of periphery */
typedef enum
{
    SENSOR_HTU21D = 1,       /**< HTU21D temperature and humidity sensor */
    SENSOR_ADXL345 = 2,      /**< ADXL345 acceleration sensor */
    ACTUATOR_PCA9629 = 3,    /**< PCA9629 stepper motor controller */
} PERIPHERY_IO_TYPE;

/** Types of peripheral IO values */
typedef enum
{
    /* air dependend values */
    TEMPERATURE = 1,         /**< Temperature value in degree celsius (°C) */
    RELATIVE_HUMIDITY = 2,   /**< Relative humidity value in percent (%) */
    
    /* kinematic dependend values */
    ACCELERATION = 20,       /**< Acceleration of all three physical axes
                              *   (x, y, z) in metre per second squared
                              *   (m/s^2) */
    ROTATION = 21,           /**< Rotation around all three physical axes
                              *   (x, y, z) in radians per second (rad/s) */
    
    /* field dependend values */
    MAGNETIC_FIELD = 40      /**< Geomagnetic field strength of all three
                              *   physical axes (x, y, z) in micro tesla
                              *   (µT) */
} PERIPHERY_VALUE_TYPE;

/** 
 * Prototype for creating an Peripheral IO.
 * 
 * \see CREATE_PERIPHERY
 */
typedef MW_ERROR_TYPE (*CREATE_PERIPHERY_FUNCTION)(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR SETTING);

/** 
 * Prototype for reading a value from an Peripheral IO.
 * 
 * \see GET_PERIPHERY_VALUE
 */
typedef MW_ERROR_TYPE (*GET_PERIPHERY_VALUE_FUNCTION)(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

/** 
 * Prototype for setting a value of an Peripheral IO.
 * 
 * \see SET_PERIPHERY_VALUE
 */
typedef MW_ERROR_TYPE (*SET_PERIPHERY_VALUE_FUNCTION)(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

typedef PERIPHERY_VALUE_TYPE SUPPORTED_VALUES_TYPE[NUM_SUPPORTED_VALUES];

/** General representation of an peripheral IO */
typedef struct
{
    PERIPHERY_IO_TYPE TYPE;            /**< Type of the periphery */
    PERIPHERY_NAME_TYPE NAME;          /**< Name of the periphery */
    CREATE_PERIPHERY_FUNCTION create;  /**< Create function of the periphery */
    GET_PERIPHERY_VALUE_FUNCTION get;  /**< Get value function of the
                                        *   periphery */
    SET_PERIPHERY_VALUE_FUNCTION set;  /**< Set value function of the
                                        *   periphery */
} PERIPHERY_TYPE;

/**
 * The \a CREATE_PERIPHERY service request creates a new device and initializes
 * the \p PERIPHERY structure with the properties of the specified device
 * \p TYPE. Furthermore, the periphery is opened and accessed through the OSAL
 * IO devices. Periphery specific settings, e.g. bus specific addresses, can
 * be specified with the peripheral specific \p SETTING structure.
 * 
 * \param  [out]                PERIPHERY
 * \param  [in]                 TYPE
 * \param  [in]                 SETTING
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Failed to create periphery
 */
extern MW_ERROR_TYPE CREATE_PERIPHERY(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_IO_TYPE TYPE,
        MW_VOID_PTR SETTING);

/**
 * Read a value from an Peripheral IO. The data type of \p VALUE depends on the
 * \p VALUE_TYPE and the \p PERIPHERY.
 * 
 * \param  [in]                 PERIPHERY
 * \param  [in]                 VALUE_TYPE
 * \param  [out]                VALUE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The specified \p VALUE_TYPE is not supported
 *                              by the \p PERIPHERY
 * \retval INVALID_MODE         The specified \p PERIPHERY is not able to
 *                              perform the get value request
 */
extern MW_ERROR_TYPE GET_PERIPHERY_VALUE(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

/**
 * Set a value of an Peripheral IO. The data type of \p VALUE depends on the
 * \p VALUE_TYPE and the \p PERIPHERY.
 * 
 * \param  [in]                 PERIPHERY
 * \param  [in]                 VALUE_TYPE
 * \param  [in]                 VALUE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The specified \p VALUE_TYPE is not supported
 *                              by the \p PERIPHERY
 * \retval INVALID_MODE         The specified \p PERIPHERY is not able to
 *                              perform the set value request
 */
extern MW_ERROR_TYPE SET_PERIPHERY_VALUE(
        MW_VOID_PTR PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

#endif

/** \} */