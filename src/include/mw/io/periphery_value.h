/**----------------------------------------------------------------------------
 * \file periphery_value.h
 * 
 * Peripheral IO value data constant and type definitions and management
 * services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_periphery_value
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_periphery_value
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/io/periphery.h>
#include <mw/types.h>

#ifndef MW_PERIPHERY_VALUE
#define MW_PERIPHERY_VALUE

/** Representation of temperature value for peripheral IO */
typedef struct
{
    MW_DWORD TEMPERATURE;      /**< Temperature value in degree celsius (°C) */
} TEMPERATURE_VALUE_TYPE;

/** Representation of humidity value for peripheral IO */
typedef struct
{
    MW_BYTE HUMIDITY;          /**< Relative humidity value in percent (%) */
} HUMIDITY_VALUE_TYPE;

/** Representation of acceleration value for peripheral IO */
typedef struct
{
    MW_FLOAT ACCELERATION_X;   /**< Acceleration of the physical x axis in
                                *   metre per second squared (m/s^2) */
    MW_FLOAT ACCELERATION_Y;   /**< Acceleration of the physical y axis in
                                *   metre per second squared (m/s^2) */
    MW_FLOAT ACCELERATION_Z;   /**< Acceleration of the physical z axis in
                                *   metre per second squared (m/s^2) */
} ACCELERATION_VALUE_TYPE;

/** Representation of rotation value for peripheral IO */
typedef struct
{
    MW_FLOAT ROTATION_X;       /**< Rotation around the physical x axis in
                                *   radians per second (rad/s) */
    MW_FLOAT ROTATION_Y;       /**< Rotation around the physical y axis in
                                *   radians per second (rad/s) */
    MW_FLOAT ROTATION_Z;       /**< Rotation around the physical z axis in
                                *   radians per second (rad/s) */
} ROTATION_VALUE_TYPE;

/** Representation of magnetic field value for peripheral IO */
typedef struct
{
    MW_FLOAT MAGNETIC_FIELD_X; /**< Geomagnetic field strength of physical x
                                *   axis in micro tesla (µT) */
    MW_FLOAT MAGNETIC_FIELD_Y; /**< Geomagnetic field strength of physical y
                                *   axis in micro tesla (µT) */
    MW_FLOAT MAGNETIC_FIELD_Z; /**< Geomagnetic field strength of physical z
                                *   axis in micro tesla (µT) */
} MAGNETIC_FIELD_VALUE_TYPE;

#endif

/** \} */