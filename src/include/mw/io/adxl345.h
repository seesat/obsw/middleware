/**----------------------------------------------------------------------------
 * \file adxl345.h
 * 
 * ADXL345 peripheral IO acceleration sensor constant and type definitions and
 * management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_periphery_adxl345
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_periphery_adxl345
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/io/periphery.h>

#ifndef MW_PERIPHERY_ADXL345
#define MW_PERIPHERY_ADXL345

/** Specific representation of the ADXL345 acceleration sensor */
typedef struct
{
    PERIPHERY_TYPE base;              /**< Instance of the base class */
} PERIPHERY_ADXL345_TYPE;

typedef MW_BYTE SPI_ADDRESS_TYPE;

/** Specific representation of the ADXL345 settings */
typedef struct
{
    PERIPHERY_NAME_TYPE ADXL345_NAME;  /**< Name of the ADXL345 sensor */
    MW_DEVICE_NAME_TYPE IO_DEVICE_NAME;/**< Name of the used OSAL IO device to
                                        *   communicate with the sensor */
    SPI_ADDRESS_TYPE ADXL345_ADDRESS;  /**< SPI bus address of the sensor */
} PERIPHERY_ADXL345_SETTING_TYPE;

/**
 * \see CREATE_PERIPHERY
 */
extern MW_ERROR_TYPE CREATE_ADXL345_PERIPHERY(
        PERIPHERY_ADXL345_TYPE *PERIPHERY,
        PERIPHERY_IO_TYPE TYPE);

/**
 * \see GET_PERIPHERY_VALUE
 */
extern MW_ERROR_TYPE GET_ADXL345_PERIPHERY_VALUE(
        PERIPHERY_ADXL345_TYPE *PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

/**
 * \see SET_PERIPHERY_VALUE
 */
extern MW_ERROR_TYPE SET_ADXL345_PERIPHERY_VALUE(
        PERIPHERY_ADXL345_TYPE *PERIPHERY,
        PERIPHERY_VALUE_TYPE VALUE_TYPE,
        MW_VOID_PTR VALUE);

#endif

/** \} */
