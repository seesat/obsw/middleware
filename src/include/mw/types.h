/**----------------------------------------------------------------------------
 * \file mw/types.h
 * 
 * Global constant and type definitions
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_types
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_types
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef MW_TYPES
#define MW_TYPES

/* basic data types */
typedef APEX_BYTE MW_BYTE;                 /**< 8-bit unsigned */

typedef APEX_SHORT MW_USHORT;              /**< 16-bit unsigned */

typedef APEX_INTEGER MW_DWORD;             /**< 32-bit signed */

typedef APEX_UNSIGNED MW_UDWORD;           /**< 32-bit unsigned */

typedef APEX_LONG_INTEGER MW_QWORD;        /**< 64-bit signed */

typedef unsigned long long MW_UQWORD;      /**< 64-bit unsigned */

/* specific data types */
typedef RETURN_CODE_TYPE MW_ERROR_TYPE;

typedef void MW_VOID;

typedef float MW_FLOAT;                    /**< 32-bit float */

typedef double MW_DOUBLE;                  /**< 64-bit float */

typedef SYSTEM_ADDRESS_TYPE MW_VOID_PTR;

#endif

/** \} */