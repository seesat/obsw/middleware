/**----------------------------------------------------------------------------
 * \file mw.h
 * 
 * MW constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup interface_mw
 *---------------------------------------------------------------------------*/
/** \addtogroup interface_mw
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/types.h>

#ifndef MW
#define MW

/**
 * The \a INIT_MW service request initializes the MW layer. This service
 * request must be called at the beginning of each application, before the
 * usage of any other MW service requests.
 * The service request automatically calls the underlying \ref INIT_OSAL
 * service request to initialize the OSAL layer, too.
 * Also, it initializes all defined mandatory middleware services and sets
 * up the complete middleware to provide and use services.
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         The layer can't be initialized
 */
extern MW_ERROR_TYPE INIT_MW(MW_VOID);

#endif

/** \} */