/**----------------------------------------------------------------------------
 * \file app.h
 * 
 * App constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_app
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_app
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/types.h>

#ifndef MW_APP
#define MW_APP

/** Maximum number of characters for the app name */
#define APP_NAME_SIZE                  32

typedef MW_BYTE APP_NAME_TYPE[APP_NAME_SIZE];

typedef MW_UDWORD APP_ADDRESS_TYPE;

/** Configuration information for an app */
typedef struct
{
    APP_NAME_TYPE NAME;                /**< Name of the app */
    APP_ADDRESS_TYPE ADDRESS;          /**< Unique address of the app */
} APP_INFO_TYPE;

#endif

/** \} */