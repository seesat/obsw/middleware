/**----------------------------------------------------------------------------
 * \file service.h
 * 
 * Service constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_service
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_service
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/com/message.h>
#include <mw/types.h>

#ifndef MW_SERVICE
#define MW_SERVICE

/**
 * Prototype for a service function of the middleware.
 * The \p BUFFER contains binary data of the \ref MESSAGE_TYPE::DATA field.
 * The size of the delivered binary data is specified by \p LENGTH in byte.
 * The application programmer should set an return value at functions of
 * this type. This indicates the middleware after triggering a instance of this
 * protoype function the result of the application task.
 * 
 * \param  [out]                BUFFER
 * \param  [out]                LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Handle the service function was unsuccessful
 */
typedef MW_ERROR_TYPE (*SERVICE_FUNCTION)(
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE *LENGTH);

/**
 * Prototype for serializing the service data to an binary stream.
 * This binary stream will be filled into the \ref MESSAGE_TYPE::DATA field
 * of a middleware message.
 * 
 * \param  [in]                 DATA_STRUCT
 * \param  [out]                BUFFER
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Serializing the service data failed
 */
typedef MW_ERROR_TYPE (*SERIALZE_SERVICE_FUNCTION)(
        MW_VOID_PTR DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * Prototype for deserializing the service data to an specified service
 * \p DATA_STRUCT that can be easyly accessed by the developer.
 * 
 * \param  [out]                DATA_STRUCT
 * \param  [in]                 BUFFER
 * \param  [in]                 LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Deserializing the service data failed
 */
typedef MW_ERROR_TYPE (*DESERIALZE_SERVICE_FUNCTION)(
        MW_VOID_PTR DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH);

/**
 * The \a REGISTER_MANDATORY_SERVICES request registers all specified
 * mandatory services of the middlware. This function will be triggered
 * at the beginning of the app runtime by the \ref INIT_MW function.
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         The services could not be registered
 */
extern MW_ERROR_TYPE REGISTER_MANDATORY_SERVICES(MW_VOID);

/**
 * The \a REGISTER_SERVICE request registers the specified \p FUNCTION
 * by the middleware to react for incoming messages of the specified
 * \p SERVICE_TYPE.
 * 
 * \param  [in]                 SERVICE_TYPE
 * \param  [in]                 FUNCTION
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         The service could not be registered
 * 
 * \warning There is only one service \p FUNCTION for each \p SERVICE_TYPE
 *          possible. If another function should be registered for the same
 *          \p SERVICE_TYPE, please call the \ref UNREGISTER_SERVICE request
 *          before.
 */
extern MW_ERROR_TYPE REGISTER_SERVICE(
        MESSAGE_SERVICE_TYPE SERVICE_TYPE,
        SERVICE_FUNCTION FUNCTION);

/**
 * The \a UNREGISTER_SERVICE request unregisters a registered service function
 * connected to the \p SERVICE_TYPE.
 * 
 * \param  [in]                 SERVICE_TYPE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            The service is already unregistered
 * \retval INVALID_MODE         The service could not be unregistered
 */
extern MW_ERROR_TYPE UNREGISTER_SERVICE(MESSAGE_SERVICE_TYPE SERVICE_TYPE);

#endif

/** \} */