/**----------------------------------------------------------------------------
 * \file state_vector.h
 * 
 * Service constant and type definitions and management for state vector
 * services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_service_state_vector
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_service_state_vector
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/app/app.h>
#include <mw/com/message.h>
#include <mw/types.h>

#ifndef MW_SERVICE_STATE_VECTOR
#define MW_SERVICE_STATE_VECTOR

/** Status vector values. */
typedef enum
{
    NORMAL_OPERATION = 1,
    PRE_OPERATION = 2,
    FAILSAFE = 3,
} STATE_VECTOR_TYPE;

/**
 * Representation of the service data of a state_vector message.
 * The message service type is \ref MESSAGE_SERVICE_TYPE_STATE_VECTOR_CHANGE
 * and \ref MESSAGE_SERVICE_TYPE_FORCE_STATE_VECTOR_CHANGE.
 */
typedef struct
{
    STATE_VECTOR_TYPE STATE_VECTOR;    /**< State vector of an app */
} STATE_VECTOR_SERVICE_DATA_TYPE;

/**
 * The \a SERIALZE_STATE_VECTOR_SERVICE request serializes the service data to
 * an binary stream. This binary stream can be filled into the
 * \ref MESSAGE_TYPE::DATA field of a middleware message.
 * 
 * \param  [in]                 DATA_STRUCT
 * \param  [out]                BUFFER
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Serializing the service data failed
 */
extern MW_ERROR_TYPE SERIALZE_STATE_VECTOR_SERVICE(
        STATE_VECTOR_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * The \a DESERIALZE_STATE_VECTOR_SERVICE request deserializes the service data
 * to an specified service \p DATA_STRUCT that can be easyly accessed by the
 * developer.
 * 
 * \param  [out]                DATA_STRUCT
 * \param  [in]                 BUFFER
 * \param  [in]                 LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Deserializing the service data failed
 */
extern MW_ERROR_TYPE DESERIALZE_STATE_VECTOR_SERVICE(
        STATE_VECTOR_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH);

/**
 * The \a CHANGE_STATE service request is a wrapper function to report a new
 * \p STATE_VECTOR after a state change of an app by the supervisor app.
 * 
 * \param  [in]                 STATE_VECTOR
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Reporting the state change failed
 * 
 * \note This function should only be used by normal apps.
 */
extern MW_ERROR_TYPE CHANGE_STATE(STATE_VECTOR_SERVICE_DATA_TYPE STATE_VECTOR);

/**
 * The \a FORCE_CHANGE_STATE service request is a wrapper function to change
 * the internal state of an app specified by the \p APP_ADDRESS.
 * 
 * \param  [in]                 APP_ADDRESS
 * \param  [in]                 STATE_VECTOR
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Reporting the state change failed
 * 
 * \warning This function should only be used by the supervisor app to offer
 *          a remote control functionality.
 */
extern MW_ERROR_TYPE FORCE_CHANGE_STATE(
        APP_ADDRESS_TYPE APP_ADDRESS,
        STATE_VECTOR_SERVICE_DATA_TYPE STATE_VECTOR);

#endif

/** \} */