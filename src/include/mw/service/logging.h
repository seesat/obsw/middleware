/**----------------------------------------------------------------------------
 * \file logging.h
 * 
 * Service constant and type definitions and management for logging services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_service
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_service_logging
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/com/message.h>
#include <mw/types.h>

#ifndef MW_SERVICE_LOGGING
#define MW_SERVICE_LOGGING

/** 
 * Maximum number of byte of the logging message in byte:\n
 *   MESSAGE_MAX_DATA_LENGTH -
 *   sizeof(\ref LOGGING_SERVICE_DATA_TYPE::LENGTH) -
 *   sizeof(\ref LOGGING_SERVICE_DATA_TYPE::LOG_LEVEL) =
 *   496 byte - 2 byte - 4 byte = 490 byte
 */
#define MAX_LOGGING_MESSAGE_SIZE       490

/** Types of logging levels. */
typedef enum
{
    LEVEL_DEBUG = 1,
    LEVEL_INFO = 2,
    LEVEL_WARN = 3,
    LEVEL_ERROR = 4
} LOGGING_LEVEL_TYPE;

typedef MW_BYTE LOGGING_MESSAGE_TYPE[MAX_LOGGING_MESSAGE_SIZE];

/**
 * Representation of the service data of a logging message.
 * The message service type is \ref MESSAGE_SERVICE_TYPE_LOG_MESSAGE.
 */
typedef struct
{
    MESSAGE_LENGTH_TYPE LENGTH;        /**< Length of the logging message */
    LOGGING_LEVEL_TYPE LOG_LEVEL;      /**< Log level of the logging message */
    LOGGING_MESSAGE_TYPE MESSAGE;      /**< Content of the logging message */
} LOGGING_SERVICE_DATA_TYPE;

/**
 * The \a SERIALZE_LOGGING_SERVICE request serializes the service data to an
 * binary stream. This binary stream can be filled into the
 * \ref MESSAGE_TYPE::DATA field of a middleware message.
 * 
 * \param  [in]                 DATA_STRUCT
 * \param  [out]                BUFFER
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Serializing the service data failed
 */
extern MW_ERROR_TYPE SERIALZE_LOGGING_SERVICE(
        LOGGING_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * The \a DESERIALZE_LOGGING_SERVICE request deserializes the service data to
 * an specified service \p DATA_STRUCT that can be easyly accessed by the
 * developer.
 * 
 * \param  [out]                DATA_STRUCT
 * \param  [in]                 BUFFER
 * \param  [in]                 LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Deserializing the service data failed
 */
extern MW_ERROR_TYPE DESERIALZE_LOGGING_SERVICE(
        LOGGING_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH);

/**
 * The \a LOG service request is a wrapper function to generate the logging
 * service data and send them to the logging handler app.
 * 
 * \param  [in]                 LOG_LEVEL
 * \param  [in]                 MESSAGE
 * \param  [in]                 ...
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Logging the \p MESSAGE failed
 * 
 * \note This function has the same function declaration as the printf
 *       function of the C programming language to generate formated logging
 *       messages.
 */
extern MW_ERROR_TYPE LOG(
        LOGGING_LEVEL_TYPE LOG_LEVEL,
        const MW_BYTE *MESSAGE,
        ...);

#endif

/** \} */