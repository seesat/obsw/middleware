/**----------------------------------------------------------------------------
 * \file keepalive.h
 * 
 * Service constant and type definitions and management for keepalive services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_service
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_service_keepalive
 *  \{
 *---------------------------------------------------------------------------*/

#ifndef MW_SERVICE_KEEPALIVE
#define MW_SERVICE_KEEPALIVE

/** Heartbeat interval of keepalive messages to the supervisor app in
 *  milliseconds (ms) */
#define KEEPALIVE_HEARTBEAT_INTERVAL   2000

/** 
 * Representation of the service data of a keepalive message.
 * The message service type is \ref MESSAGE_SERVICE_TYPE_KEEPALIVE_HEARTBEAT.
 * \note Keepalive messages are messages with empty content.
 */
typedef struct {} KEEPALIVE_SERVICE_DATA_TYPE;

/**
 * The \a SERIALZE_KEEPALIVE_SERVICE request serializes the service data to an
 * binary stream. This binary stream can be filled into the
 * \ref MESSAGE_TYPE::DATA field of a middleware message.
 * 
 * \param  [in]                 DATA_STRUCT
 * \param  [out]                BUFFER
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Serializing the service data failed
 * 
 * \note The serialized service data is always zero, because the keepalive
 *       messages have no message content. 
 */
extern MW_ERROR_TYPE SERIALZE_KEEPALIVE_SERVICE(
        KEEPALIVE_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * The \a DESERIALZE_KEEPALIVE_SERVICE request deserializes the service data to
 * an specified service \p DATA_STRUCT that can be easyly accessed by the
 * developer.
 * 
 * \param  [out]                DATA_STRUCT
 * \param  [in]                 BUFFER
 * \param  [in]                 LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Deserializing the service data failed
 * 
 * \note The deserialized service data is always zero, because the keepalive
 *       messages have no message content. 
 */
extern MW_ERROR_TYPE DESERIALZE_KEEPALIVE_SERVICE(
        KEEPALIVE_SERVICE_DATA_TYPE *DATA_STRUCT,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH);

/**
 * The \a SEND_KEEPALIVE_HEARTBEAT service request is a wrapper function to
 * generate a keepalive heartbeat and send them to the supervisor app.
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Sending keepalive heartbeat failed
 */
extern MW_ERROR_TYPE SEND_KEEPALIVE_HEARTBEAT(MW_VOID);

#endif

/** \} */