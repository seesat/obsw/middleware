/**----------------------------------------------------------------------------
 * \file message.h
 * 
 * Message constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_com_message
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_com_message
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/app/app.h>
#include <mw/types.h>

#ifndef MW_COM_MESSAGE
#define MW_COM_MESSAGE

/* 
 * version information of the message
 */
typedef MW_BYTE MESSAGE_VERSION_TYPE;

/** Valid message protocol version 1 */
#define MESSAGE_VERSION               1

/*
 * communication type of the message
 */
typedef MW_BYTE MESSAGE_COM_TYPE;

/** Message is a middleware unicast to another app */
#define MESSAGE_COM_TYPE_UNICAST      1

/** Message is a middleware broadcast to all apps */
#define MESSAGE_COM_TYPE_BROADCAST    2

/*
 * flags for the message
 */
typedef MW_BYTE MESSAGE_FLAGS_TYPE;

/**
 * Message data field is encrypted.
 */
#define MESSAGE_FLAGS_DATA_ENCRYPTED  1

/*
 * content type of the message data 
 */
typedef MW_USHORT MESSAGE_SERVICE_TYPE;

/**
 * Message contains logging data.
 * \see SERIALZE_LOGGING_SERVICE
 * \see DESERIALZE_LOGGING_SERVICE
 * \see LOG
 */
#define MESSAGE_SERVICE_TYPE_LOG_MESSAGE                   1

/** 
 * Message is a keepalive heart beat.
 * \see SERIALZE_KEEPALIVE_SERVICE
 * \see DESERIALZE_KEEPALIVE_SERVICE
 * \see SEND_KEEPALIVE_HEARTBEAT
 */
#define MESSAGE_SERVICE_TYPE_KEEPALIVE_HEARTBEAT           10

/** 
 * Message is a state vector change performed by an app.
 * App reports state vector change to supervisor app.
 * \see SERIALZE_STATE_VECTOR_SERVICE
 * \see DESERIALZE_STATE_VECTOR_SERVICE
 * \see CHANGE_STATE
 */
#define MESSAGE_SERVICE_TYPE_STATE_VECTOR_CHANGE           11

/** 
 * Message is a forced state vector change from the supervisor app.
 * Supervisor app forces a app to change the internal state.
 * \see SERIALZE_STATE_VECTOR_SERVICE
 * \see DESERIALZE_STATE_VECTOR_SERVICE
 * \see FORCE_CHANGE_STATE
 */
#define MESSAGE_SERVICE_TYPE_FORCE_STATE_VECTOR_CHANGE     12

/*
 * data and data size of the message
 */
typedef MW_USHORT MESSAGE_LENGTH_TYPE;

/** 
 * Message header length without checksum field in byte:\n
 *   sizeof(\ref MESSAGE_TYPE::VERSION) +
 *   sizeof(\ref MESSAGE_TYPE::COM_TYPE) +
 *   sizeof(\ref MESSAGE_TYPE::FLAGS) +
 *   sizeof(\ref MESSAGE_TYPE::RESERVED) +
 *   sizeof(\ref MESSAGE_TYPE::SRC_ADDRESS) + 
 *   sizeof(\ref MESSAGE_TYPE::DST_ADDRESS) +
 *   sizeof(\ref MESSAGE_TYPE::SERVICE_TYPE) +
 *   sizeof(\ref MESSAGE_TYPE::LENGTH) =
 *   1 byte + 1 byte + 1 byte + 4 byte + 4 byte + 2 byte + 2 byte = 16 byte
 */
#define MESSAGE_HEADER_LENGTH          16

/** Maximum possible number of bytes of the message data */
#define MESSAGE_MAX_DATA_LENGTH        1006

/** 
 * Maximum possible size in bytes of complete message:\n
 *   \ref MESSAGE_HEADER_LENGTH +
 *   \ref MESSAGE_MAX_DATA_LENGTH +
 *   sizeof(\ref MESSAGE_TYPE::CHECKSUM) =
 *   16 byte + 1006 byte + 2 byte = 1024 byte
 */
#define MESSAGE_MAX_LENGTH  MESSAGE_HEADER_LENGTH + MESSAGE_MAX_DATA_LENGTH + 2

typedef MW_BYTE MESSAGE_DATA_TYPE[MESSAGE_MAX_DATA_LENGTH];

/*
 * checksum type of the message
 */
typedef MW_USHORT MESSAGE_CHECKSUM_TYPE;

typedef struct
{
    MESSAGE_VERSION_TYPE VERSION;      /**< Version of the message protocol */
    MESSAGE_COM_TYPE COM_TYPE;         /**< Communication type of the
                                        *   message */
    MESSAGE_FLAGS_TYPE FLAGS;          /**< Flags for the message */
    MW_BYTE RESERVED;                  /**< \warning Reserved for further use.
                                        *            Do not use that field. */
    APP_ADDRESS_TYPE SRC_ADDRESS;      /**< App address of the message
                                        *   source */
    APP_ADDRESS_TYPE DST_ADDRESS;      /**< App address of the message
                                        *   destination */
    MESSAGE_SERVICE_TYPE SERVICE_TYPE; /**< Type of content of the message
                                        *   data */
    MESSAGE_LENGTH_TYPE LENGTH;        /**< Length of the message date in
                                        *   bytes */
    MESSAGE_DATA_TYPE DATA;            /**< Data payload of the message */
    MESSAGE_CHECKSUM_TYPE CHECKSUM;    /**< Checksum of the complete message */
} MESSAGE_TYPE;

/** Broadcast app address for \ref MESSAGE_COM_TYPE_BROADCAST */
#define APP_BROADCAST_ADDRESS          0xffffffff

/**
 * The \a CREATE_MESSAGE service request initializes a empty message with
 * default header values.
 * 
 * \param  [out]                MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE CREATE_MESSAGE(MESSAGE_TYPE *MESSAGE);

/**
 * The \a SERIALIZE_MESSAGE service request serializes a specified \p MESSAGE
 * into a byte stream. The byte stream is written into the \p BUFFER with
 * with capacity of \p LENGTH bytes. The \p BUFFER length must have a minimum
 * capacity of \ref MESSAGE_MAX_LENGTH to serialize a message with maximum
 * possible length. The serialized byte stream size of the \p MESSAGE is
 * written into \p STREAM_LENGTH.
 * 
 * \param  [in]                 MESSAGE
 * \param  [out]                BUFFER
 * \param  [in]                 LENGTH
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH of \p BUFFER is undersized to
 *                              serialize the complete specified \p MESSAGE
 */
extern MW_ERROR_TYPE SERIALIZE_MESSAGE(
        MESSAGE_TYPE *MESSAGE,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * The \a DESERIALIZE_MESSAGE service request deserializes a byte stream into a
 * accessable \p MESSAGE structure. The byte stream is read \p BUFFER with a
 * capacity of \p STREAM_SIZE.
 * 
 * \param  [out]                MESSAGE
 * \param  [in]                 BUFFER
 * \param  [in]                 STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p STREAM_LENGTH is equal to zero
 */
extern MW_ERROR_TYPE DESERIALIZE_MESSAGE(
        MESSAGE_TYPE *MESSAGE,
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE STREAM_LENGTH);

/**
 * The \a ENCRYPT_MESSAGE_DATA service request encrypts the message data of
 * the specified \p MESSAGE. The message will be enrypted with the AES
 * algorithm with help of the hardware optimized \ref osal_crypt_aes "OSAL AES"
 * functions.
 * 
 * \param  [in,out]             MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Encryption of the message data failed
 * 
 * \note This function for a message is optional and should only be used if
 *       communication between the apps must be secure. That's very important,
 *       if all applications do not run on the same machine.
 */
extern MW_ERROR_TYPE ENCRYPT_MESSAGE_DATA(MESSAGE_TYPE *MESSAGE);

/**
 * The \a DECRYPT_MESSAGE_DATA service request decrypts the message data of
 * the specified \p MESSAGE. The message will be derypted with the AES
 * algorithm with help of the hardware optimized \ref osal_crypt_aes "OSAL AES"
 * functions.
 * 
 * \param  [in,out]             MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Decryption of the message data failed
 * 
 * \note This function for a message is optional and should only be used if
 *       communication between the apps must be secure. That's very important,
 *       if all applications do not run on the same machine.
 */
extern MW_ERROR_TYPE DECRYPT_MESSAGE_DATA(MESSAGE_TYPE *MESSAGE);

/**
 * The \a GENERATE_MESSAGE_CRC service request caluclates the CRC checksum
 * for the complete \p MESSAGE.
 * 
 * \param  [in,out]             MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE GENERATE_MESSAGE_CRC(MESSAGE_TYPE *MESSAGE);

/**
 * The \a CHECK_MESSAGE_CRC service request checks the CRC checksum
 * of the \p MESSAGE. Therefore, the checksum is calculated for the complete
 * \p MESSAGE, including the original checksum, again. The result should be
 * zero to obtain an valid \p MESSAGE, otherwise the \p MESSAGE was damaged.
 * 
 * \param  [in]                 MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         \p MESSAGE was damaged and is invalid
 */
extern MW_ERROR_TYPE CHECK_MESSAGE_CRC(MESSAGE_TYPE *MESSAGE);

/**
 * The \a READ_MESSAGE_DATA16 service request reads a 16 bit value from \p IN
 * convert it to the correct host byte order and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE READ_MESSAGE_DATA16(
        MW_USHORT *IN,
        MW_USHORT *OUT);

/**
 * The \a READ_MESSAGE_DATA32 service request reads a 32 bit value from \p IN
 * convert it to the correct host byte order and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE READ_MESSAGE_DATA32(
        MW_UDWORD *IN,
        MW_UDWORD *OUT);

/**
 * The \a READ_MESSAGE_DATA64 service request reads a 64 bit value from \p IN
 * convert it to the correct host byte order and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE READ_MESSAGE_DATA64(
        MW_UQWORD *IN,
        MW_UQWORD *OUT);

/**
 * The \a READ_MESSAGE_DATA_FLOAT service request reads a 32 bit float value
 * from \p IN convert it to the correct host byte order and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE READ_MESSAGE_DATA_FLOAT(
        MW_FLOAT *IN,
        MW_FLOAT *OUT);

/**
 * The \a READ_MESSAGE_DATA_DOUBLE service request reads a 64 bit float value
 * from \p IN convert it to the correct host byte order and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE READ_MESSAGE_DATA_DOUBLE(
        MW_DOUBLE *IN,
        MW_DOUBLE *OUT);

/**
 * The \a WRITE_MESSAGE_DATA16 service request reads a 16 bit value from \p IN
 * convert it to the correct byte order for message transportation and saves it
 * in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE WRITE_MESSAGE_DATA16(
        MW_USHORT *IN,
        MW_USHORT *OUT);

/**
 * The \a WRITE_MESSAGE_DATA32 service request reads a 32 bit value from \p IN
 * convert it to the correct byte order for message transportation and saves it
 * in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE WRITE_MESSAGE_DATA32(
        MW_UDWORD *IN,
        MW_UDWORD *OUT);

/**
 * The \a WRITE_MESSAGE_DATA64 service request reads a 64 bit value from \p IN
 * convert it to the correct byte order for message transportation and saves it
 * in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE WRITE_MESSAGE_DATA64(
        MW_UQWORD *IN,
        MW_UQWORD *OUT);

/**
 * The \a WRITE_MESSAGE_DATA_FLOAT service request reads a 32 bit float
 * value from \p IN convert it to the correct byte order for message
 * transportation and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE WRITE_MESSAGE_DATA_FLOAT(
        MW_FLOAT *IN,
        MW_FLOAT *OUT);

/**
 * The \a WRITE_MESSAGE_DATA_DOUBLE service request reads a 64 bit float
 * value from \p IN convert it to the correct byte order for message
 * transportation and saves it in \p OUT.
 * 
 * \param  [in]                 IN
 * \param  [out]                OUT
 * 
 * \retval NO_ERROR             Successful completion
 */
extern MW_ERROR_TYPE WRITE_MESSAGE_DATA_DOUBLE(
        MW_DOUBLE *IN,
        MW_DOUBLE *OUT);

#endif

/** \} */