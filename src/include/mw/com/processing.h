/**----------------------------------------------------------------------------
 * \file processing.h
 * 
 * Communication processing constant and type definitions and management
 * services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup mw_com_processing
 *---------------------------------------------------------------------------*/
/** \addtogroup mw_com_processing
 *  \{
 *---------------------------------------------------------------------------*/

#include <mw/app/app.h>
#include <mw/com/message.h>
#include <mw/types.h>

#ifndef MW_COM_PROCESSING
#define MW_COM_PROCESSING

/**
 * The \a PROCESS_MESSAGES service request receives tries to receive a message.
 * If a message is avaiable, this function process the message by calling the
 * correct registered service function. After that, the registered service
 * function is able to deserialize the message with the correct deserializing
 * function of the middleware \ref mw_service.
 * If there is no message avaiable to process, the function returns an
 * INVALID_MODE error.
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         There is no message avaiable to process
 */
extern MW_ERROR_TYPE PROCESS_MESSAGES(MW_VOID);

/**
 * The \a RECEIVE_MESSAGE service request receives a message from any other
 * application. This service request automatically proceed all needed steps to
 * receive a message and convert the received data into a high level \p MESSAGE.
 * Therefore, the following functions will be called:
 *   - \ref RECEIVE_RAW_MESSAGE
 *     - \ref DESERIALIZE_MESSAGE
 *       - Optional: [\ref DECRYPT_MESSAGE_DATA]
 * 
 * If there is no message avaiable, the function returns an INVALID_MODE error.
 * 
 * \param  [out]                MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         There is no message avaiable
 */
extern MW_ERROR_TYPE RECEIVE_MESSAGE(MESSAGE_TYPE *MESSAGE);

/**
 * The \a RECEIVE_RAW_MESSAGE service request receives a message from any other
 * application. All communication channels to other apps are checked for
 * new messages. If a message is avaiable, the message with the length of
 * \p STREAM_LENGTH will be copied into the specified \p BUFFER with a
 * capacity of \p LENGTH byte. If there is no message avaiable, the function
 * returns an INVALID_MODE error.
 * 
 * \param  [out]                BUFFER
 * \param  [in]                 LENGTH
 * \param  [out]                STREAM_LENGTH
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH of the \p BUFFER is undersized to
 *                              copy the entire message into it
 * \retval INVALID_MODE         There is no message avaiable
 */
extern MW_ERROR_TYPE RECEIVE_RAW_MESSAGE(
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH,
        MESSAGE_LENGTH_TYPE *STREAM_LENGTH);

/**
 * The \a SEND_MESSAGE service request sends a specified \p MESSAGE to an
 * application. The addressed application is specified by the
 * \ref MESSAGE_TYPE::DST_ADDRESS field in the \p MESSAGE structure.
 * This service request automatically proceed all needed steps to send the
 * high level \p MESSAGE to the destination application. Therefore, the
 * following functions will be called:
 *   - Optional: [\ref ENCRYPT_MESSAGE_DATA]
 *     - \ref SERIALIZE_MESSAGE
 *       - \ref SEND_RAW_MESSAGE
 * 
 * \param  [in]                 MESSAGE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Message could not be sent
 */
extern MW_ERROR_TYPE SEND_MESSAGE(MESSAGE_TYPE *MESSAGE);

/**
 * The \a SEND_RAW_MESSAGE service request sends a serialized message to an
 * application specified by the \p DST_ADDRESS parameter. The \p BUFFER
 * contains the binary message with the message size of \p LENGTH.
 * Before the message is send, the function determines the correct transport
 * medium to reach the addressed application. After that operation, the message
 * is sent away with the help of the \ref osal_io "OSAL IO" module.
 * 
 * \param  [in]                 BUFFER
 * \param  [in]                 LENGTH
 * \param  [in]                 DST_ADDRESS
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is equal to zero
 * \retval INVALID_MODE         Message could not be sent
 */
extern MW_ERROR_TYPE SEND_RAW_MESSAGE(
        MW_BYTE *BUFFER,
        MESSAGE_LENGTH_TYPE LENGTH,
        APP_ADDRESS_TYPE DST_ADDRESS);

#endif

/** \} */