/**----------------------------------------------------------------------------
 * \file crc.h
 * 
 * CRC constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup osal_crypt_crc
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_crypt_crc
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_CRYPT_CRC
#define OSAL_CRYPT_CRC

/**
 * The \a GENERATE_CRC8 service request is used to calculate the 8bit CRC
 * value from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes.
 * 
 * The following polynomial representation is used for calculation:
 * \f$ x^{8} + x^{4} + x^{3} + x^{2} + 1 \f$
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                CRC
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero
 */
extern void GENERATE_CRC8(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *CRC,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GENERATE_CRC16 service request is used to calculate the 16bit CRC
 * value from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes.
 * 
 * The following polynomial representation is used for calculation:
 * \f$ x^{16} + x^{12} + x^{5} + 1 \f$
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                CRC
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero
 */
extern void GENERATE_CRC16(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_SHORT *CRC,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GENERATE_CRC32 service request is used to calculate the 32bit CRC
 * value from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes.
 * 
 * The following polynomial representation is used for calculation:
 * \f$ x^{32} + x^{26} + x^{23} + x^{22} + x^{16} + x^{12} + x^{11} + x^{10} +
 *     x^{8} + x^{7} + x^{5} + x^{4} + x^{2} + x + 1 \f$
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                CRC
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero
 */
extern void GENERATE_CRC32(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_UNSIGNED *CRC,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */