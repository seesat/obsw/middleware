/**----------------------------------------------------------------------------
 * \file hash.h
 * 
 * HASH constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup osal_crypt_hash
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_crypt_hash
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_CRYPT_HASH
#define OSAL_CRYPT_HASH

/**
 * The \a GENERATE_MD5 service request is used to calculate the 128bit MD5
 * hash from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes. The output buffer for the \p HASH value must have an capacity of
 * 16byte.
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                HASH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * 
 * \warning The security of the MD5 hash function has been severely
 *          compromised.
 */
extern void GENERATE_MD5(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *HASH,
        RETURN_CODE_TYPE *RETURN_CODE);


/**
 * The \a GENERATE_SHA1 service request is used to calculate the 160bit SHA-1
 * hash from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes. The output buffer for the \p HASH value must have an capacity of
 * 20byte.
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                HASH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * 
 * \warning The SHA-1 hash function might not be secure enough for ongoing use.
 *          It's recommended to replace it by SHA-2 hash functions.
 */
extern void GENERATE_SHA1(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *HASH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GENERATE_SHA224 service request is used to calculate the 224bit SHA-2
 * hash from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes. The output buffer for the \p HASH value must have an capacity of
 * 28byte.
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                HASH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GENERATE_SHA224(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *HASH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GENERATE_SHA256 service request is used to calculate the 256bit SHA-2
 * hash from a specified binary \p INPUT buffer with capacity of \p LENGTH
 * bytes. The output buffer for the \p HASH value must have an capacity of
 * 32byte.
 * 
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                HASH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GENERATE_SHA256(
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *HASH,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */