/**----------------------------------------------------------------------------
 * \file aes.h
 * 
 * AES constant and type definitions and management services
 * 
 * These services are adopted from Lawrence Bassham ANSI C API for AES at the
 * National Institute of Standards and Technology (NIST).
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * 
 * \author Lawrence Bassham
 * 
 * \ingroup osal_crypt_aes
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_crypt_aes
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_CRYPT_AES
#define OSAL_CRYPT_AES

#define MAX_KEY_SIZE                  64   /**< Number of ASCII char’s needed
                                            *   to represent a key */
#define MAX_IV_SIZE                   16   /**< Number of bytes needed to
                                            *   represent an IV */

typedef APEX_UNSIGNED BLOCK_SIZE_TYPE;

typedef APEX_BYTE KEY_MATERIAL_TYPE[MAX_KEY_SIZE + 1];

typedef APEX_BYTE IV_TYPE[MAX_IV_SIZE];

typedef enum
{
    KEY_LEN_128 = 1,
    KEY_LEN_192 = 2,
    KEY_LEN_256 = 3
} KEY_LENGTH_TYPE;

typedef enum
{
    DIR_ENCRYPT = 0,                       /**< Direction for encrpyting */
    DIR_DECRYPT = 1                        /**< Direction for decrpyting */
} DIRECTION_TYPE;

typedef enum
{
    MODE_ECB = 1,                          /**< Ciphering in ECB mode */
    MODE_CBC = 2,                          /**< Ciphering in CBC mode */
    MODE_CFB1 = 3                          /**< Ciphering in 1-bit CFB mode */
} CIPHER_MODE_TYPE;

typedef struct
{
    DIRECTION_TYPE DIRECTION;              /**< Direction of the used key */
    KEY_LENGTH_TYPE KEY_LENGTH;            /**< Length of the key */
    KEY_MATERIAL_TYPE KEY_MATERIAL;        /**< Raw key data in ASCII, e.g.,
                                            *   user input or KAT values */
    
    /* The following parameters are algorithm dependent, replace or add as
     * necessary */
    APEX_BYTE *KS;                         /**< Pointer to a Key Schedule, a la
                                            *   DES */
} KEY_TYPE;

/* The structure for cipher information */
typedef struct
{
    CIPHER_MODE_TYPE MODE;                 /**< Chiper mode */
    IV_TYPE IV;                            /**< A possible Initialization
                                            *   Vector for ciphering */
    BLOCK_SIZE_TYPE BLOCK_SIZE;            /**< Block size for the chiper */
} CIPHER_TYPE;

/**
 * The \a MAKE_KEY_AES service request initializes a AES \p KEY with the
 * following information:
 *     - DIRECTION: the \p KEY is being setup for encryption or decryption
 *     - KEY_LENGTH: The key length (128, 192, 256) of the \p KEY
 *     - KEY_MATERIAL: The raw key data of the \p KEY
 * 
 * \param  [out]                KEY
 * \param  [in]                 DIRECTION
 * \param  [in]                 KEY_LENGTH
 * \param  [in]                 KEY_MATERIAL
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void MAKE_KEY_AES(
        KEY_TYPE *KEY,
        DIRECTION_TYPE DIRECTION,
        KEY_LENGTH_TYPE KEY_LENGTH,
        KEY_MATERIAL_TYPE KEY_MATERIAL,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a INIT_CHIPER_AES service request initializes the cipher with the mode
 * and, if present, sets the Initialization Vector.
 * The IV parameter passed to \a INIT_CHIPER_AES is an ASCII hex string
 * representation of the IV, i.e. the IV passed as a parameter will typically
 * be 32bytes long. The IV field of the \p CIPHER structure is
 * the binary value of the IV, i.e. it will typically be 16bytes long.
 * 
 * \param  [out]                CIPHER
 * \param  [in]                 MODE
 * \param  [in]                 IV
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void INIT_CHIPER_AES(
        CIPHER_TYPE *CIPHER,
        CIPHER_MODE_TYPE MODE,
        IV_TYPE IV,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a ENCRYPT_BLOCK_AES service request uses the \p CIPHER object and the
 * \p KEY object to encrypt one block of data in the input buffer. The output
 * (the encrypted data) is returned in \p ENCRYPT, which is the same size as
 * \p INPUT. \p LENGTH will typically be 128 bits, but some algorithms may
 * handle additional block sizes. Additionally, it is acceptable to use this
 * routine to encrypt multiple “blocks” of data with one call. For example, if
 * your algorithm has a block size of 128 bits, it is acceptable to pass
 * n*128 bits to \a ENCRYPT_BLOCK_AES.
 * 
 * \param  [in]                 CIPHER
 * \param  [in]                 KEY
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                ENCRYPT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero
 */
extern void ENCRYPT_BLOCK_AES(
        CIPHER_TYPE *CIPHER,
        KEY_TYPE *KEY,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *ENCRYPT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a DECRYPT_BLOCK_AES service request uses the \p CIPHER object and the
 * \p KEY object to decrypt one block of data in the \p INPUT buffer. The
 * output (the decrypted data) is returned in \p DECRYPT, which is the same
 * size as \p INPUT. \p LENGTH will typically be 128 bits, but some algorithms
 * may handle additional block sizes. Additionally, it is acceptable to use
 * this routine to decrypt multiple “blocks” of data with one call. For
 * example, if your algorithm has a block size of 128 bits, it is acceptable
 * to pass n*128 bits to \a DECRYPT_BLOCK_AES.
 * 
 * \param  [in]                 CIPHER
 * \param  [in]                 KEY
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                DECRYPT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero
 */
extern void DECRYPT_BLOCK_AES(
        CIPHER_TYPE *CIPHER,
        KEY_TYPE *KEY,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        APEX_BYTE *DECRYPT,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */