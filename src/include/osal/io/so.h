/**----------------------------------------------------------------------------
 * \file so.h
 * 
 * SOCKET constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup osal_io_so
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_io_so
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/io/io.h>

#ifndef OSAL_IO_SO
#define OSAL_IO_SO

/** 
 * Length of an IPv4 address in ascii representation with dots.
 * \see IP_TYPE
 */
#define IP_ADDRESS_LENGTH             15

/** Specific representation of a socket device */
typedef struct
{
    DEVICE_TYPE base;                 /**< Instance of the base class */
    
} DEVICE_SO_TYPE;

/** IPv4 address in ascii representation: [xxx].[xxx].[xxx].[xxx] */
typedef APEX_BYTE IP_TYPE[IP_ADDRESS_LENGTH];

/** Socket port range */
typedef APEX_SHORT PORT_TYPE;

/** Specific representation of a message queue device info */
typedef struct
{
    DEVICE_NAME_TYPE SO_NAME;         /**< Name of the socket */
    IP_TYPE LOCAL_SO_IP;              /**< Local IPv4 address of the socket */
    PORT_TYPE LOCAL_SO_PORT;          /**< Local port of the socket */
    IP_TYPE REMOTE_SO_IP;             /**< Remote IPv4 address of the socket */
    PORT_TYPE REMOTE_SO_PORT;         /**< Remote port of the socket */
} DEVICE_SO_SETTING_TYPE;

/**
 * \see CREATE_DEVICE
 */
extern void CREATE_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        IO_TYPE TYPE,
        DEVICE_SO_SETTING_TYPE *SETTING,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see OPEN_DEVICE
 */
extern void OPEN_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see CLOSE_DEVICE
 */
extern void CLOSE_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see READ_DEVICE
 */
extern void READ_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *OUTPUT,
        MESSAGE_SIZE_TYPE LENGTH_BUFFER,
        MESSAGE_SIZE_TYPE *LENGTH_RECV,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see WRITE_DEVICE
 */
extern void WRITE_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see IOCTL_DEVICE
 */
extern void IOCTL_SO_DEVICE(
        DEVICE_SO_TYPE *DEVICE,
        IOCTL_PARAM_TYPE PARAM,
        SYSTEM_ADDRESS_TYPE VALUE,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
