/**----------------------------------------------------------------------------
 * \file io.h
 * 
 * IO constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup osal_io
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_io
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_IO
#define OSAL_IO

/** Maximum size of a device name */
#define DEVICE_MAX_NAME_LENGTH        32

typedef APEX_BYTE DEVICE_NAME_TYPE[DEVICE_MAX_NAME_LENGTH];

typedef enum
{
    MQ = 1,         /**< Message Queue */
    SO = 2,         /**< Network Sockets */
    I2C = 3,        /**< Inter-Integrated Circuit */
    SER = 4,        /**< Serial */
    USB = 5,        /**< Universal Serial Bus */
    CAN = 6,        /**< Controller Area Network */
    SPI = 7,        /**< Serial Peripheral Interface */
    SPW = 8,        /**< Space Wire */
    MIL = 9         /**< MIL-STD 1553 */
} IO_TYPE;

typedef enum
{
    BLOCKING = 1,
    NON_BLOCKING = 2
} FLAGS_TYPE;

typedef enum
{
    MQ_MESSAGE_SIZE = 1
} IOCTL_PARAM_TYPE;

/** 
 * Prototype for creating an IO device.
 * 
 * \see CREATE_DEVICE
 */
typedef void (*IO_CREATE_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        IO_TYPE TYPE,
        SYSTEM_ADDRESS_TYPE SETTING,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * Prototype for opening an IO device.
 * 
 * \see OPEN_DEVICE
 */
typedef void (*IO_OPEN_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * Prototype for closing an IO device.
 * 
 * \see CLOSE_DEVICE
 */
typedef void (*IO_CLOSE_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * Prototype for reading from an IO device
 * 
 * \see READ_DEVICE
 */
typedef void (*IO_READ_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *OUTPUT,
        MESSAGE_SIZE_TYPE LENGTH_BUFFER,
        MESSAGE_SIZE_TYPE *LENGTH_RECV,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * Prototype for writing to an IO device.
 * 
 * \see WRITE_DEVICE
 */
typedef void (*IO_WRITE_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * Prototype for controling an IO device.
 * 
 * \see IOCTL_DEVICE
 */
typedef void (*IO_IOCTL_FUNCTION)(
        SYSTEM_ADDRESS_TYPE DEVICE,
        IOCTL_PARAM_TYPE PARAM,
        SYSTEM_ADDRESS_TYPE VALUE,
        RETURN_CODE_TYPE *RETURN_CODE);

/** General representation of an IO device */
typedef struct
{
    IO_TYPE TYPE;                     /**< Type of the device */
    DEVICE_NAME_TYPE NAME;            /**< Name of the device */
    IO_CREATE_FUNCTION create;        /**< Create function of the device */
    IO_OPEN_FUNCTION open;            /**< Open function of the device */
    IO_CLOSE_FUNCTION close;          /**< Close function of the device */
    IO_READ_FUNCTION read;            /**< Read function of the device */
    IO_WRITE_FUNCTION write;          /**< Write function of the device */
    IO_IOCTL_FUNCTION ioctl;          /**< IO control function of the device */
} DEVICE_TYPE;

/**
 * The \a CREATE_DEVICE service request creates a new device and initializes
 * the \p DEVICE structure with the properties of the specified device \p TYPE.
 * Device specific settings structures can be specified by the \p SETTING
 * parameter.
 * 
 * \param  [out]                DEVICE
 * \param  [in]                 TYPE
 * \param  [in]                 SETTING
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Failed to create the \p DEVICE
 */
extern void CREATE_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        IO_TYPE TYPE,
        SYSTEM_ADDRESS_TYPE SETTING,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a OPEN_DEVICE service request opens a created and initialized device.
 * 
 * \param  [in]                 DEVICE
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         \p DEVICE can not be opened
 * \retval NO_ACTION            \p DEVICE is already be opened
 */
extern void OPEN_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CLOSE_DEVICE service request closes a device after usage.
 * 
 * \param  [in]                 DEVICE
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         \p DEVICE can not be closed
 * \retval NO_ACTION            \p DEVICE is already be closed
 */
extern void CLOSE_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a READ_DEVICE service request performs a read request on the specified
 * device. The read request can be controlled by the \p FLAGS parameter. This
 * parameter specify if the read operation should be excecuted as a blocking
 * or non-blocking function. The data is written to the \p OUTPUT buffer with
 * size \p LENGTH_BUFFER if data is avaiable and the number of stored bytes
 * in the buffer is returned in the parameter \p LENGTH_RECV. \p LENGTH_RECV
 * will be zero if there is no data avaiable.
 * 
 * \param  [in]                 DEVICE
 * \param  [in]                 FLAGS
 * \param  [out]                OUTPUT
 * \param  [in]                 LENGTH_BUFFER
 * \param  [out]                LENGTH_RECV
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            There is no data avaiable to read from the
 *                              device
 * \retval INVALID_PARAM        \p LENGTH_BUFFER is zero or negative
 * \retval INVALID_MODE         The specified \p DEVICE is not able to
 *                              perform the read request
 */
extern void READ_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *OUTPUT,
        MESSAGE_SIZE_TYPE LENGTH_BUFFER,
        MESSAGE_SIZE_TYPE *LENGTH_RECV,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a WRITE_DEVICE service request writes the specified data in the
 * \p INPUT buffer of the size \p LENGTH to the device. The \p FLAGS parameter
 * can control the operation to be performed in blocking or non-blocking mode.
 * 
 * \param  [in]                 DEVICE
 * \param  [in]                 FLAGS
 * \param  [in]                 INPUT
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is zero or negative
 * \retval INVALID_MODE         The specified \p DEVICE is not able to
 *                              perform the write request
 */
extern void WRITE_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a IOCTL_DEVICE service request can control device specific properties
 * or functions. Therefore, the IOCTL \p PARAM specifies the function that
 * should be performed on the \p DEVICE. The \p VALUE can be used to reference
 * to data if the IOCTL \p PARAM function requires an additional paramter or
 * returns a value.
 * 
 * \param  [in]                 DEVICE
 * \param  [in]                 PARAM
 * \param  [in,out]             VALUE
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p VALUE is not a valid parameter
 * \retval INVALID_MODE         The specified \p DEVICE is not able to
 *                              perform the IO control request
 * \retval NOT_AVAILABLE        The IOCTL \p PARAM is not avaiable on the
 *                              specified \p DEVICE 
 */
extern void IOCTL_DEVICE(
        SYSTEM_ADDRESS_TYPE DEVICE,
        IOCTL_PARAM_TYPE PARAM,
        SYSTEM_ADDRESS_TYPE VALUE,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */