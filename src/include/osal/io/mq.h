/**----------------------------------------------------------------------------
 * \file mq.h
 * 
 * MESSAGE QUEUE constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup osal_io_mq
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_io_mq
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/io/io.h>

#ifndef OSAL_IO_MQ
#define OSAL_IO_MQ

/** Specific representation of a message queue device */
typedef struct
{
    DEVICE_TYPE base;                 /**< Instance of the base class */
    
} DEVICE_MQ_TYPE;

typedef APEX_UNSIGNED MQ_SIZE_TYPE;

typedef APEX_UNSIGNED MQ_MAX_MESSAGES_TYPE;

/** Specific representation of a message queue device info */
typedef struct
{
    DEVICE_NAME_TYPE MQ_NAME;         /**< Name of the message queue */
    MQ_SIZE_TYPE MQ_SIZE;             /**< Size of the message queue in byte */
    MQ_MAX_MESSAGES_TYPE MQ_MAX_MESSAGES;   /**< Maximum number of possible
                                             *   messages in the queue */
} DEVICE_MQ_SETTING_TYPE;

/**
 * \see CREATE_DEVICE
 */
extern void CREATE_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        IO_TYPE TYPE,
        DEVICE_MQ_SETTING_TYPE *SETTING,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see OPEN_DEVICE
 */
extern void OPEN_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see CLOSE_DEVICE
 */
extern void CLOSE_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see READ_DEVICE
 */
extern void READ_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *OUTPUT,
        MESSAGE_SIZE_TYPE LENGTH_BUFFER,
        MESSAGE_SIZE_TYPE *LENGTH_RECV,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see WRITE_DEVICE
 */
extern void WRITE_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        FLAGS_TYPE FLAGS,
        APEX_BYTE *INPUT,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * \see IOCTL_DEVICE
 */
extern void IOCTL_MQ_DEVICE(
        DEVICE_MQ_TYPE *DEVICE,
        IOCTL_PARAM_TYPE PARAM,
        SYSTEM_ADDRESS_TYPE VALUE,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
