/**----------------------------------------------------------------------------
 * \file byte_order.h
 * 
 * BYTE ORDER constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * 
 * \ingroup osal_misc_byte_order
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_misc_byte_order
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_MISC_BYTE_ORDER
#define OSAL_MISC_BYTE_ORDER

typedef enum
{
    LITTLE_ENDIAN = 1,          /**< Little endian byte order */
    BIG_ENDIAN = 2,             /**< Big endian byte order */
} BYTE_ORDER_TYPE;

/**
 * The \a GET_BYTE_ORDER service request returns the byte order of the current
 * system.
 * 
 * \param  [out]                BYTE_ORDER
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GET_BYTE_ORDER(
        BYTE_ORDER_TYPE *BYTE_ORDER,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CHANGE_BYTE_ORDER16 service request swaps the byte order of the
 * given 16bit \p INPUT value and stores the result in \p OUTPUT.
 * 
 * \param  [in]                 INPUT
 * \param  [out]                OUTPUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void CHANGE_BYTE_ORDER16(
        APEX_SHORT INPUT,
        APEX_SHORT *OUTPUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CHANGE_BYTE_ORDER32 service request swaps the byte order of the
 * given 32bit \p INPUT value and stores the result in \p OUTPUT.
 * 
 * \param  [in]                 INPUT
 * \param  [out]                OUTPUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void CHANGE_BYTE_ORDER32(
        APEX_INTEGER INPUT,
        APEX_INTEGER *OUTPUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CHANGE_BYTE_ORDER64 service request swaps the byte order of the
 * given 64bit \p INPUT value and stores the result in \p OUTPUT.
 * 
 * \param  [in]                 INPUT
 * \param  [out]                OUTPUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void CHANGE_BYTE_ORDER64(
        APEX_LONG_INTEGER INPUT,
        APEX_LONG_INTEGER *OUTPUT,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */