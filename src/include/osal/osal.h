/**----------------------------------------------------------------------------
 * \file osal.h
 * 
 * OSAL constant and type definitions and management services
 * 
 * \author Manuel Bentele
 * \author Dominik Schwarzbauer
 * \author Robin Windey
 * \ingroup interface_osal
 *---------------------------------------------------------------------------*/
/** \addtogroup interface_osal
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL
#define OSAL

/**
 * The \a INIT_OSAL service request initializes the OSAL layer. This service
 * request must be called at the beginning of each application, before the
 * usage of any other OSAL service requests.
 * 
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         The layer can't be initialized
 */
extern void INIT_OSAL(RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */