/**----------------------------------------------------------------------------
 * \file process.h
 * 
 * PROCESS constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Process Management
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_process
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_process
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_APEX_PROCESS
#define OSAL_APEX_PROCESS

#define MAX_NUMBER_OF_PROCESSES       SYSTEM_LIMIT_NUMBER_OF_PROCESSES

#define MIN_PRIORITY_VALUE            1

#define MAX_PRIORITY_VALUE            239

#define MAX_LOCK_LEVEL                16

typedef NAME_TYPE PROCESS_NAME_TYPE;

typedef APEX_INTEGER PROCESS_ID_TYPE;
#define   NULL_PROCESS_ID             0
#define   MAIN_PROCESS_ID            -1

typedef APEX_INTEGER LOCK_LEVEL_TYPE;

typedef APEX_UNSIGNED STACK_SIZE_TYPE;

typedef APEX_INTEGER WAITING_RANGE_TYPE;

typedef APEX_INTEGER PRIORITY_TYPE;

typedef enum
{
    DORMANT = 0,
    READY = 1,
    RUNNING = 2,
    WAITING = 3,
    FAULTED = 4
} PROCESS_STATE_TYPE;

typedef enum
{
    SOFT = 0,
    HARD = 1
} DEADLINE_TYPE;

typedef struct
{
    SYSTEM_TIME_TYPE PERIOD;
    SYSTEM_TIME_TYPE TIME_CAPACITY;
    SYSTEM_ADDRESS_TYPE ENTRY_POINT;
    STACK_SIZE_TYPE STACK_SIZE;
    PRIORITY_TYPE BASE_PRIORITY;
    DEADLINE_TYPE DEADLINE;
    PROCESS_NAME_TYPE NAME;
} PROCESS_ATTRIBUTE_TYPE;

typedef struct
{
    SYSTEM_TIME_TYPE DEADLINE_TIME;
    PRIORITY_TYPE CURRENT_PRIORITY;
    PROCESS_STATE_TYPE PROCESS_STATE;
    PROCESS_ATTRIBUTE_TYPE ATTRIBUTES;
} PROCESS_STATUS_TYPE;

/**
 * The \a CREATE_PROCESS service request creates a process and returns an
 * identifier that denotes the created process. For each partition, as many
 * processes as the pre-allocated memory space will support can be created.
 * The creation of processes (e.g., names used, number of processes) for one
 * partition has no impact on the creation of processes for other partitions.
 * 
 * \param  [in]                 ATTRIBUTES
 * \param  [out]                PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       Insufficient storage capacity for the creation
 *                              of the specified process or maximum number of
 *                              processes have been created
 * \retval NO_ACTION            The process named \p ATTRIBUTES.NAME is already
 *                              created
 * \retval INVALID_PARAM        \p ATTRIBUTES.STACK_SIZE out of range
 * \retval INVALID_PARAM        \p ATTRIBUTES.BASE_PRIORITY out of range
 * \retval INVALID_PARAM        \p ATTRIBUTES.PERIOD out of range
 * \retval INVALID_CONFIG       \p ATTRIBUTES.PERIOD is finite and
 *                              \p ATTRIBUTES.PERIOD is not an integer multiple
 *                              of partition period defined in the
 *                              configuration tables
 * \retval INVALID_PARAM        \p ATTRIBUTES.TIME_CAPACITY out of range
 * \retval INVALID_PARAM        \p ATTRIBUTES.PERIOD is finite and
 *                              \p ATTRIBUTES.TIME_CAPACITY is greater than
 *                              \p ATTRIBUTES.PERIOD
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_PROCESS(
        PROCESS_ATTRIBUTE_TYPE *ATTRIBUTES,
        PROCESS_ID_TYPE *PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SET_PRIORITY service request changes a process’s current priority. If
 * the process’s current state is ready, the process is considered as having
 * been in the ready state with the set priority for the shortest elapsed time
 * (i.e., other processes at the same priority eligible for the same processor
 * core(s) will be selected to run before this process).
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [in]                 PRIORITY
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 * \retval INVALID_PARAM        \p PRIORITY is out of range
 * \retval INVALID_MODE         The specified process is in the DORMANT state
 */
extern void SET_PRIORITY(
        PROCESS_ID_TYPE PROCESS_ID,
        PRIORITY_TYPE PRIORITY,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SUSPEND_SELF service request suspends the execution of the current
 * process, if aperiodic. The process remains suspended until the RESUME
 * service request is issued or the specified timeout value expires.
 * 
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Current process owns a mutex or current process
 *                              is error handler process
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         Process is periodic
 * \retval TIMED_OUT            The specified time-out is expired
 */
extern void SUSPEND_SELF(
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SUSPEND service request allows the current process to suspend the
 * execution of any aperiodic process except itself. The suspended process
 * remains suspended until resumed by another process. If the process is
 * pending in a queue at the time it is suspended, it is not removed from that
 * queue. When it is resumed, it will continue pending unless it has been
 * removed from the queue (by availability of the resource it was waiting on or
 * expiration of a time-out) before the end of its suspension. If the process
 * is waiting on a timed-wait time counter, the process will continue to wait
 * on the time counter when it is resumed unless the time counter has expired.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            Specified process has already been suspended
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process or identifies the current process
 * \retval INVALID_MODE         \p PROCESS_ID is a process that currently owns
 *                              a mutex or is waiting on a mutex’s queue
 * \retval INVALID_MODE         The state of the specified process is DORMANT
 *                              or FAULTED
 * \retval INVALID_MODE         Specified process is periodic
 */
extern void SUSPEND(
        PROCESS_ID_TYPE PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a RESUME service request allows the current process to resume a
 * previously suspended process (i.e., via \ref SUSPEND or \ref SUSPEND_SELF
 * services). The resumed process will become ready if it is not waiting on a
 * process queue (e.g., resource associated with a queuing port, buffer,
 * semaphore, event) or a time delay associated with the \ref TIMED_WAIT
 * service.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            Identified process is not a suspended process
 *                              and is not FAULTED
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process or identifies the current process
 * \retval INVALID_MODE         The state of the specified process is DORMANT
 * \retval INVALID_MODE         \p PROCESS_ID identifies a periodic process and
 *                              is not FAULTED
 */
extern void RESUME(
        PROCESS_ID_TYPE PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a STOP_SELF service request allows the current process to stop itself.
 * No return code is returned to the requesting process procedure.
 */
extern void STOP_SELF(void);

/**
 * The \a STOP service request makes a process ineligible for processor
 * resources until another process issues the \ref START service request.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process or identifies the current process
 * \retval NO_ACTION            The state of the specified process is DORMANT
 */
extern void STOP(
        PROCESS_ID_TYPE PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a START service request initializes all attributes of a process to
 * their default values and resets the runtime stack of the process. If the
 * partition is in the NORMAL mode, the process’ deadline expiration time and
 * next release point are calculated.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 * \retval INVALID_CONFIG       DEADLINE_TIME calculation is out of range
 * \retval NO_ACTION            The state of the specified process is not
 *                              DORMANT
 */
extern void START(
        PROCESS_ID_TYPE PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a DELAYED_START service request initializes all attributes of a process
 * to their default values, resets the runtime stack of the process, and places
 * the process into the waiting state, i.e., the specified process goes from
 * dormant to waiting. If the partition is in the normal operating mode, the
 * process’s release point is calculated with the specified delay time, and the
 * process’s deadline expiration time is also calculated.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [in]                 DELAY_TIME
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 * \retval INVALID_PARAM        \p DELAY_TIME is out of range
 * \retval INVALID_PARAM        \p DELAY_TIME is infinite
 * \retval INVALID_PARAM        The process is periodic and \p DELAY_TIME is
 *                              greater or equal to the period of the specified
 *                              process
 * \retval INVALID_CONFIG       DEADLINE_TIME calculation is out of range
 * \retval NO_ACTION            The state of the specified process is not
 *                              DORMANT
 */
extern void DELAYED_START(
        PROCESS_ID_TYPE PROCESS_ID,
        SYSTEM_TIME_TYPE DELAY_TIME,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a LOCK_PREEMPTION service request attempts to acquire the preemption
 * lock mutex created by the O/S within each partition in support of preemption
 * locking. For each partition, the O/S provides a predefined preemption lock
 * mutex that is accessed using the preemption services. The preemption lock
 * mutex is created with a queuing discipline of FIFO and with a priority that
 * prevents preemption by other processes, except by the error handler process.
 * If \ref GET_PROCESS_STATUS is invoked on a process that owns the preemption
 * lock mutex, the current priority of the process will be returned as the
 * maximum process priority.
 * 
 * \param  [out]                LOCK_LEVEL
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       Partition’s lock level value is equal to
 *                              MAX_LOCK_LEVEL
 * \retval NO_ACTION            Current process is the error handler process
 *                              or OPERATING_MODE is not NORMAL
 * \retval INVALID_MODE         Current process already owns a mutex
 */
extern void LOCK_PREEMPTION(
        LOCK_LEVEL_TYPE *LOCK_LEVEL,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a UNLOCK_PREEMPTION service request decrements the current lock level
 * of a partition. The process that has preemption locked can be preempted by
 * another process only when the partition’s lock level becomes zero (i.e.,
 * when preemption is unlocked).
 * 
 * \param  [out]                LOCK_LEVEL
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            The current process is the error handler
 *                              process or OPERATING_MODE is not NORMAL or no
 *                              process has preemption locked
 * \retval INVALID_MODE         The current process does not own the preemption
 *                              lock mutex
 */
extern void UNLOCK_PREEMPTION(
        LOCK_LEVEL_TYPE *LOCK_LEVEL,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_MY_ID service request returns the process identifier of the
 * current process.
 * 
 * \param  [out]                PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Current process has no identifier
 */
extern void GET_MY_ID(
        PROCESS_ID_TYPE *PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_PROCESS_ID service request allows a process to obtain a process
 * identifier by specifying the process name.
 * 
 * \param  [in]                 PROCESS_NAME
 * \param  [out]                PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current partition process named
 *                              \p PROCESS_NAME
 */
extern void GET_PROCESS_ID(
        PROCESS_NAME_TYPE PROCESS_NAME,
        PROCESS_ID_TYPE *PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_PROCESS_STATUS service request returns the current status of the
 * specified process. The current status of each of the individual processes of
 * a partition is available to all processes within that partition.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                PROCESS_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 */
extern void GET_PROCESS_STATUS(
        PROCESS_ID_TYPE PROCESS_ID,
        PROCESS_STATUS_TYPE *PROCESS_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a INITIALIZE_PROCESS_CORE_AFFINITY service initializes a process’s core
 * affinity. The process will be scheduled only on the processor core it has
 * been assigned an affinity to.
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [in]                 PROCESSOR_CORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 * \retval INVALID_CONFIG       \p PROCESSOR_CORE_ID does not identify a
 *                              processor core assigned to this partition
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void INITIALIZE_PROCESS_CORE_AFFINITY(
        PROCESS_ID_TYPE PROCESS_ID,
        PROCESSOR_CORE_ID_TYPE PROCESSOR_CORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_MY_PROCESSOR_CORE_ID service returns the processor core
 * identifier of the current process (may be called by the error handler
 * process).
 * 
 * \param  [out]                PROCESSOR_CORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GET_MY_PROCESSOR_CORE_ID(
        PROCESSOR_CORE_ID_TYPE *PROCESSOR_CORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */