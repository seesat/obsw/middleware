/**----------------------------------------------------------------------------
 * \file buffer.h
 * 
 * BUFFER constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Intrapartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_buffer
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_buffer
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_BUFFER
#define OSAL_APEX_BUFFER

#define MAX_NUMBER_OF_BUFFERS         SYSTEM_LIMIT_NUMBER_OF_BUFFERS

typedef NAME_TYPE BUFFER_NAME_TYPE;

typedef APEX_INTEGER BUFFER_ID_TYPE;

typedef struct
{
    MESSAGE_RANGE_TYPE NB_MESSAGE;
    MESSAGE_RANGE_TYPE MAX_NB_MESSAGE;
    MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} BUFFER_STATUS_TYPE;

/**
 * The \a CREATE_BUFFER service request is used to create a message buffer with
 * a maximum number of maximum size messages. A \p BUFFER_ID is assigned by the
 * O/S and returned to the calling process. Processes can create as many
 * buffers as the pre-allocated memory space will support. The
 * \p QUEUING_DISCIPLINE input parameter indicates the process queuing policy
 * (FIFO or priority order) associated with that buffer.
 * 
 * \param  [in]                 BUFFER_NAME
 * \param  [in]                 MAX_MESSAGE_SIZE
 * \param  [in]                 MAX_NB_MESSAGE
 * \param  [in]                 QUEUING_DISCIPLINE
 * \param  [out]                BUFFER_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is not enough available storage space for
 *                              the creation of the specified buffer
 * \retval NO_ACTION            The buffer named \p BUFFER_NAME has already
 *                              been created
 * \retval INVALID_PARAM        \p MAX_MESSAGE_SIZE is zero or negative
 * \retval INVALID_PARAM        \p MAX_NB_MESSAGE is out of range
 * \retval INVALID_PARAM        \p QUEUING_DISCIPLINE is invalid
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_BUFFER(
        BUFFER_NAME_TYPE BUFFER_NAME,
        MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE,
        MESSAGE_RANGE_TYPE MAX_NB_MESSAGE,
        QUEUING_DISCIPLINE_TYPE QUEUING_DISCIPLINE,
        BUFFER_ID_TYPE *BUFFER_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SEND_BUFFER service request is used to send a message in the
 * specified buffer. The calling process will be queued while the buffer is
 * full for a maximum duration of the specified time-out.
 * 
 * \param  [in]                 BUFFER_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BUFFER_ID does not identify an existing
 *                              buffer
 * \retval INVALID_PARAM        \p LENGTH is greater than MAX_MESSAGE_SIZE
 *                              specified for the buffer
 * \retval INVALID_PARAM        \p LENGTH is zero or negative
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         (Current process owns a mutex or current
 *                              process is the error handler process) and
 *                              \p TIME_OUT is not zero
 * \retval NOT_AVAILABLE        No place in the buffer to put a message
 * \retval TIMED_OUT            The specified \p TIME_OUT is expired
 */
extern void SEND_BUFFER(
        BUFFER_ID_TYPE BUFFER_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a RECEIVE_BUFFER service request is used to receive a message from the
 * specified buffer. The calling process will be queued while the buffer is
 * empty for a time-out maximum duration.
 * 
 * \param  [in]                 BUFFER_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                MESSAGE_ADDR
 * \param  [out]                LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BUFFER_ID does not identify an existing
 *                              buffer
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         (Current process owns a mutex or current
 *                              process is error handler process) and
 *                              \p TIME_OUT is not zero
 * \retval NOT_AVAILABLE        The buffer does not contain any message
 * \retval TIMED_OUT            The specified \p TIME_OUT expired
 */
extern void RECEIVE_BUFFER(
        BUFFER_ID_TYPE BUFFER_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE *LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_BUFFER_ID service request returns the buffer identifier that
 * corresponds to a buffer name.
 * 
 * \param  [in]                 BUFFER_NAME
 * \param  [out]                BUFFER_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition buffer named
 *                              \p BUFFER_NAME
 */
extern void GET_BUFFER_ID(
        BUFFER_NAME_TYPE BUFFER_NAME,
        BUFFER_ID_TYPE *BUFFER_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_BUFFER_STATUS service request returns the status of the specified
 * buffer.
 * 
 * \param  [in]                 BUFFER_ID
 * \param  [out]                BUFFER_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BUFFER_ID does not identify an existing
 *                              buffer
 */
extern void GET_BUFFER_STATUS(
        BUFFER_ID_TYPE BUFFER_ID,
        BUFFER_STATUS_TYPE *BUFFER_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
