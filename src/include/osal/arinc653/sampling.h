/**----------------------------------------------------------------------------
 * \file sampling.h
 * 
 * SAMPLING PORT constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Interpartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_sampling
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_sampling
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_APEX_SAMPLING
#define OSAL_APEX_SAMPLING

#define MAX_NUMBER_OF_SAMPLING_PORTS  SYSTEM_LIMIT_NUMBER_OF_SAMPLING_PORTS

typedef NAME_TYPE SAMPLING_PORT_NAME_TYPE;

typedef APEX_INTEGER SAMPLING_PORT_ID_TYPE;

typedef enum
{
    INVALID = 0,
    VALID = 1
} VALIDITY_TYPE;

typedef struct
{
    SYSTEM_TIME_TYPE REFRESH_PERIOD;
    MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE;
    PORT_DIRECTION_TYPE PORT_DIRECTION;
    VALIDITY_TYPE LAST_MSG_VALIDITY;
} SAMPLING_PORT_STATUS_TYPE;

/**
 * The \a CREATE_SAMPLING_PORT service request is used to create a sampling
 * port. An identifier is assigned by the O/S and returned to the calling
 * process. For a source port, the refresh period is meaningless. At creation,
 * the port is empty.
 * 
 * \param  [in]                 SAMPLING_PORT_NAME
 * \param  [in]                 MAX_MESSAGE_SIZE
 * \param  [in]                 PORT_DIRECTION
 * \param  [in]                 REFRESH_PERIOD
 * \param  [out]                SAMPLING_PORT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            The port named \p SAMPLING_PORT_NAME is already
 *                              created
 * \retval INVALID_CONFIG       Implementation-defined limit to sampling port
 *                              creation is exceeded
 * \retval INVALID_CONFIG       no sampling port of the partition is named
 *                              \p SAMPLING_PORT_NAME in the configuration
 *                              tables
 * \retval INVALID_CONFIG       \p MAX_MESSAGE_SIZE is zero, negative, or is
 *                              not equal to the value specified in the
 *                              configuration tables
 * \retval INVALID_CONFIG       \p PORT_DIRECTION is invalid or is not equal to
 *                              the value specified in the configuration tables
 * \retval INVALID_CONFIG       \p REFRESH_PERIOD is out of range
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_SAMPLING_PORT(
        SAMPLING_PORT_NAME_TYPE SAMPLING_PORT_NAME,
        MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE,
        PORT_DIRECTION_TYPE PORT_DIRECTION,
        SYSTEM_TIME_TYPE REFRESH_PERIOD,
        SAMPLING_PORT_ID_TYPE *SAMPLING_PORT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a WRITE_SAMPLING_MESSAGE service request is used to write a message in
 * the specified sampling port. The message overwrites the previous one.
 * 
 * \param  [in]                 SAMPLING_PORT_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p SAMPLING_PORT_ID does not identify an
 *                              existing sampling port
 * \retval INVALID_CONFIG       \p LENGTH is greater than MAX_MESSAGE_SIZE for
 *                              the specified port
 * \retval INVALID_PARAM        \p LENGTH is zero or negative
 * \retval INVALID_MODE         The specified port is not configured to operate
 *                              as a source
 */
extern void WRITE_SAMPLING_MESSAGE(
        SAMPLING_PORT_ID_TYPE SAMPLING_PORT_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a READ_SAMPLING_MESSAGE service request is used to read a message from
 * the specified sampling port. A validity output parameter indicates whether
 * the age of the read message is consistent with the required refresh period
 * attribute of the port.
 * 
 * \param  [in]                 SAMPLING_PORT_ID
 * \param  [out]                MESSAGE_ADDR
 * \param  [out]                LENGTH
 * \param  [out]                VALIDITY
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            Sampling port is empty
 * \retval INVALID_PARAM        \p SAMPLING_PORT_ID does not identify an
 *                              existing sampling port
 * \retval INVALID_MODE         The specified port is not configured to operate
 *                              as a destination
 */
extern void READ_SAMPLING_MESSAGE(
        SAMPLING_PORT_ID_TYPE SAMPLING_PORT_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE *LENGTH,
        VALIDITY_TYPE *VALIDITY,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_SAMPLING_PORT_ID service request returns the sampling port
 * identifier that corresponds to a sampling port name.
 * 
 * \param  [in]                 SAMPLING_PORT_NAME
 * \param  [out]                SAMPLING_PORT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition sampling port
 *                              named \p SAMPLING_PORT_NAME
 */
extern void GET_SAMPLING_PORT_ID(
        SAMPLING_PORT_NAME_TYPE SAMPLING_PORT_NAME,
        SAMPLING_PORT_ID_TYPE *SAMPLING_PORT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_SAMPLING_PORT_STATUS service returns the current status of the
 * specified sampling port.
 * 
 * \param  [in]                 SAMPLING_PORT_ID
 * \param  [out]                SAMPLING_PORT_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p SAMPLING_PORT_ID does not identify an
 *                              existing sampling port
 */
extern void GET_SAMPLING_PORT_STATUS(
        SAMPLING_PORT_ID_TYPE SAMPLING_PORT_ID,
        SAMPLING_PORT_STATUS_TYPE *SAMPLING_PORT_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */