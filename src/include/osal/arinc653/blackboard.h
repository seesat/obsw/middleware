/**----------------------------------------------------------------------------
 * \file blackboard.h
 * 
 * BLACKBOARD constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Intrapartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_blackboard
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_blackboard
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_BLACKBOARD
#define OSAL_APEX_BLACKBOARD

#define MAX_NUMBER_OF_BLACKBOARDS     SYSTEM_LIMIT_NUMBER_OF_BLACKBOARDS

typedef NAME_TYPE BLACKBOARD_NAME_TYPE;

typedef APEX_INTEGER BLACKBOARD_ID_TYPE;

typedef enum
{
    EMPTY = 0,
    OCCUPIED = 1
} EMPTY_INDICATOR_TYPE;

typedef struct
{
    EMPTY_INDICATOR_TYPE EMPTY_INDICATOR;
    MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} BLACKBOARD_STATUS_TYPE;

/**
 * The \a CREATE_BLACKBOARD service request is used to create a blackboard with
 * a specified maximum message size. A BLACKBOARD_ID is assigned by the O/S and
 * returned to the calling process. Processes can create as many blackboards as
 * the pre-allocated memory space will support.
 * 
 * \param  [in]                 BLACKBOARD_NAME
 * \param  [in]                 MAX_MESSAGE_SIZE
 * \param  [out]                BLACKBOARD_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is not enough available storage space for
 *                              the creation of the specified blackboard or
 *                              maximum number of blackboards has been created
 * \retval NO_ACTION            The blackboard named \p BLACKBOARD_NAME has
 *                              already been created
 * \retval INVALID_PARAM        \p MAX_MESSAGE_SIZE is zero or negative
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_BLACKBOARD(
        BLACKBOARD_NAME_TYPE BLACKBOARD_NAME,
        MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE,
        BLACKBOARD_ID_TYPE *BLACKBOARD_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a DISPLAY_BLACKBOARD service request is used to display a message in
 * the specified blackboard. The specified blackboard becomes not empty. If
 * processes were waiting on the empty blackboard, the processes will be
 * released on a priority followed by FIFO (when priorities are equal) basis.
 * 
 * \param  [in]                 BLACKBOARD_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BLACKBOARD_ID does not identify an existing
 *                              blackboard
 * \retval INVALID_PARAM        \p LENGTH is greater than MAX_MESSAGE_SIZE
 *                              specified for the blackboard
 * \retval INVALID_PARAM        \p LENGTH is zero or negative
 */
extern void DISPLAY_BLACKBOARD(
        BLACKBOARD_ID_TYPE BLACKBOARD_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a READ_BLACKBOARD service request is used to read a message in the
 * specified blackboard. The calling process will be in waiting state while the
 * blackboard is empty for up to the specified \p TIME_OUT duration.
 * 
 * \param  [in]                 BLACKBOARD_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                MESSAGE_ADDR
 * \param  [out]                LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BLACKBOARD_ID does not identify an existing
 *                              blackboard
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         (Current process owns a mutex or current
 *                              process is the error handler process) and
 *                              \p TIME_OUT is not zero
 * \retval NOT_AVAILABLE        No message in the blackboard
 * \retval TIMED_OUT            The specified \p TIME_OUT expired
 */
extern void READ_BLACKBOARD(
        BLACKBOARD_ID_TYPE BLACKBOARD_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE *LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CLEAR_BLACKBOARD service request is used to clear the message of the
 * specified blackboard. The specified blackboard becomes empty.
 * 
 * \param  [in]                 BLACKBOARD_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BLACKBOARD_ID does not identify an existing
 *                              blackboard
 */
extern void CLEAR_BLACKBOARD(
        BLACKBOARD_ID_TYPE BLACKBOARD_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_BLACKBOARD_ID service request returns the blackboard identifier
 * that corresponds to a blackboard name.
 * 
 * \param  [in]                 BLACKBOARD_NAME
 * \param  [out]                BLACKBOARD_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition blackboard named
 *                              \p BLACKBOARD_NAME
 */
extern void GET_BLACKBOARD_ID(
        BLACKBOARD_NAME_TYPE BLACKBOARD_NAME,
        BLACKBOARD_ID_TYPE *BLACKBOARD_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_BLACKBOARD_STATUS service request returns the status of the
 * specified blackboard.
 * 
 * \param  [in]                 BLACKBOARD_ID
 * \param  [out]                BLACKBOARD_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BLACKBOARD_ID does not identify an existing
 *                              blackboard
 */
extern void GET_BLACKBOARD_STATUS(
        BLACKBOARD_ID_TYPE BLACKBOARD_ID,
        BLACKBOARD_STATUS_TYPE *BLACKBOARD_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */