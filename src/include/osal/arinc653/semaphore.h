/**----------------------------------------------------------------------------
 * \file semaphore.h
 * 
 * SEMAPHORE constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Intrapartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_semaphore
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_semaphore
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_SEMAPHORE
#define OSAL_APEX_SEMAPHORE

#define MAX_NUMBER_OF_SEMAPHORES      SYSTEM_LIMIT_NUMBER_OF_SEMAPHORES

#define MAX_SEMAPHORE_VALUE           32767

typedef NAME_TYPE SEMAPHORE_NAME_TYPE;

typedef APEX_INTEGER SEMAPHORE_ID_TYPE;

typedef APEX_INTEGER SEMAPHORE_VALUE_TYPE;

typedef struct
{
    SEMAPHORE_VALUE_TYPE CURRENT_VALUE;
    SEMAPHORE_VALUE_TYPE MAXIMUM_VALUE;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} SEMAPHORE_STATUS_TYPE;

/**
 * The \a CREATE_SEMAPHORE service request is used to create a semaphore of a
 * specified current and maximum value. The maximum value parameter is the
 * maximum value that the semaphore can be signaled to (e.g., number of
 * resources managed by the semaphore). The current value is the semaphores’
 * starting value after creation. For example, if the semaphore was used to
 * manage access to five resources, and at the time of creation three resources
 * were available, the semaphore would be created with a maximum value of five
 * and a current value of three. The \p QUEUING_DISCIPLINE input parameter
 * indicates the process queuing policy (FIFO or priority order) associated
 * with the semaphore.
 * 
 * \param  [in]                 SEMAPHORE_NAME
 * \param  [in]                 CURRENT_VALUE
 * \param  [in]                 MAXIMUM_VALUE
 * \param  [in]                 QUEUING_DISCIPLINE
 * \param  [out]                SEMAPHORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The maximum number of semaphores has been
 *                              created
 * \retval NO_ACTION            The semaphore named \p SEMAPHORE_NAME has
 *                              already been created
 * \retval INVALID_PARAM        \p CURRENT_VALUE is out of range
 * \retval INVALID_PARAM        \p MAXIMUM_VALUE is out of range
 * \retval INVALID_PARAM        \p CURRENT_VALUE is greater than
 *                              \p MAXIMUM_VALUE
 * \retval INVALID_PARAM        \p QUEUING_DISCIPLINE is invalid
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_SEMAPHORE(
        SEMAPHORE_NAME_TYPE SEMAPHORE_NAME,
        SEMAPHORE_VALUE_TYPE CURRENT_VALUE,
        SEMAPHORE_VALUE_TYPE MAXIMUM_VALUE,
        QUEUING_DISCIPLINE_TYPE QUEUING_DISCIPLINE,
        SEMAPHORE_ID_TYPE *SEMAPHORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a WAIT_SEMAPHORE service request is used to attempt to acquire
 * (i.e., decrements) one count of a semaphore’s value (e.g., request for one
 * resource managed by the semaphore). The service moves the current process
 * from the running state to the waiting state if the current value of the
 * specified semaphore is zero and if the specified time-out is not zero. If
 * the current value of the specified semaphore is positive, the process goes
 * on executing and the semaphore’s current value is decremented.
 * 
 * \param  [in]                 SEMAPHORE_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p SEMAPHORE_ID does not identify an existing
 *                              semaphore
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         (Current process owns a mutex or current
 *                              process is the error handler process) and
 *                              \p TIME_OUT is not zero
 * \retval NOT_AVAILABLE        Current value of \p SEMAPHORE_ID <= 0 and
 *                              \p TIME_OUT = 0
 * \retval TIMED_OUT            The specified \p TIME_OUT expired
 */
extern void WAIT_SEMAPHORE(
        SEMAPHORE_ID_TYPE SEMAPHORE_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * If the semaphore’s value is not equal to its maximum, the
 * \a SIGNAL_SEMAPHORE service request increments the current value of the
 * specified semaphore (e.g., the current process completed its usage of one
 * resource managed by the semaphore). If processes are waiting on that
 * semaphore, the first process of the queue is moved from the waiting state to
 * the ready state and a scheduling takes place.
 * 
 * \param  [in]                 SEMAPHORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p SEMAPHORE_ID does not identify an existing
 *                              semaphore
 * \retval NO_ACTION            The semaphore’s current value equals its
 *                              maximum value
 */
extern void SIGNAL_SEMAPHORE(
        SEMAPHORE_ID_TYPE SEMAPHORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_SEMAPHORE_ID service request returns the semaphore identifier
 * that corresponds to a semaphore name.
 * 
 * \param  [in]                 SEMAPHORE_NAME
 * \param  [out]                SEMAPHORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition semaphore named
 *                              \p SEMAPHORE_NAME
 */
extern void GET_SEMAPHORE_ID(
        SEMAPHORE_NAME_TYPE SEMAPHORE_NAME,
        SEMAPHORE_ID_TYPE *SEMAPHORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_SEMAPHORE_STATUS service request returns the status of the
 * specified semaphore.
 * 
 * \param  [in]                 SEMAPHORE_ID
 * \param  [out]                SEMAPHORE_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p SEMAPHORE_ID does not identify an existing
 *                              semaphore
 */
extern void GET_SEMAPHORE_STATUS(
        SEMAPHORE_ID_TYPE SEMAPHORE_ID,
        SEMAPHORE_STATUS_TYPE *SEMAPHORE_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
