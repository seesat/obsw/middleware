/**----------------------------------------------------------------------------
 * \file mutex.h
 * 
 * MUTEX constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Intrapartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_mutex
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_mutex
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_MUTEX
#define OSAL_APEX_MUTEX

#define MAX_NUMBER_OF_MUTEXES         SYSTEM_LIMIT_NUMBER_OF_MUTEXES

typedef NAME_TYPE MUTEX_NAME_TYPE;

typedef APEX_INTEGER MUTEX_ID_TYPE;
#define NO_MUTEX_OWNED                -2
#define PREEMPTION_LOCK_MUTEX         -3

typedef APEX_INTEGER LOCK_COUNT_TYPE;

typedef enum
{
    AVAILABLE = 0,
    OWNED = 1
} MUTEX_STATE_TYPE;

typedef struct
{
    MUTEX_STATE_TYPE MUTEX_STATE;
    PROCESS_ID_TYPE MUTEX_OWNER;
    PRIORITY_TYPE MUTEX_PRIORITY;
    LOCK_COUNT_TYPE LOCK_COUNT;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} MUTEX_STATUS_TYPE;

/**
 * The \a CREATE_MUTEX service request is used to create a mutex with a
 * specified process priority value. When the mutex is acquired, the priority
 * of the process is raised to the priority specified for the mutex. The
 * \p QUEUING_DISCIPLINE input parameter indicates the process queuing policy
 * (FIFO or priority order) associated with the mutex.
 * 
 * \param  [in]                 MUTEX_NAME
 * \param  [in]                 MUTEX_PRIORITY
 * \param  [in]                 QUEUING_DISCIPLINE
 * \param  [out]                MUTEX_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The maximum number of mutexes has been created
 * \retval NO_ACTION            The mutex named \p MUTEX_NAME has already been
 *                              created
 * \retval INVALID_PARAM        \p MUTEX_PRIORITY is out of range
 * \retval INVALID_PARAM        \p QUEUING_DISCIPLINE is invalid
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_MUTEX(
        MUTEX_NAME_TYPE MUTEX_NAME,
        PRIORITY_TYPE MUTEX_PRIORITY,
        QUEUING_DISCIPLINE_TYPE QUEUING_DISCIPLINE,
        MUTEX_ID_TYPE *MUTEX_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a ACQUIRE_MUTEX service request is used to attempt to acquire the
 * mutex. If the mutex is not already owned, the current priority of the
 * process is retained, the priority of the process is raised to the priority
 * assigned to the mutex, the mutex is set to an owned state, and the process
 * continues executing. If the mutex is already owned by another process (and a
 * timeout is specified), the service moves the current process from the
 * running state to the waiting state.
 * 
 * \param  [in]                 MUTEX_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p MUTEX_ID does not identify an existing mutex
 * \retval INVALID_PARAM        \p MUTEX_ID is equal to PREEMPTION_LOCK_MUTEX
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         current process already owns a different mutex
 * \retval INVALID_MODE         current process is the error handler process
 * \retval INVALID_MODE         current process’s priority is greater than the
 *                              mutex priority
 * \retval NOT_AVAILABLE        The mutex is currently owned and
 *                              \p TIME_OUT = 0
 * \retval TIMED_OUT            The specified \p TIME_OUT expired
 * \retval INVALID_CONFIG       mutex’s lock count is equal to MAX_LOCK_LEVEL
 */
extern void ACQUIRE_MUTEX(
        MUTEX_ID_TYPE MUTEX_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * If the current process owns the mutex, the \a RELEASE_MUTEX service
 * decreases the mutex’s lock count. If the lock count is zero, the process is
 * released from ownership of the mutex and process’s priority is restored to
 * the value retained for the process (with the process being positioned as if
 * it was running at this priority for the longest time). If processes are
 * waiting on that mutex, the first process of the queue is moved from the
 * waiting state to the ready state, this process is set as owner of the mutex,
 * the process’s priority is raised to the priority assigned to the mutex, and
 * a scheduling takes place.
 * 
 * \param  [in]                 MUTEX_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p MUTEX_ID does not identify an existing mutex
 * \retval INVALID_PARAM        \p MUTEX_ID is equal to PREEMPTION_LOCK_MUTEX
 * \retval INVALID_MODE         the current process does not own the specified
 *                              mutex
 */
extern void RELEASE_MUTEX(
        MUTEX_ID_TYPE MUTEX_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * If the identified process owns the mutex, the \a RESET_MUTEX service
 * releases a process from ownership of the mutex and restores the process’s
 * priority to the retained value (with the process being positioned as if it
 * was running at this priority for the longest time). If processes are waiting
 * on that mutex, the first process of the queue is moved from the waiting
 * state to the ready state, this process is set as owner of the mutex, the
 * process’s priority is raised to the priority assigned to the mutex, and a
 * scheduling takes place.
 * 
 * \param  [in]                 MUTEX_ID
 * \param  [in]                 PROCESS_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p MUTEX_ID does not identify an existing mutex
 * \retval INVALID_PARAM        \p MUTEX_ID is equal to PREEMPTION_LOCK_MUTEX
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 * \retval INVALID_MODE         the identified process does not own the
 *                              specified mutex
 */
extern void RESET_MUTEX(
        MUTEX_ID_TYPE MUTEX_ID,
        PROCESS_ID_TYPE PROCESS_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_MUTEX_ID service request returns the mutex identifier that
 * corresponds to a mutex name.
 * 
 * \param  [in]                 MUTEX_NAME
 * \param  [out]                MUTEX_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition mutex named
 *                              \p MUTEX_NAME
 */
extern void GET_MUTEX_ID(
        MUTEX_NAME_TYPE MUTEX_NAME,
        MUTEX_ID_TYPE *MUTEX_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_MUTEX_STATUS service request returns the status of the specified
 * mutex.
 * 
 * \param  [in]                 MUTEX_ID
 * \param  [out]                MUTEX_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p MUTEX_ID does not identify an existing mutex
 * \retval INVALID_PARAM        \p MUTEX_ID is equal to PREEMPTION_LOCK_MUTEX
 */
extern void GET_MUTEX_STATUS(
        MUTEX_ID_TYPE MUTEX_ID,
        MUTEX_STATUS_TYPE *MUTEX_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_PROCESS_ MUTEX_STATE service is used (e.g., by the process level
 * error handler) to identify whether a process owns a mutex (including the
 * partition’s preemption lock mutex).
 * 
 * \param  [in]                 PROCESS_ID
 * \param  [out]                MUTEX_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p PROCESS_ID does not identify an existing
 *                              process
 */
extern void GET_PROCESS_MUTEX_STATE(
        PROCESS_ID_TYPE PROCESS_ID,
        MUTEX_ID_TYPE *MUTEX_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
