/**----------------------------------------------------------------------------
 * \file event.h
 * 
 * EVENT constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Intrapartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_event
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_event
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_EVENT
#define OSAL_APEX_EVENT

#define MAX_NUMBER_OF_EVENTS          SYSTEM_LIMIT_NUMBER_OF_EVENTS

typedef NAME_TYPE EVENT_NAME_TYPE;

typedef APEX_INTEGER EVENT_ID_TYPE;

typedef enum
{
    DOWN = 0,
    UP = 1
} EVENT_STATE_TYPE;

typedef struct
{
    EVENT_STATE_TYPE EVENT_STATE;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} EVENT_STATUS_TYPE;

/**
 * The \a CREATE_EVENT service request creates an event object for use by any
 * of the process in the partition. Upon creation the event is set to the down
 * state.
 * 
 * \param  [in]                 EVENT_NAME
 * \param  [out]                EVENT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The maximum number of events has been created
 * \retval NO_ACTION            The event named \p EVENT_NAME has already been
 *                              created
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_EVENT(
        EVENT_NAME_TYPE EVENT_NAME,
        EVENT_ID_TYPE *EVENT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SET_EVENT service request sets the specified event to the up state.
 * All the processes waiting on the event are moved from the waiting state to
 * the ready state (unless suspended while waiting on the event), and a
 * scheduling takes place. If processes were waiting on the event, all the
 * processes waiting on the event will be released on a priority followed by
 * FIFO (when priorities are equal) basis.
 * 
 * \param  [in]                 EVENT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p EVENT_ID does not identify an existing event
 */
extern void SET_EVENT(
        EVENT_ID_TYPE EVENT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a RESET_EVENT service request sets the specified event in down state.
 * 
 * \param  [in]                 EVENT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p EVENT_ID does not identify an existing event
 */
extern void RESET_EVENT(
        EVENT_ID_TYPE EVENT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a WAIT_EVENT service request moves the current process from the running
 * state to the waiting state if the specified event is down and if the
 * specified time-out is not zero. The process goes on executing if the
 * specified event is up or if it is a conditional wait (event down and
 * time-out is zero).
 * 
 * \param  [in]                 EVENT_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p EVENT_ID does not identify an existing event
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         (Current process owns a mutex or current
 *                              process is the error handler process) and
 *                              \p TIME_OUT is not zero
 * \retval NOT_AVAILABLE        The event is in the DOWN state
 * \retval TIMED_OUT            The specified \p TIME_OUT expired
 */
extern void WAIT_EVENT(
        EVENT_ID_TYPE EVENT_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_EVENT_ID service request returns the event identifier that
 * corresponds to an event name.
 * 
 * \param  [in]                 EVENT_NAME
 * \param  [out]                EVENT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition event named
 *                              \p EVENT_NAME
 */
extern void GET_EVENT_ID(
        EVENT_NAME_TYPE EVENT_NAME,
        EVENT_ID_TYPE *EVENT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_EVENT_STATUS service request returns the status of the specified
 * event.
 * 
 * \param  [in]                 EVENT_ID
 * \param  [out]                EVENT_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p EVENT_ID does not identify an existing event
 */
extern void GET_EVENT_STATUS(
        EVENT_ID_TYPE EVENT_ID,
        EVENT_STATUS_TYPE *EVENT_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
