/**----------------------------------------------------------------------------
 * \file partition.h
 * 
 * PARTITION constant and type definitions and management services   
 * 
 * \see ARINC 653 PART 1: Required Services - Partition Management
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_partition
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_partition
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_PARTITION
#define OSAL_APEX_PARTITION

#define MAX_NUMBER_OF_PARTITIONS SYSTEM_LIMIT_NUMBER_OF_PARTITIONS

typedef enum
{
    IDLE = 0,
    COLD_START = 1,
    WARM_START = 2,
    NORMAL = 3
} OPERATING_MODE_TYPE;

typedef APEX_INTEGER PARTITION_ID_TYPE;

typedef APEX_UNSIGNED NUM_CORES_TYPE;

typedef enum
{
    NORMAL_START = 0,
    PARTITION_RESTART = 1,
    HM_MODULE_RESTART = 2,
    HM_PARTITION_RESTART = 3
} START_CONDITION_TYPE;

typedef struct
{
    SYSTEM_TIME_TYPE PERIOD;
    SYSTEM_TIME_TYPE DURATION;
    PARTITION_ID_TYPE IDENTIFIER;
    LOCK_LEVEL_TYPE LOCK_LEVEL;
    OPERATING_MODE_TYPE OPERATING_MODE;
    START_CONDITION_TYPE START_CONDITION;
    NUM_CORES_TYPE NUM_ASSIGNED_CORES;
} PARTITION_STATUS_TYPE;

/**
 * The \a GET_PARTITION_STATUS service request is used to obtain the status of
 * the current partition.
 * 
 * \param  [out]                PARTITION_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GET_PARTITION_STATUS(
        PARTITION_STATUS_TYPE *PARTITION_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SET_PARTITION_MODE service request is used to set the operating mode
 * of the current partition to normal after the application portion of the
 * initialization of the partition is complete. The service is also used for
 * setting the partition back to idle (partition shutdown) and to cold start or
 * warm start (partition restart) when a fault is detected and processed.
 * 
 * \param  [in]                 OPERATING_MODE
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p OPERATING_MODE does not represent an
 *                              existing mode
 * \retval NO_ACTION            \p OPERATING_MODE is normal and current mode is
 *                              NORMAL
 * \retval INVALID_MODE         \p OPERATING_MODE is WARM_START and current
 *                              mode is COLD_START
 */
extern void SET_PARTITION_MODE(
        OPERATING_MODE_TYPE OPERATING_MODE,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */