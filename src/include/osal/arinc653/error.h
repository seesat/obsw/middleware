/**----------------------------------------------------------------------------
 * \file error.h
 * 
 * ERROR constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Health Monitoring
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_error
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_error
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_ERROR
#define OSAL_APEX_ERROR

#define MAX_ERROR_MESSAGE_SIZE        128

typedef APEX_INTEGER ERROR_MESSAGE_SIZE_TYPE;

typedef APEX_BYTE ERROR_MESSAGE_TYPE[MAX_ERROR_MESSAGE_SIZE];

typedef enum
{
    DEADLINE_MISSED = 0,
    APPLICATION_ERROR = 1,
    NUMERIC_ERROR = 2,
    ILLEGAL_REQUEST = 3,
    STACK_OVERFLOW = 4,
    MEMORY_VIOLATION = 5,
    HARDWARE_FAULT = 6,
    POWER_FAIL = 7
} ERROR_CODE_TYPE;

typedef struct
{
    ERROR_CODE_TYPE ERROR_CODE;
    ERROR_MESSAGE_SIZE_TYPE LENGTH;
    PROCESS_ID_TYPE FAILED_PROCESS_ID;
    SYSTEM_ADDRESS_TYPE FAILED_ADDRESS;
    ERROR_MESSAGE_TYPE MESSAGE;
} ERROR_STATUS_TYPE;

typedef enum
{
    PROCESSES_PAUSE = 0,
    PROCESSES_SCHEDULED = 1
} ERROR_HANDLER_CONCURRENCY_CONTROL_TYPE;

/**
 * The \a REPORT_APPLICATION_MESSAGE service request allows the current
 * partition to transmit a message to the HM function.
 * \a REPORT_APPLICATION_MESSAGE may be used to record an event for logging
 * purposes.
 * 
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is out of range
 */
extern void REPORT_APPLICATION_MESSAGE(
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CREATE_ERROR_HANDLER service request creates an error handler process
 * for the current partition. This process has no identifier (ID) and cannot be
 * accessed by the other processes of the partition. The error handler is a
 * special aperiodic process with the highest priority, no deadline, and whose
 * entry point defined by this service is invoked by the O/S when a detected
 * error is evaluated as a process level error. It is executed during the
 * partition’s time windows. It cannot be suspended nor stopped by another
 * process. Its priority cannot be modified. The error handler is scheduled
 * even if a process has preemption locked (i.e., lock level not equal to
 * zero).
 * 
 * \param  [in]                 ENTRY_POINT
 * \param  [in]                 STACK_SIZE
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            Error handler is already created
 * \retval INVALID_CONFIG       insufficient storage capacity for the creation
 *                              of the error handler process
 * \retval INVALID_CONFIG       \p STACK_SIZE is out of range
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_ERROR_HANDLER(
        SYSTEM_ADDRESS_TYPE ENTRY_POINT,
        STACK_SIZE_TYPE STACK_SIZE,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_ERROR_STATUS service must be used by the error handler process to
 * determine the error code, the identifier of the faulty process, the address
 * at which the error occurs, and the message associated with the fault. Use of
 * the \a GET_ERROR_STATUS service does not change the state of the identified
 * faulty process.
 * 
 * \param  [out]                ERROR_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The current process is not the error handler
 * \retval NO_ACTION            There is no process in error
 */
extern void GET_ERROR_STATUS(
        ERROR_STATUS_TYPE *ERROR_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a RAISE_APPLICATION_ERROR service request allows a running process to
 * invoke the error handler process for the specific error code
 * APPLICATION_ERROR. When a partition is allocated two or more processor
 * cores, the invoking process may be eligible to continue running after its
 * message and error have been added to the partition’s error process list. If
 * the error process list cannot currently accommodate another application
 * error, the invoking process is blocked until the error process list can
 * accommodate the process’s application error message.
 * 
 * \param  [in]                 ERROR_CODE
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p LENGTH is negative or greater than
 *                              MAX_ERROR_MESSAGE_SIZE
 * \retval INVALID_PARAM        \p ERROR_CODE is not APPLICATION_ERROR
 */
extern void RAISE_APPLICATION_ERROR(
        ERROR_CODE_TYPE ERROR_CODE,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        ERROR_MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CONFIGURE_ERROR_HANDLER service request allows the current partition
 * to configure the behavior of how process scheduling of other processor cores
 * will be managed for the partition when the error handler process is
 * executing. This service configures whether processes on the other processor
 * cores run concurrently or do not run concurrently (i.e., pause) while the
 * error handler process is running. The default behavior is processes do not
 * run concurrently (i.e., PROCESSES_PAUSE). If the partition is allocated only
 * one processor core, the two behaviors are equivalent (i.e., configuration is
 * not necessary).
 * 
 * \param  [in]                 CONCURRENCY_CONTROL
 * \param  [in]                 PROCESSOR_CORE_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       The error handler process is not created
 * \retval INVALID_PARAM        \p CONCURRENCY_CONTROL is invalid
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CONFIGURE_ERROR_HANDLER(
        ERROR_HANDLER_CONCURRENCY_CONTROL_TYPE CONCURRENCY_CONTROL,
        PROCESSOR_CORE_ID_TYPE PROCESSOR_CORE_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */