/**----------------------------------------------------------------------------
 * \file time.h
 * 
 * TIME constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Time Management
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_time
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_time
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_APEX_TIME
#define OSAL_APEX_TIME

/**
 * The \a TIMED_WAIT service request suspends execution of the requesting
 * process for a minimum amount of elapsed time. A delay time of zero allows
 * round-robin scheduling of processes of the same priority. When delay time is
 * zero, all other ready processes with the current priority equal to the
 * current process’s value will be eligible to run before the current process.
 * 
 * \param  [in]                 DELAY_TIME
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Current process owns a mutex or current process
 *                              is error handler process
 * \retval INVALID_PARAM        \p DELAY_TIME is out of range
 * \retval INVALID_PARAM        \p DELAY_TIME is infinite
 */
extern void TIMED_WAIT(
        SYSTEM_TIME_TYPE DELAY_TIME,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a PERIODIC_WAIT service request suspends execution of the requesting
 * periodic process until the next release point in the processor time line
 * that corresponds to the period of the process.
 * 
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_MODE         Current process owns a mutex or current process
 *                              is error handler process
 * \retval INVALID_MODE         Current process is not periodic
 * \retval INVALID_CONFIG       DEADLINE_TIME calculation is out of range
 */
extern void PERIODIC_WAIT(
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The service \a GET_TIME requests the current value of the system clock. The
 * system clock is the value of a clock common to all partitions in the core
 * module.
 * 
 * \param  [out]                SYSTEM_TIME
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 */
extern void GET_TIME(
        SYSTEM_TIME_TYPE *SYSTEM_TIME,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a REPLENISH service request updates the deadline of the current process
 * with a specified \p BUDGET_TIME value.
 * 
 * \param  [in]                 BUDGET_TIME
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p BUDGET_TIME is out of range
 * \retval INVALID_MODE         Current process is periodic and new deadline
 *                              would exceed the next release point
 * \retval NO_ACTION            Process is error handler process or
 *                              OPERATING_MODE is not NORMAL
 */
extern void REPLENISH(
        SYSTEM_TIME_TYPE BUDGET_TIME,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */