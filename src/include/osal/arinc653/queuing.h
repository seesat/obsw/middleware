/**----------------------------------------------------------------------------
 * \file queuing.h
 * 
 * QUEUING PORT constant and type definitions and management services
 * 
 * \see ARINC 653 PART 1: Required Services - Interpartition Communication
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_queuing
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_queuing
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>
#include <osal/arinc653/process.h>

#ifndef OSAL_APEX_QUEUING
#define OSAL_APEX_QUEUING

#define MAX_NUMBER_OF_QUEUING_PORTS   SYSTEM_LIMIT_NUMBER_OF_QUEUING_PORTS

typedef NAME_TYPE QUEUING_PORT_NAME_TYPE;

typedef APEX_INTEGER QUEUING_PORT_ID_TYPE;

typedef struct
{
    MESSAGE_RANGE_TYPE NB_MESSAGE;
    MESSAGE_RANGE_TYPE MAX_NB_MESSAGE;
    MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE;
    PORT_DIRECTION_TYPE PORT_DIRECTION;
    WAITING_RANGE_TYPE WAITING_PROCESSES;
} QUEUING_PORT_STATUS_TYPE;

/**
 * The \a CREATE_QUEUING_PORT service is used to create a port of communication
 * operating in queuing mode. An identifier is assigned by the O/S and returned
 * to the calling process. At creation, the port is empty. The
 * \p QUEUING_DISCIPLINE attribute indicates whether blocked processes are
 * queued in FIFO, or in priority order.
 * 
 * \param  [in]                 QUEUING_PORT_NAME
 * \param  [in]                 MAX_MESSAGE_SIZE
 * \param  [in]                 MAX_NB_MESSAGE
 * \param  [in]                 PORT_DIRECTION
 * \param  [in]                 QUEUING_DISCIPLINE
 * \param  [out]                QUEUING_PORT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval NO_ACTION            Port named \p QUEUING_PORT_NAME is already
 *                              created
 * \retval INVALID_CONFIG       Implementation-defined limit to queuing port
 *                              creation is exceeded
 * \retval INVALID_CONFIG       no queuing port of the partition is named
 *                              \p QUEUING_PORT_NAME in the configuration
 *                              tables
 * \retval INVALID_CONFIG       \p MAX_MESSAGE_SIZE is zero, negative, or is
 *                              not equal to the value specified in the
 *                              configuration tables
 * \retval INVALID_CONFIG       \p MAX_NB_MESSAGE is out of range or is not
 *                              equal to the value specified in the
 *                              configuration tables
 * \retval INVALID_CONFIG       \p PORT_DIRECTION is invalid or is not equal to
 *                              the value specified in the configuration tables
 * \retval INVALID_CONFIG       \p QUEUING_DISCIPLINE is invalid
 * \retval INVALID_MODE         Operating mode is NORMAL
 */
extern void CREATE_QUEUING_PORT(
        QUEUING_PORT_NAME_TYPE QUEUING_PORT_NAME,
        MESSAGE_SIZE_TYPE MAX_MESSAGE_SIZE,
        MESSAGE_RANGE_TYPE MAX_NB_MESSAGE,
        PORT_DIRECTION_TYPE PORT_DIRECTION,
        QUEUING_DISCIPLINE_TYPE QUEUING_DISCIPLINE,
        QUEUING_PORT_ID_TYPE *QUEUING_PORT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a SEND_QUEUING_MESSAGE service request is used to send a message in the
 * specified queuing port. If there is sufficient space in the queuing port to
 * accept the message, the message is added to the end of the port’s message
 * queue. If there is insufficient space, the process is blocked and added to
 * the sending process queue, according to the queuing discipline of the port.
 * The process stays on the queue until the specified time-out, if finite,
 * expires or space becomes free in the port to accept the message.
 * 
 * \param  [in]                 QUEUING_PORT_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [in]                 TIME_OUT
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p QUEUING_PORT_ID does not identify an
 *                              existing queuing port
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_CONFIG       \p LENGTH is greater than MAX_MESSAGE_SIZE for
 *                              the port
 * \retval INVALID_PARAM        \p LENGTH is zero or negative
 * \retval INVALID_MODE         \p QUEUING_PORT_ID is not configured to operate
 *                              as a source
 * \retval NOT_AVAILABLE        Insufficient space in the \p QUEUING_PORT_ID to
 *                              accept a new message and specified \p TIME_OUT
 *                              is zero
 * \retval INVALID_MODE         (Current process owns a mutex or the current
 *                              process is the error handler process) and
 *                              specified \p TIME_OUT is not zero
 * \retval TIMED_OUT            Specified \a TIME_OUT expired
 */
extern void SEND_QUEUING_MESSAGE(
        QUEUING_PORT_ID_TYPE QUEUING_PORT_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        SYSTEM_TIME_TYPE TIME_OUT,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a RECEIVE_QUEUING_MESSAGE service request is used to receive a message
 * from the specified queuing port. If the queuing port is not empty, the
 * message at the head of the port’s message queue is removed and returned to
 * the calling process. If the queuing port is empty, the process is blocked
 * and added to the receiving process queue, according to the queuing
 * discipline of the port. The process stays on the queue until the specified
 * time-out, if finite, expires or a message arrives in the port.
 * 
 * \param  [in]                 QUEUING_PORT_ID
 * \param  [in]                 TIME_OUT
 * \param  [out]                MESSAGE_ADDR
 * \param  [out]                LENGTH
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval TIMED_OUT            Specified \p TIME_OUT expired
 * \retval INVALID_PARAM        \p QUEUING_PORT_ID does not identify an
 *                              existing queuing port
 * \retval INVALID_PARAM        \p TIME_OUT is out of range
 * \retval INVALID_MODE         The specified port is not configured to operate
 *                              as a destination port
 * \retval INVALID_CONFIG       The queue has overflowed since it was last read
 * \retval NOT_AVAILABLE        There is no message in the specified port and
 *                              the specified \p TIME_OUT is zero
 * \retval INVALID_MODE         (current process owns a mutex or current
 *                              process is the error handler process) and
 *                              \p TIME_OUT is not zero
 */
extern void RECEIVE_QUEUING_MESSAGE(
        QUEUING_PORT_ID_TYPE QUEUING_PORT_ID,
        SYSTEM_TIME_TYPE TIME_OUT,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE *LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_QUEUING_PORT_ID service request returns the queuing port
 * identifier that corresponds to a queuing port name.
 * 
 * \param  [in]                 QUEUING_PORT_NAME
 * \param  [out]                QUEUING_PORT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_CONFIG       There is no current-partition queuing port
 *                              named \p QUEUING_PORT_NAME
 */
extern void GET_QUEUING_PORT_ID(
        QUEUING_PORT_NAME_TYPE QUEUING_PORT_NAME,
        QUEUING_PORT_ID_TYPE *QUEUING_PORT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a GET_QUEUING_PORT_STATUS service request returns the current status of
 * the specified queuing port.
 * 
 * \param  [in]                 QUEUING_PORT_ID
 * \param  [out]                QUEUING_PORT_STATUS
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p QUEUING_PORT_ID does not identify an
 *                              existing queuing port
 */
extern void GET_QUEUING_PORT_STATUS(
        QUEUING_PORT_ID_TYPE QUEUING_PORT_ID,
        QUEUING_PORT_STATUS_TYPE *QUEUING_PORT_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE);

/**
 * The \a CLEAR_QUEUING_PORT service request discards any messages in the
 * specified port’s receive queue.
 * 
 * \param  [in]                 QUEUING_PORT_ID
 * \param  [out]                RETURN_CODE
 * 
 * \retval NO_ERROR             Successful completion
 * \retval INVALID_PARAM        \p QUEUING_PORT_ID does not identify an
 *                              existing queuing port
 * \retval INVALID_MODE         The specified port is not configured as a
 *                              destination port
 */
extern void CLEAR_QUEUING_PORT(
        QUEUING_PORT_ID_TYPE QUEUING_PORT_ID,
        RETURN_CODE_TYPE *RETURN_CODE);

#endif

/** \} */
