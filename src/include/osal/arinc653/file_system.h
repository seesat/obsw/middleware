/**----------------------------------------------------------------------------
 * \file file_system.h
 * 
 * File System constant and type definitions and management services
 * 
 * \see ARINC 653 PART 2: Extended Services - File System
 * \author Aeronautical Radio Incorporated (ARINC)
 * \ingroup osal_arinc653_file_system
 *---------------------------------------------------------------------------*/
/** \addtogroup osal_arinc653_file_system
 *  \{
 *---------------------------------------------------------------------------*/

#include <osal/arinc653/types.h>

#ifndef OSAL_APEX_FILE_SYSTEM
#define OSAL_APEX_FILE_SYSTEM

#define SYSTEM_LIMIT_FILE_SIZE        0x40000000      /**< file system scope
                                                       *   implementation
                                                       *   dependent */

#define MAX_FILE_SIZE                 SYSTEM_LIMIT_FILE_SIZE

#define MAX_FILE_NAME_LENGTH          512             /**< 511 char + null */

typedef char FILE_NAME_TYPE[MAX_FILE_NAME_LENGTH];

#define MAX_DIRECTORY_ENTRY_LENGTH    64              /**< 63 char + null */

typedef char DIRECTORY_ENTRY_TYPE[MAX_DIRECTORY_ENTRY_LENGTH];

typedef APEX_INTEGER FILE_ID_TYPE;

typedef APEX_INTEGER DIRECTORY_ID_TYPE;

typedef APEX_INTEGER FILE_SIZE_TYPE;  /**< 32-bit or 64-bit
                                       *   (\ref APEX_LONG_INTEGER)
                                       *   implementation dependent */

typedef APEX_INTEGER FILE_ERRNO_TYPE;

typedef enum
{
    READ = 0,
    READ_WRITE = 1
} FILE_MODE_TYPE;

typedef enum
{
    SEEK_SET = 0,
    SEEK_CUR = 1,
    SEEK_END = 2
} FILE_SEEK_TYPE;

typedef enum
{
    FILE_ENTRY = 0,
    DIRECTORY_ENTRY = 1,
    OTHER_ENTRY = 2,
    END_OF_DIRECTORY = 3
} ENTRY_KIND_TYPE;

typedef enum
{
    UNSET = 0,
    SET = 1
} TIME_SET_TYPE;

typedef enum
{
    VOLATILE = 0,
    NONVOLATILE = 1,
    REMOTE = 2
} MEDIA_TYPE;

typedef struct
{
    APEX_INTEGER TM_SEC;           /**< seconds after the minute [0,59] */
    APEX_INTEGER TM_MIN;           /**< minutes after the hour [0,59] */
    APEX_INTEGER TM_HOUR;          /**< hours since midnight [0,23] */
    APEX_INTEGER TM_MDAY;          /**< day of the month [1,31] */
    APEX_INTEGER TM_MON;           /**< months since January [0,11] */
    APEX_INTEGER TM_YEAR;          /**< years since 1900 */
    APEX_INTEGER TM_WDAY;          /**< days since Sunday [0,6] */
    APEX_INTEGER TM_YDAY;          /**< days since January 1 [0,365] */
    APEX_INTEGER TM_ISDST;         /**< Daylight Savings Time flag */
    TIME_SET_TYPE TM_IS_SET;       /**< time has been set */
} COMPOSITE_TIME_TYPE;

typedef struct
{
    COMPOSITE_TIME_TYPE CREATE_TIME;
    COMPOSITE_TIME_TYPE LAST_UPDATE;
    FILE_SIZE_TYPE POSITION;
    FILE_SIZE_TYPE SIZE;
    APEX_INTEGER NB_OF_CHANGES;
    APEX_INTEGER NB_OF_WRITE_ERRORS;
} FILE_STATUS_TYPE;

typedef struct
{
    APEX_LONG_INTEGER TOTAL_BYTES;
    APEX_LONG_INTEGER USED_BYTES;
    APEX_LONG_INTEGER FREE_BYTES;
    APEX_LONG_INTEGER MAX_ATOMIC_SIZE;
    APEX_INTEGER BLOCK_SIZE;
    FILE_MODE_TYPE ACCESS_RIGHTS;
    MEDIA_TYPE MEDIA;
} VOLUME_STATUS_TYPE;

/* ERRNO Error codes */
/* The values defined by the ERRNO constants are implementation dependent */
#define EPERM                1     /**< Operation not permitted */
#define ENOENT               2     /**< No such file or directory  */
#define EIO                  5     /**< Input/output error */
#define EBADF                9     /**< Bad file descriptor */
#define EACCES               13    /**< Permission denied */
#define EBUSY                16    /**< Resource busy */
#define EEXIST               17    /**< File exists */
#define ENOTDIR              20    /**< Not a volume or directory */
#define EISDIR               21    /**< Is a directory */
#define EINVAL               22    /**< Invalid argument */
#define EMFILE               24    /**< Too many open files by the current 
                                    *   partition */
#define EFBIG                27    /**< File too large */
#define ENOSPC               28    /**< No space left on volume */
#define EROFS                30    /**< Storage device containing file is
                                    *   currently write protected */
#define ENAMETOOLONG         78    /**< Filename too long */
#define EOVERFLOW            79    /**< Current file position beyond end
                                    *   of file  */
#define ENOTEMPTY            93    /**< Directory not empty */
#define ESTALE               151   /**< File or directory ID which an
                                    *   application has been using is no 
                                    *   longer valid */

/**
 * The \a OPEN_NEW_FILE service request creates and opens the file specified by
 * its absolute file name and provides the identifier for the new file to
 * permit subsequent read and write to the file. A partition must have write
 * permissions for the requested file’s volume in order to create the file.
 * Files created with this service do not already exist.
 * 
 * \param  [in]                 FILE_NAME
 * \param  [out]                FILE_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_CONFIG, EMFILE       Maximum number of open
 *                                      files/directories is reached by the
 *                                      current partition
 * \retval INVALID_PARAM,  ENAMETOOLONG \p FILE_NAME or one of its components
 *                                      exceed maximum character length
 * \retval INVALID_PARAM,  EINVAL       \p FILE_NAME is syntactically invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights for the volume
 *                                      that would contain \p FILE_NAME
 * \retval INVALID_PARAM,  EROFS        The storage device that would
 *                                      \p FILE_NAME is currently write
 *                                      protected
 * \retval INVALID_PARAM,  EEXIST       \p FILE_NAME is an existing file
 * \retval INVALID_PARAM,  EISDIR       \p FILE_NAME is an existing directory
 * \retval INVALID_CONFIG, ENOSPC       There is not enough space available on
 *                                      volume
 * \retval NOT_AVAILABLE,  EIO          Storage device containing \p FILE_NAME
 *                                      reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL)or the current
 *                                      process is the error handler
 * 
 * \note This service corresponds to the POSIX creat() function.
 */
extern void OPEN_NEW_FILE(
        FILE_NAME_TYPE FILE_NAME,
        FILE_ID_TYPE *FILE_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a OPEN_FILE service request opens the file specified by its full path
 * name and provides the identifier of the file to permit subsequent read and
 * write to the file. Files opened with this service must already exist.
 * 
 * \param  [in]                 FILE_NAME
 * \param  [in]                 FILE_MODE
 * \param  [out]                FILE_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_CONFIG, EMFILE       Maximum number of open
 *                                      files/directories is reached by the
 *                                      current partition
 * \retval INVALID_PARAM,  ENAMETOOLONG \p FILE_NAME or one of its components
 *                                      exceed maximum character length
 * \retval INVALID_PARAM,  EINVAL       \p FILE_NAME is syntactically invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  EINVAL       \p FILE_MODE does not represent an
 *                                      existing access mode
 * \retval INVALID_PARAM,  EISDIR       \p FILE_NAME is an existing directory
 * \retval INVALID_PARAM,  ENOENT       \p FILE_NAME is not an existing file
 * \retval INVALID_CONFIG, EACCES       \p FILE_NAME is an existing file
 *                                      currently open as read-write and
 *                                      \p FILE_MODE is read-write
 * \retval INVALID_PARAM,  EACCES       The current partition does not have the
 *                                      access rights represented by
 *                                      \p FILE_MODE for the volume containing
 *                                      \p FILE_NAME
 * \retval INVALID_PARAM,  EROFS        The storage device that would
 *                                      \p FILE_NAME is currently write
 *                                      protected and \p FILE_MODE is
 *                                      read-write
 * \retval NOT_AVAILABLE,  EIO          Storage device containing \p FILE_NAME
 *                                      reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL)or the current
 *                                      process is the error handler
 * 
 * \note This service corresponds to the POSIX open() function.
 */
extern void OPEN_FILE(
        FILE_NAME_TYPE FILE_NAME,
        FILE_MODE_TYPE FILE_MODE,
        FILE_ID_TYPE *FILE_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a CLOSE_FILE service request signals the end of read or write
 * activities. The associated file identifier is de-allocated.
 * 
 * \param  [in]                 FILE_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 *  
 * \note This service corresponds to the POSIX close() function.
 */
extern void CLOSE_FILE(
        FILE_ID_TYPE FILE_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a READ_FILE service request attempts to read a message of the
 * designated number of bytes pointed by the position indicator from the
 * specified file. The number of bytes actually read is returned. It can be
 * different from the number of bytes to read if End_Of_File is encountered
 * before having read all the specified bytes. No data transfer occurs past the
 * End_Of_File. If the starting position is at or after the End_Of_File, the
 * number of bytes read is zero. The position indicator is advanced by the
 * number of bytes read.
 * 
 * \param  [in]                 FILE_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 IN_LENGTH
 * \param  [out]                OUT_LENGTH
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval NOT_AVAILABLE,  ESTALE       \p FILE_ID is no longer valid due to
 *                                      the file owner’s action
 * \retval INVALID_PARAM,  EINVAL       \p IN_LENGTH is zero or negative
 * \retval INVALID_PARAM,  EFBIG        \p IN_LENGTH is greater than maximum
 *                                      atomicity
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval NOT_AVAILABLE,  EIO          I/O is in error
 * \retval NOT_AVAILABLE,  EOVERFLOW    file’POSITION is greater than file’SIZE
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX read() function.
 */
extern void READ_FILE(
        FILE_ID_TYPE FILE_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE IN_LENGTH,
        MESSAGE_SIZE_TYPE *OUT_LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a WRITE_FILE service request writes a message of the designated number
 * of bytes to the specified file at its position indicator. After the write
 * operation is complete, the file position indicator is advanced by the number
 * of bytes written. If this incremented position indicator is greater than the
 * file size, the size of the file is set to this position indicator. When a
 * composite time source is available and the file’s metadata supports last
 * update time, the file’s update time is set to the current composite time
 * value. When no composite time source is available, the update time’s
 * TM_IS_SET field is marked UNSET to indicate update time for this file is not
 * available.
 * 
 * \param  [in]                 FILE_ID
 * \param  [in]                 MESSAGE_ADDR
 * \param  [in]                 LENGTH
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval INVALID_PARAM,  EACCES       \p FILE_ID is not opened with
 *                                      read-write access mode
 * \retval INVALID_PARAM,  EINVAL       \p LENGTH is zero or negative
 * \retval INVALID_PARAM,  ENOSPC       Space required to allocate \p LENGTH
 *                                      bytes are not available for the volume
 * \retval INVALID_PARAM,  EFBIG        \p LENGTH is greater than maximum
 *                                      atomicity
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval NOT_AVAILABLE,  EIO          I/O is in error
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX write() function.
 */
extern void WRITE_FILE(
        FILE_ID_TYPE FILE_ID,
        MESSAGE_ADDR_TYPE MESSAGE_ADDR,
        MESSAGE_SIZE_TYPE LENGTH,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a SEEK_FILE service request sets the file position indicator according
 * to offset value and seek type. This file position indicator is the location
 * where subsequent reads or writes will take place.
 * 
 * \param  [in]                 FILE_ID
 * \param  [in]                 OFFSET
 * \param  [in]                 WHENCE
 * \param  [out]                POSITION
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval NOT_AVAILABLE,  ESTALE       \p FILE_ID is no longer valid due to
 *                                      the file owner’s action
 * \retval INVALID_PARAM,  EINVAL       \p WHENCE does not represent a defined
 *                                      type
 * \retval INVALID_PARAM,  EINVAL       \p WHENCE is SEEK_SET and \p OFFSET is
 *                                      not in the range 0..file'SIZE
 * \retval INVALID_PARAM,  EINVAL       \p WHENCE is SEEK_CUR and
 *                                      file'POSITION + \p OFFSET is not in the
 *                                      range 0..file'SIZE
 * \retval INVALID_PARAM,  EINVAL       \p WHENCE is SEEK_END and
 *                                      file'SIZE + \p OFFSET is not in the
 *                                      range 0..file'SIZE
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX lseek() function.
 */
extern void SEEK_FILE(
        FILE_ID_TYPE FILE_ID,
        FILE_SIZE_TYPE OFFSET,
        FILE_SEEK_TYPE WHENCE,
        FILE_SIZE_TYPE *POSITION,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a RESIZE_FILE service request changes the file size to a specified
 * number of bytes. This service causes the regular file referenced by the file
 * identifier to allow the extension (or contraction) of its size to a new
 * size.
 * 
 * \param  [in]                 FILE_ID
 * \param  [in]                 NEW_SIZE
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval INVALID_PARAM,  EACCES       \p FILE_ID is a file opened as
 *                                      read-only
 * \retval INVALID_PARAM,  EINVAL       \p NEW_SIZE is not a proper value or
 *                                      the resulting file offset would be
 *                                      invalid
 * \retval INVALID_PARAM,  ENOSPC       Allocating \p NEW_SIZE exceeds the
 *                                      blocks allocated to the volume
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX ftruncate() function.
 */
extern void RESIZE_FILE(
        FILE_ID_TYPE FILE_ID,
        FILE_SIZE_TYPE NEW_SIZE,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a SYNC_FILE service request causes data associated with the specified
 * file be transferred to the associated storage device. The service does not
 * return until the transfer completes or an error is reported. This provides
 * a means to potentially prevent data from being lost due to a power-loss.
 * Just before closing a file, the file system will also perform a sync.
 * 
 * \param  [in]                 FILE_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval INVALID_PARAM,  EACCES       \p FILE_ID is a file opened as
 *                                      read-only
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX fsync() function.
 */
extern void SYNC_FILE(
        FILE_ID_TYPE FILE_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a REMOVE_FILE service request removes a file. Once removed, the file is
 * no longer available to be opened.
 * 
 * \param  [in]                 FILE_NAME
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p FILE_NAME or one of its components
 *                                      exceed maximum character length
 * \retval INVALID_PARAM,  EINVAL       \p FILE_NAME is syntactically invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  EPERM        \p FILE_NAME is an existing directory
 * \retval INVALID_PARAM,  ENOENT       \p FILE_NAME is not an existing file
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights to the volume
 *                                      containing \p FILE_NAME
 * \retval NOT_AVAILABLE,  EBUSY        The file referenced by \p FILE_NAME has
 *                                      an operation in progress
 * \retval INVALID_PARAM,  EROFS        The storage device containing
 *                                      \p FILE_NAME is currently write
 *                                      protected
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_NAME is open for any access
 *                                      mode by the owning partition
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX unlink() function.
 */
extern void REMOVE_FILE(
        FILE_NAME_TYPE FILE_NAME,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a RENAME_FILE service request renames the existing file with the
 * specified new file name. A file can be given a new name in the same
 * directory or another directory on the same volume (cannot rename to a
 * different volume). The file’s data contents are unaffected by the rename
 * operation. Once renamed, the OLD_FILE_NAME name is no longer valid to open
 * the file.
 * 
 * \param  [in]                 OLD_FILE_NAME
 * \param  [in]                 NEW_FILE_NAME
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p OLD_FILE_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p OLD_FILE_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p OLD_FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  ENAMETOOLONG \p NEW_FILE_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p NEW_FILE_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p NEW_FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights to the volume
 *                                      containing \p OLD_FILE_NAME
 * \retval NOT_AVAILABLE,  EBUSY        \p OLD_FILE_NAME has an operation in
 *                                      progress
 * \retval INVALID_PARAM,  EROFS        The storage device containing
 *                                      \p OLD_FILE_NAME is currently write
 *                                      protected
 * \retval INVALID_PARAM,  EINVAL       The volume for \p OLD_FILE_NAME is not
 *                                      identical to the volume for
 *                                      \p NEW_FILE_NAME
 * \retval INVALID_CONFIG, ENOSPC       not able to create the file in the new
 *                                      directory (not enough resources or max
 *                                      number of files exceeded)
 * \retval INVALID_PARAM,  EPERM        \p OLD_FILE_NAME is an existing
 *                                      directory
 * \retval INVALID_PARAM,  EISDIR       \p NEW_FILE_NAME is an existing
 *                                      directory
 * \retval INVALID_PARAM,  ENOENT       \p OLD_FILE_NAME is not an existing
 *                                      file
 * \retval INVALID_PARAM,  EEXIST       \p NEW_FILE_NAME is an existing file
 * \retval NOT_AVAILABLE,  EBUSY        \p OLD_FILE_NAME is open for any access
 *                                      mode by the owning partition
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p OLD_FILE_NAME or \p NEW_FILE_NAME
 *                                      reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX rename() function.
 */
extern void RENAME_FILE(
        FILE_NAME_TYPE OLD_FILE_NAME,
        FILE_NAME_TYPE NEW_FILE_NAME,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a GET_FILE_STATUS service request obtains the statistics (e.g.,
 * position, size, etc.) of the specified file or file identifier. If the
 * file’s metadata does not support creation time or last update, the
 * corresponding TM_IS_SET field is returned UNSET to indicate its
 * unavailability.
 * 
 * \param  [in]                 FILE_ID
 * \param  [out]                FILE_STATUS
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p FILE_ID is not an open file
 *                                      identifier for the current partition
 * \retval NOT_AVAILABLE,  EBUSY        \p FILE_ID has an operation in progress
 * \retval NOT_AVAILABLE,  ESTALE       \p FILE_ID is no longer valid due to
 *                                      the file owner’s action
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX fstat() function.
 */
extern void GET_FILE_STATUS(
        FILE_ID_TYPE FILE_ID,
        FILE_STATUS_TYPE *FILE_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a GET_VOLUME_STATUS service request provides information about the
 * file system of the volume containing the specified file.
 * 
 * \param  [in]                 FILE_NAME
 * \param  [out]                VOLUME_STATUS
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p FILE_NAME or one of its components
 *                                      exceed maximum character length
 * \retval INVALID_PARAM,  EINVAL       \p FILE_NAME is syntactically invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p FILE_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  ENOENT       \p FILE_NAME is not an existing file,
 *                                      directory, or volume
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read access rights for the volume
 *                                      containing \p FILE_NAME
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p FILE_NAME reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX statvfs() function.
 */
extern void GET_VOLUME_STATUS(
        FILE_NAME_TYPE FILE_NAME,
        VOLUME_STATUS_TYPE *VOLUME_STATUS,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a OPEN_DIRECTORY service request opens the directory specified by its
 * full path name and provides the identifier of the directory to permit
 * subsequent read from the directory. Directories opened with this service
 * must already exist. The position indicator of the directory points to the
 * first directory entry.
 * 
 * \param  [in]                 DIRECTORY_NAME
 * \param  [out]                DIRECTORY_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_CONFIG, EMFILE       The maximum number of open
 *                                      files/directories is reached by the
 *                                      current partition
 * \retval INVALID_PARAM,  ENAMETOOLONG \p DIRECTORY_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p DIRECTORY_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p DIRECTORY_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  EEXIST       \p DIRECTORY_NAME is an existing file
 * \retval INVALID_PARAM,  ENOENT       \p DIRECTORY_NAME is not an existing
 *                                      directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read access rights for the volume
 *                                      containing \p DIRECTORY_NAME
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_NAME reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX opendir() function.
 */
extern void OPEN_DIRECTORY(
        FILE_NAME_TYPE DIRECTORY_NAME,
        DIRECTORY_ID_TYPE *DIRECTORY_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a CLOSE_DIRECTORY service request signals the end of read activities
 * and the associated directory identifier is de-allocated.
 * 
 * \param  [in]                 DIRECTORY_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p DIRECTORY_ID is not an open
 *                                      directory identifier for the current
 *                                      partition
 * \retval NOT_AVAILABLE,  EBUSY        \p DIRECTORY_ID has an operation in
 *                                      progress
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX closedir() function.
 */
extern void CLOSE_DIRECTORY(
        DIRECTORY_ID_TYPE DIRECTORY_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a READ_DIRECTORY service request attempts to read the directory entry
 * pointed by the position indicator from the specified directory and returns
 * its associated file name.
 * 
 * \param  [in]                 DIRECTORY_ID
 * \param  [out]                ENTRY_NAME
 * \param  [out]                ENTRY_KIND
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p DIRECTORY_ID is not an open
 *                                      directory identifier for the current
 *                                      partition
 * \retval NOT_AVAILABLE,  EBUSY        \p DIRECTORY_ID has an operation in
 *                                      progress
 * \retval NOT_AVAILABLE,  ESTALE       \p DIRECTORY_ID is no longer valid due
 *                                      to the directory owner’s action
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * \retval NO_ACTION,      ENAMETOOLONG the directory entry’s length exceeds
 *                                      maximum directory entry length
 * 
 * \note This service corresponds to the POSIX readdir() function.
 */
extern void READ_DIRECTORY (
        DIRECTORY_ID_TYPE DIRECTORY_ID,
        DIRECTORY_ENTRY_TYPE *ENTRY_NAME,
        ENTRY_KIND_TYPE *ENTRY_KIND,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO );

/**
 * The \a REWIND_DIRECTORY service positions the directory pointer to its first
 * entry for subsequent reads. \a REWIND_DIRECTORY will cause the directory
 * identifier to refer to the current state of the corresponding directory, as
 * if a call to \ref OPEN_DIRECTORY was performed.
 * 
 * \param  [in]                 DIRECTORY_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p DIRECTORY_ID is not an open
 *                                      directory identifier for the current
 *                                      partition
 * \retval NOT_AVAILABLE,  EBUSY        \p DIRECTORY_ID has an operation in
 *                                      progress
 * \retval NOT_AVAILABLE,  ESTALE       \p DIRECTORY_ID is no longer valid due
 *                                      to the directory owner’s action
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_ID reports a failure
 * \retval INVALID_PARAM,  EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX rewinddir() function.
 */
extern void REWIND_DIRECTORY(
        DIRECTORY_ID_TYPE DIRECTORY_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a MAKE_DIRECTORY service request creates the directory specified by its
 * full path name on the volume. A partition must have write permissions for
 * the volume in which the directory is to be created. Directories created with
 * this service cannot already exist.
 * 
 * \param  [in]                 DIRECTORY_NAME
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p DIRECTORY_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p DIRECTORY_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p DIRECTORY_NAME is not a volume or
 *                                      directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights for the volume
 *                                      that would contain \p DIRECTORY_NAME
 * \retval INVALID_PARAM,  EROFS        The storage device containing
 *                                      \p DIRECTORY_NAME is currently write
 *                                      protected
 * \retval INVALID_PARAM,  EISDIR       \p DIRECTORY_NAME is an existing
 *                                      directory
 * \retval INVALID_PARAM,  EEXIST       \p DIRECTORY_NAME is an existing file
 * \retval INVALID_CONFIG, ENOSPC       There is not enough space available on
 *                                      volume to make a directory
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_NAME reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX mkdir() function.
 */
extern void MAKE_DIRECTORY(
        FILE_NAME_TYPE DIRECTORY_NAME,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a REMOVE_DIRECTORY service request removes a directory. A partition
 * must have write permissions for the requested parent directory in order to
 * remove the directory. The directory must be empty of files and
 * sub-directories.
 * 
 * \param  [in]                 DIRECTORY_NAME
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p DIRECTORY_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p DIRECTORY_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p DIRECTORY_NAME is not a volume or
 *                                      directory
 * \retval INVALID_PARAM,  EPERM        \p DIRECTORY_NAME is an existing file
 * \retval INVALID_PARAM,  ENOENT       \p DIRECTORY_NAME is not an existing
 *                                      directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights for the volume
 *                                      containing \p DIRECTORY_NAME
 * \retval INVALID_PARAM,  EROFS        The storage device containing
 *                                      \p DIRECTORY_NAME is currently write
 *                                      protected
 * \retval INVALID_PARAM,  EACCES       \p DIRECTORY_NAME is the root directory
 * \retval INVALID_PARAM,  ENOTEMPTY    \p DIRECTORY_NAME is not empty of files
 *                                      and directories
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_NAME reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * \retval NOT_AVAILABLE,  EBUSY        \p DIRECTORY_NAME is open by the owning
 *                                      partition
 * 
 * \note This function corresponds to the POSIX rmdir() function.
 */
extern void REMOVE_DIRECTORY(
        FILE_NAME_TYPE DIRECTORY_NAME,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a SYNC_DIRECTORY service request causes data associated with the
 * specified directory to be transferred to the associated storage device. The
 * service does not return until the transfer completes or an error is
 * reported. This provides a means to potentially prevent data from being lost
 * due to a power-loss.
 * 
 * \param  [in]                 DIRECTORY_ID
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  EBADF        \p DIRECTORY_ID is not an open
 *                                      directory identifier for the current
 *                                      partition
 * \retval NOT_AVAILABLE,  EBUSY        \p DIRECTORY_ID has an operation in
 *                                      progress
 * \retval INVALID_PARAM,  EACCES       The current partition does not have
 *                                      read-write access rights for the volume
 *                                      that contains the directory identified
 *                                      by \p DIRECTORY_ID
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p DIRECTORY_ID reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service is modeled after the POSIX fsync() function.
 */
extern void SYNC_DIRECTORY(
        DIRECTORY_ID_TYPE DIRECTORY_ID,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

/**
 * The \a RENAME_DIRECTORY service request renames the existing directory with
 * the specified new directory name. A directory can be given a new directory
 * name that resides on the same volume (cannot rename to a different volume).
 * The directory’s contents are unaffected by the rename operation. Once
 * renamed, the OLD_DIRECTORY_NAME name is no longer valid to open the
 * directory.
 * 
 * \param  [in]                 OLD_DIRECTORY_NAME
 * \param  [in]                 NEW_DIRECTORY_NAME
 * \param  [out]                RETURN_CODE
 * \param  [in,out]             ERRNO
 * 
 * \retval NO_ERROR                     Successful completion
 * \retval INVALID_PARAM,  ENAMETOOLONG \p OLD_DIRECTORY_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p OLD_DIRECTORY_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p OLD_DIRECTORY_NAME is not a volume
 *                                      or directory
 * \retval INVALID_PARAM,  ENAMETOOLONG \p NEW_DIRECTORY_NAME or one of its
 *                                      components exceed maximum character
 *                                      length
 * \retval INVALID_PARAM,  EINVAL       \p NEW_DIRECTORY_NAME is syntactically
 *                                      invalid
 * \retval INVALID_PARAM,  ENOTDIR      A component of the path prefix of
 *                                      \p NEW_DIRECTORY_NAME is not a volume
 *                                      or directory
 * \retval INVALID_CONFIG, EACCES       The current partition does not have
 *                                      read-write access rights to the volume
 *                                      containing \p OLD_DIRECTORY_NAME
 * \retval NOT_AVAILABLE,  EBUSY        \p OLD_DIRECTORY_NAME has an operation
 *                                      in progress
 * \retval INVALID_PARAM,  EROFS        The storage device containing
 *                                      \p OLD_DIRECTORY_NAME is currently
 *                                      write protected
 * \retval INVALID_PARAM,  EINVAL       The volume for \p OLD_DIRECTORY_NAME is
 *                                      not identical to the volume for
 *                                      \p NEW_DIRECTORY_NAME
 * \retval INVALID_CONFIG, ENOSPC       Not able to rename the directory (not
 *                                      enough resources or max number of
 *                                      directories exceeded)
 * \retval INVALID_PARAM,  EPERM        \p OLD_DIRECTORY_NAME is an existing
 *                                      file
 * \retval INVALID_PARAM,  EISDIR       \p NEW_DIRECTORY_NAME is an existing
 *                                      file
 * \retval INVALID_PARAM,  ENOENT       \p OLD_DIRECTORY_NAME is not an
 *                                      existing directory
 * \retval INVALID_PARAM,  EEXIST       \p NEW_DIRECTORY_NAME is an existing
 *                                      directory and it is not empty
 * \retval NOT_AVAILABLE,  EBUSY        \p OLD_DIRECTORY_NAME is open for any
 *                                      access mode by the owning partition
 * \retval INVALID_PARAM,  EINVAL       \p OLD_DIRECTORY_NAME contains only the
 *                                      name of the volume
 * \retval INVALID_PARAM,  EINVAL       \p NEW_DIRECTORY_NAME is a subdirectory
 *                                      of \p OLD_DIRECTORY_NAME
 * \retval NOT_AVAILABLE,  EIO          The storage device containing
 *                                      \p OLD_DIRECTORY_NAME or
 *                                      \p NEW_DIRECTORY_NAME reports a failure
 * \retval INVALID_MODE,   EACCES       (Current process owns a mutex and
 *                                      operating mode is NORMAL) or the
 *                                      current process is the error handler
 * 
 * \note This service corresponds to the POSIX rename() function.
 */
extern void RENAME_DIRECTORY(
        FILE_NAME_TYPE OLD_DIRECTORY_NAME,
        FILE_NAME_TYPE NEW_DIRECTORY_NAME,
        RETURN_CODE_TYPE *RETURN_CODE,
        FILE_ERRNO_TYPE *ERRNO);

#endif

/** \} */