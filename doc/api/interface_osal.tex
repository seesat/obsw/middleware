%------------------------------------------------------------------------------
% interface_osal.tex - explanation of the osal api
%------------------------------------------------------------------------------
% author : Manuel Bentele
% date   : 10.06.2017
% project: Student research for the SeeSat project: Middleware 2016/2017
%------------------------------------------------------------------------------

\subsection{\texorpdfstring{\acrshort{osal}-\acrshort{api}}{OSAL-API}}
\label{subsec:osal_api}
Damit die Middleware die benötigte Kommunikation und Verwaltungslogik für die \glspl{app} implementieren kann, ist sie auf grundlegende Funktionalität aus dem \gls{osal} angewiesen. Hierfür wird hauptsächlich der Standard \gls{arinc}~653 als Hilfestellung zu Rate gezogen, um alle wichtigen Funktionalitäten der \gls{osal}"=\gls{api} zu berücksichtigen. Folgende Hauptfunktionalitätsgruppen müssen abgedeckt werden:
\begin{itemize}
    \item Partitionsverwaltung
    \item Prozessverwaltung
    \item Zeitverwaltung
    \item Speicherverwaltung (falls vom \gls{os} bei Partitioning verfügbar)
    \item Inter"= und Intrapartition Kommunikation
    \item Dateisystemverwaltung
    \item Hardwareverwaltung von Bus"= und anderen \gls{io} Geräten
    \item optionale Kryptologiefunktionalität
\end{itemize}

Diese Funktionalitätsgruppen sind in der \gls{osal}"=\gls{api} in mehreren Modulen strukturiert. \autoref{fig:osalmodulestructure} zeigt die schematische Grobstruktur der gesamten \gls{osal}"=\gls{api}.
Insgesamt gliedert sich die \gls{api} in vier große Modulgruppen, welche in der schematischen Darstellung als grau hinterlegte Spalten dargestellt werden. Der Begriff einer Modulgruppe wird in diesem Zusammenhang als eine Sammlung von ähnlichen Programmfunktionen aufgefasst.

\begin{figure}[hb]
    \centering
    \begin{tikzpicture}[
        box/.style={draw,rectangle,minimum width=\textwidth/7,
            text width=\textwidth/7,align=center,node distance=\ndist,
            minimum height=1cm},
        descbox/.style={draw=none,rounded corners=\ndist/2,fill=gray!40},
        bgbox/.style={rounded corners=\ndist,fill=gray!10},
        heading/.style={draw=none,fill=none},
        arinc/.style={fill=cyan!15},
        io/.style={fill=green!15},
        crypt/.style={fill=red!15},
        misc/.style={fill=orange!15},
        etc/.style={draw=none,fill=none},
        font=\scriptsize
    ]
    % define the node distance
    \pgfmathsetlengthmacro{\ndist}{2.5mm}
    % set aditional offset distance between the columns
    \pgfmathsetlengthmacro{\xoffset}{2.5mm}
    % create pgf layers
    \pgfdeclarelayer{backgroundtwo layer}
    \pgfdeclarelayer{backgroundone layer}
    \pgfsetlayers{backgroundtwo layer,backgroundone layer,main}
    % draw arinc653 required services group items
    \node[box,heading]                      (arincreqserv) {Required\\services};
    \node[box,arinc,below=of arincreqserv]  (arincpart)     {Partition 
        management};
    \node[box,arinc,below=of arincpart]     (arincprc)      {Process 
        management};
    \node[box,arinc,below=of arincprc]      (arinctime)     {Time management};
    \node[box,arinc,below=of arinctime]     (arincmem)      {Memory management};
    \node[box,arinc,below=of arincmem]      (arincintercom) {Interpartition 
        communication};
    \node[box,arinc,below=of arincintercom] (arincintracom) {Intrapartition 
        communication};
    \node[box,arinc,below=of arincintracom] (arinchm)       {Health monitoring};
    % draw arinc653 extended services group items
    \node[box,heading,right=of arincreqserv](arincextserv) {Extended\\services};
    \node[box,arinc,below=of arincextserv]  (arincfs)       {File system};
    \node[box,etc,below=of arincfs]         (arincetc)      {\vdots};
    % draw io group items
    \node[box,io,right=of arincextserv,xshift=\xoffset](iomq){Message queue};
    \node[box,io,below=of iomq]             (iospw)         {Spacewire};
    \node[box,io,below=of iospw]            (ioso)          {Socket};
    \node[box,etc,below=of ioso]            (ioetc)         {\vdots};
    % draw crypt group items
    \node[box,crypt,right=of iomq,xshift=\xoffset](cryptcrc)
        {\acrshort{crc}calculation};
    \node[box,crypt,below=of cryptcrc]      (crypthash)     {Hash functions};
    \node[box,crypt,below=of crypthash]     (cryptaes)      {\acrshort{aes} 
        encryption};
    \node[box,etc,below=of cryptaes]        (cryptetc)      {\vdots};
    % draw misc group items
    \node[box,misc,right=of cryptcrc,xshift=\xoffset](miscbyteorder){Byte order 
        (endianness)};
    \node[box,etc,below=of miscbyteorder]   (miscetc)       {\vdots};
    % draw heading items
    \node[box,heading,above=of
        $(arincreqserv.north)!0.5!(arincextserv.north)$,
        minimum height=0.75cm](arincheading){\textbf{\acrshort{arinc}~653}};
    \node[box,heading,above=of iomq,minimum height=0.75cm](ioheading)
        {\textbf{\acrshort{io}}};
    \node[box,heading,above=of cryptcrc,minimum height=0.75cm](cryptheading)
        {\textbf{Cryptology}};
    \node[box,heading,above=of miscbyteorder,minimum height=0.75cm](mischeading)
        {\textbf{\acrshort{misc}}};
    % draw the boxes of the modules
    \begin{pgfonlayer}{backgroundone layer}
    % box for arinc items
    \draw[descbox] ($(arinchm.south west)+(-\ndist/2,-\ndist/2)$) rectangle 
        ($(arincextserv.north east)+(\ndist/2,\ndist/2)$);
    % box for io items
    \draw[descbox] ($(iomq.north west)+(-\ndist/2,\ndist/2)$) rectangle 
        ($(ioetc.south east)+(\ndist/2,-\ndist/2)$);
    % box for crypt items
    \draw[descbox] ($(cryptcrc.north west)+(-\ndist/2,\ndist/2)$) rectangle 
        ($(cryptetc.south east)+(\ndist/2,-\ndist/2)$);
    % box for misc items
    \draw[descbox] ($(miscbyteorder.north west)+(-\ndist/2,\ndist/2)$) 
        rectangle ($(miscetc.south east)+(\ndist/2,-\ndist/2)$);
    \end{pgfonlayer}
    % draw the osal background
    \begin{pgfonlayer}{backgroundtwo layer}
    \draw[bgbox] ($(arinchm.south west)+(-\ndist,-\ndist)$) rectangle 
        ($(mischeading.north east)+(\ndist,\ndist/2)$);
    \end{pgfonlayer}
    \end{tikzpicture}
    \caption{Modulstruktur der \acrshort{osal}"=\acrshort{api}}
    \label{fig:osalmodulestructure}
\end{figure}

\subsubsection{\texorpdfstring{\acrshort{arinc}~653}{ARINC~653}}
Die Hauptgruppe \gls{arinc}~653 fasst in der \gls{osal}"=\gls{api} alle benötigten Funktionalitäten des \gls{arinc}~653 Standards zusammen. Nach diesem Standard ist die Aufteilung der Funktionalität in die \textit{Required services} und \textit{Extended services} vorgeschrieben. Zudem schreibt der Standard vor, dass alle \textit{Required services} implementiert werden müssen.
\par
Deshalb wurden alle spezifizierten \textit{Required services} gemäß der Spezifikation in den Entwurf der \gls{osal}"=Schicht miteinbezogen. Diese Services stellen den minimalen Funktionsumfang dar und müssen in jedem Fall, unabhängig von ihrer Verwendung, bereitgestellt werden.
Um die Komplexität der OSAL"=API nicht zu sehr zu erhöhen, wurde aus den \textit{Extended services} lediglich die wichtige Funktionalität der Dateisystemverwaltung übernommen. Detaillierte Informationen zu den einzelnen Funktionalitäten können dem \gls{arinc}~653 Standard aus Quelle \cite{cite:arinc653reqservices} und \cite{cite:arinc653extservices} entnommen werden.

\subsubsection{\texorpdfstring{\acrshort{io}}{IO}}
Da der \gls{arinc}~653 Standard keine vollständige und umfassende Funktionalität bezüglich der Verwaltung und Ansteuerung von \gls{io}"=Geräten bietet, wird diese Funktionalität vollständig durch das \gls{io}"=Modul der \gls{api} ersetzt. Dabei wird eine mögliche Redefinition von Funktionalitäten aus den Required services des \gls{arinc}~653 in Kauf genommen, um eine einheitliche und zentrale Schnittstelle für \gls{io}"=Geräte bieten zu können.
\par
Das \gls{io}"=Modul der \gls{osal}"=\gls{api} fasst die physikalischen Funktionalitäten von Bussystemen und anderen \gls{io}"=Geräten zusammen und bezeichnet diese Abstraktion als \textit{Device}. Ein \textit{Device} muss immer eine festgelegte Programmierschnittstelle implementieren. \autoref{fig:osaliointerface} zeigt die vollständige Schnittstellenspezifikation in Form eines Klassendiagramms. Das Diagramm weist zusätzlich Beispiel"=\textit{Devices} in Form von eingezeichneten Klassen auf, welche die Schnittstellen realisieren.

\begin{figure}[hb]
\centering
\begin{tikzpicture}[
        font=\scriptsize
    ]
    % define the IO interface
    \umlclass[type=interface,x=0,y=4]{Device}{}{
        + create(SETTING : SYSTEM\_ADDRESS\_TYPE, \\
            \hspace{7.5mm} RETURN\_CODE : RETURN\_CODE\_TYPE*) : void \\
        + open(RETURN\_CODE : RETURN\_CODE\_TYPE*) : void \\
        + close(RETURN\_CODE : RETURN\_CODE\_TYPE*) : void \\
        + read(FLAGS : FLAGS\_TYPE, \\
            \hspace{7.5mm} OUTPUT : APEX\_BYTE*, \\
            \hspace{7.5mm} LENGTH\_BUFFER : MESSAGE\_SIZE\_TYPE, \\
            \hspace{7.5mm} LENGTH\_RECV : MESSAGE\_SIZE\_TYPE*, \\
            \hspace{7.5mm} RETURN\_CODE : RETURN\_CODE\_TYPE*) : void \\
        + write(FLAGS : FLAGS\_TYPE, \\
            \hspace{7.5mm} INPUT : APEX\_BYTE*, \\
            \hspace{7.5mm} LENGTH : MESSAGE\_SIZE\_TYPE, \\
            \hspace{7.5mm} RETURN\_CODE : RETURN\_CODE\_TYPE*) : void \\
        + ioctl(PARAM : IOCTL\_PARAM\_TYPE, \\
            \hspace{7.5mm} VALUE : SYSTEM\_ADDRESS\_TYPE, \\
            \hspace{7.5mm} RETURN\_CODE : RETURN\_CODE\_TYPE*) : void
    }
    % define the sub classes that implements the interface
    \umlsimpleclass[x=-4.5,y=-2,width=4cm]{Message queue}
    \umlsimpleclass[x=0,y=-2,width=4cm]{Socket}
    \umlsimpleclass[x=4.5,y=-2,width=4cm]{SpaceWire}
    % build the inheritance tree
    \umlimpl{Message queue}{Device}
    \umlimpl{Socket}{Device}
    \umlimpl{SpaceWire}{Device}
\end{tikzpicture}
\caption{Klassendiagramm der \acrshort{osal} \acrshort{io} Schnittstelle}
\label{fig:osaliointerface}
\end{figure}

An den Beispielen wird erkenntlich, dass alle beliebigen \gls{io}"=Geräte oder Bussysteme über diese festgelegte Geräteschnittstelle verwaltet werden können.
Dies ist besonders bei diesem Projekt von großer Bedeutung, da die endgültige Hardwareauswahl zum Zeitpunkt der Projektdurchführung noch nicht vorlag.
\par
In der Implementation wird diese Schnittstelle aus dem Klassendiagramm in eine abstrakte Basisklasse mit Vererbungshierarchie umgewandelt. Dieses Modell kann anschließend einfach nach den Regeln aus Quelle \cite{cite:schreiner:ansicoop} umgesetzt werden, da die zu verwendende Programmiersprache~C keine direkte Unterstützung der objektorientierten Programmierung bereitstellt.
\par
Ein \textit{Device} muss dabei alle Funktionen der in \autoref{fig:osaliointerface} dargestellten Programmierschnittstelle implementieren. Die \textit{create} Funktion erzeugt und initialisiert ein neues \textit{Device} mit gerätespezifischen Einstellungen, wie etwa die lokale und entfernte Adresse eines Netzwerksockets bei einem Socket"=\textit{Device}.
Nach erfolgreichem Erstellen kann das \textit{Device} geöffnet (\textit{open} Funktion) und geschlossen (\textit{close} Funktion) werden. Besonders bei Bussystemen sind diese Funktionalitäten von großer Bedeutung, um eine Verbindung aufzubauen, bzw. zu schließen. Daten können von einem Gerät mittels der Funktionen \textit{read} und \textit{write} jeweils gelesen und geschrieben werden. Gerätespezifische Funktionalitäten können durch die Funktion \textit{ioctl} gesteuert werden. Bei einer Message Queue kann mit Hilfe dieser Funktion zum Beispiel die zu Beginn fix festgelegte Länge der Queue ausgelesen werden.

\subsubsection{Kryptologie}
Dieses \gls{api}"=Modul stellt Grundfunktionalitäten zur symmetrischen Verschlüsselung mit Hilfe des \gls{aes} bereit. Des Weiteren stellt das Modul  \gls{crc} Funktionalität und Hashverfahren bereit.
\par
Die Kryptologie"=Funktionalität ist in der \gls{osal}"=Schicht angesiedelt, um bei Gegebenheit die Hardwarebeschleunigung des Systems für Algorithmen nutzen zu können. Dies bietet besonders bei komplexen Operationen wie etwa dem Verschlüsseln von Daten mit Hilfe des \gls{aes} Algorithmus eine enorme Zeitersparnis bezogen auf die Laufzeit der Operation. Sind keine Möglichkeiten zur Nutzung von Hardwarebeschleunigungen vorhanden, muss der Algorithmus softwareseitig implementiert werden.

\subsubsection{\texorpdfstring{\acrshort{misc}}{MISC}}
In diesem Modul sind alle weiteren Funktionalitäten untergebracht, welche den zuvor genannten Modulgruppen nicht zugeordnet werden können.
\par
Wichtige Vertreter dieser Gruppe stellen Funktionen zur Änderung der \textit{Byte Order} dar. Da bei der Kommunikation der Middleware eine feste \textit{Byte Order} eingehalten werden muss und die \textit{Byte Order} des darunterliegenden \gls{obc} im Zeitraum der Projektdurchführung nicht bekannt war, muss diese Funktionalität ebenfalls in die \gls{osal}"=\gls{api} integriert werden.