%------------------------------------------------------------------------------
% reportdhbw.cls - provides a latex document class for DHBW reports
%------------------------------------------------------------------------------
% author: Hannes Bartle, Manuel Bentele
% date  : 11.05.2016
%------------------------------------------------------------------------------
% used packages: scrartcl, geometry
%------------------------------------------------------------------------------

% define the minimal latex version supported by this document class
\NeedsTeXFormat{LaTeX2e}

% declare new document class for DHBW reports
\ProvidesClass{reportdhbw}[11/05/2016 Version 0.1 DHBW report document class]

% use the scrartcl document class as base document class
\LoadClass[
	fontsize = 12pt,
	twoside = false,
	a4paper,
	parskip = half,
	listof = totoc,
	bibliography = totocnumbered
]{scrartcl}

% set the padding of the document content
\RequirePackage[
	left = 2.5cm,
	right = 2.5cm,
	top = 2.5cm,
	bottom = 2.5cm,
	includehead
]{geometry}

% set the distance between the geometry frame and the top of the header
\setlength{\topmargin}{0pt}
% set the separation between the header and the content
%\setlength{\headsep}{10pt}
% set the separation between the header and the content
\setlength{\footskip}{25pt}

% use a modern serif font
\RequirePackage{lmodern}

% package for the header and the footer
\RequirePackage[
	automark,
	headsepline
]{scrlayer-scrpage}

\clearscrheadfoot
\automark{section}

% normal page style
\newpairofpagestyles[scrheadings]{normal}{
	% print the top section and the page counter in the header
	\ihead{\headmark}
	\chead{}
	\ohead{\pagemark}
	\ifoot{}
	\cfoot{}
	\ofoot{}
}

% page style for lists and other things
\newpairofpagestyles[scrheadings]{list}{
	% print only the page counter in the header
	\ihead{}
	\chead{}
	\ohead{\pagemark}
	\ifoot{}
	\cfoot{}
	\ofoot{}
}

\pagestyle{normal}

\PassOptionsToPackage{hyphens}{url}
% load and set up the hyperref package
\RequirePackage[
	pdfdisplaydoctitle = true,
	pdfstartview = {Fit},
	bookmarks = true,
	bookmarksopen = true,
	colorlinks = true,
	allcolors = black
]{hyperref}
\AtBeginDocument{
	% set meta info for the pdf document
	\hypersetup{
		pdftitle = {\@title},
		pdfauthor = {\@author},
		breaklinks = true
	}
}

% set the baseline stretch to 1.5
\RequirePackage[
	onehalfspacing
]{setspace}
