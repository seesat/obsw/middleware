%------------------------------------------------------------------------------
% communication.tex - explanation of the middleware communication
%------------------------------------------------------------------------------
% author : Manuel Bentele
% date   : 10.06.2017
% project: Student research for the SeeSat project: Middleware 2016/2017
%------------------------------------------------------------------------------

\subsection{Kommunikation}
\label{subsec:middlewarecom}
Die Kommunikation stellt die wichtigste Hauptkomponente einer funktionsfähigen Middleware dar. Sie erlaubt den \glspl{app} untereinander Daten auszutauschen.
Die Kommunikation in der Middlewareschicht zwischen den \glspl{app} basiert auf Kommunikationsnachrichten.
\par
In den folgenden Abschnitten werden die verschiedenen Aspekte zum Entwurf und zur Spezifikation einer Kommunikation mit Nachrichtenprotokoll erläutert. Das Protokoll soll dabei einfach strukturiert sein und sich in Zukunft leicht erweitern lassen.

\subsubsection{Netztopologie}
Bevor ein Kommunikationsprotokoll entwickelt werden kann, muss die Netzstruktur der Kommunikationsteilnehmer bekannt sein. Für die \glspl{app} stehen hierfür mehrere Netztopologien zur Verfügung:
\begin{itemize}
    \item Ringtopologie
    \item Sterntopologie
    \item Bustopologie
    \item Baumtopologie
    \item Vermaschte Topologie
\end{itemize}

Bei der Wahl einer geeigneten Netztopologie zur Vernetzung der \glspl{app} spielt vor allem die Ausfallsicherheit des Netzes in Bezug auf ein optimales \gls{fdir} Konzept eine wichtige Rolle. Zudem sollen die Daten den entferntesten Kommunikationsteilnehmer über möglichst wenige Netzknoten erreichen.
\par
Die Ring"= und Baumtopologie können diese beiden Forderungen nicht erfüllen und sind für eine Netzstruktur nicht geeignet. Bei der Ringtopologie führt der Ausfall eines Netzknotens zum Ausfall des gesamten Netzes. Auch bei der Baumtopologie kann durch Ausfall eines Knotens ein Teilbaum der Topologie abgeschnitten werden.
\par
Die Ausfallsicherheit eines Netzes mit Bustopologie liegt allein an der Sicherheit des zentralen Busmediums. Da bei einem Totalausfall alle Kommunikationsteilnehmer abgeschottet sind, kann auch diese Netzstruktur nicht die Kriterien erfüllen.
\par
Die Sterntopologie besitzt den Vorteil, dass die Pfade einer Kommunikationsnachricht im Netzwerk immer genau gleich kurz sind und über den Sternknotenpunkt erfolgen. Ähnlich wie bei der Bustopologie bringt ein Ausfall dieses Knotens ebenso die gesamte Netzkommunikation zum Erliegen.
\par
Einzig und allein die vermaschte Topologie eines Netzes kann die gestellten Forderungen erfüllen (Vgl. \autoref{fig:vermaschtetopologien}). Fällt ein beliebiger Netzknoten aus, fällt nur ein kleiner Teil des Netzwerkes aus. Wird die Bedingung and die Topologie verschärft und eine vollvermaschte Topologie gefordert, entsteht eine ausfallsichere Peer-to-Peer Netzstruktur, wie in \autoref{fig:subfig:vollvermascht} dargestellt. Auch die Pfade einer Kommunikationsnachricht sind in einer vermaschten Topologie bezogen auf die Länge minimal, da der nächste Netzknoten ausgehend vom Sender in allen Fällen immer der Empfänger ist.
\par
 \begin{figure}[hb]
    \centering   
    \begin{subfigure}{0.35\linewidth}
    \centering
	\resizebox{\linewidth}{!}{  
    	\begin{tikzpicture} [
    		net node/.style = {circle, minimum width=1cm, inner sep=0pt, outer sep=0pt, ball color=blue!50!cyan},
    	  	net connect/.style = {line width=1pt, draw=blue!50!cyan!25!black},
     	 	net thick connect/.style = {net connect, line width=2.5pt},
    	]
     	 \foreach \i in {0,...,4}
     	   	\path (-90+\i*72:2) node (n\i) [net node] {};
     	 \foreach \i in {0,...,4}
       	 	\foreach \j in {0,...,4}
         		 \path [net connect]
           		 	(n\i) -- (n\j);;
    	\end{tikzpicture}
    }
    \subcaption{Vollvermaschte Topologie}
    \label{fig:subfig:vollvermascht}
    \end{subfigure}						% !!! NO NEWLINE HERE !!!
	\begin{subfigure}{0.35\linewidth}
	\centering
	\resizebox{\linewidth}{!}{    
    \begin{tikzpicture} [
    	net node/.style = {circle, minimum width=1cm, inner sep=0pt, outer sep=0pt, ball color=blue!50!cyan},
      	net connect/.style = {line width=1pt, draw=blue!50!cyan!25!black},
      	net thick connect/.style = {net connect, line width=2.5pt},
    ]
      \foreach \i in {0,...,4}
        \path (-90+\i*72:2) node (n\i) [net node] {};
      \foreach \i in {0,2,4}
        \foreach \j in {0,1,3}
          \path [net connect]
            (n\i) -- (n\j);;
    \end{tikzpicture}
    }
    \subcaption{Vermaschte Topologie}
    \label{fig:subfig:vermascht}
    \end{subfigure}       
    \caption{Vermaschte Topologien}
    \label{fig:vermaschtetopologien}
  \end{figure}

In der Middleware ist deshalb die vermaschte Topologie als Kommunikationsnetzstruktur zwischen den \glspl{app} vorgesehen. Alle weiteren Überlegungen basieren auf dieser Wahl der Netztopologie.

\subsubsection{Kommunikationsform}
Zu Beginn jeder Kommunikation stellt sich die Frage, wer mit wem Nachrichten austauschen kann. In der Middleware sind für diesen diesen Zweck verschiedene Kommunikationsformen vorgesehen.
\par
In der Middleware kommen \textit{Unicast} und \textit{Broadcast} zum Einsatz. Beide Kommunikationsformen stellen die Möglichkeit bereit die Daten einer Kommunikation in Form von Nachrichten an verschieden viele Netzelemente einer Struktur zu adressieren.
\par
Ein \textit{Unicast} erlaubt die Übermittlung von Nachrichten von einem Sender zu genau einem Empfänger. \newline
Ein \textit{Broadcast} erlaubt dem Sender einer Nachricht, dass alle Empfänger aus der Gruppe der Empfänger bei einer Nachrichtenübertragung adressiert werden.
\par
\textit{Broadcast}"=Nachrichten können durch \textit{Unicast}"=Nachrichten abgebildet werden. Jedoch erhöht sich bei dieser Methode die Auslastung des genutzten Übertragungsmediums sehr stark. Deshalb werden von der Middleware beide Kommunikationsformen unterstützt. Zudem können durch diese Differenzierung der Kommunikationsformen spezielle technische Eigenschaften des jeweiligen Übertragungsmediums besser ausgenutzt werden.
Beispielsweise kann ein \textit{Broadcast} bei speziellen \glspl{rtos} Implementierungen von Message Queues ohne Nachbildung durch \textit{Unicasts} umgesetzt werden \cite[109\psq]{cite:li:realtimeconcepts}.

\subsubsection{Kommunikationsprotokoll}
Das Kommunikationsprotokoll für die Nachrichten besteht aus einer einzigen Nachrichtenstruktur, welche sehr einfach und flexibel nutzbar ist.
\autoref{fig:structuremessage} veranschaulicht die Struktur einer Nachricht.
Die \textit{Byte Order} einer Nachricht ist in diesem Projekt auf die Netzwerk Byte Order Big"=Endian festgelegt. Die zu übermittelnden Daten einer Nachricht werden über die \textit{Type"=Length"=Value} Kodierung kodiert. Nach Sharp können dabei Daten mit dynamischer Länge durch eine Längeninformation (siehe \textit{Length} Feld) und einem Typ (siehe \textit{Service type} Feld) kodiert werden \cite[244]{cite:sharp:protocoldesign}.

\begin{figure}[hb]
\centering
\definecolor{lightgray}{gray}{0.8}
\newcommand{\colorbitbox}[3]{%
    \rlap{\bitbox{#2}{\color{#1}\rule{\width}{\height}}}%
    \bitbox{#2}{#3}%
}
\begin{bytefield}{32}
\bitheader{0,7,8,15,16,23,24,31} \\
    \begin{rightwordgroup}{Message header}
        \bitbox{8}{Version} & \bitbox{8}{Type} & \bitbox{8}{Flags} &
            \colorbitbox{lightgray}{8}{Reserved} \\
        \bitbox{32}{Source \acrshort{app} address (SRC)} \\
        \bitbox{32}{Destination \acrshort{app} address (DST)} \\
        \bitbox{16}{Service type} & \bitbox{16}{Length}
    \end{rightwordgroup} \\
    \begin{rightwordgroup}{Message payload}
        \wordbox[lrt]{1}{Data (byte aligned)} \\
        \skippedwords \\
        \wordbox[lrb]{1}{} \\
        \bitbox{16}{Checksum} & \bitbox{16}{\dots \emph{optional} padding}
    \end{rightwordgroup}
\end{bytefield}
\caption{Struktur einer Kommunikationsnachricht der Middleware}
\label{fig:structuremessage}
\end{figure}

Eine Nachricht besteht aus zwei Bereichen. Ein Nachrichten"=Header fasst alle wichtigen Informationen zur Nachricht zusammen. Der Header weist eine fest Länge auf, um das Parsen der Informationen in Softwareimplementierungen zu erleichtern.
\par
Das \textit{Version} Feld einer Nachricht gibt Auskunft über die vorliegende Version des verwendeten Protokolls. Dieses Feld ist vorgesehen, um zukünftig bei Bedarf angepasste Protokollversionen der Nachrichtenstruktur einführen zu können.
\par
Das \textit{Type} Feld einer Nachricht gibt die Kommunikationsform einer Nachricht an. Hierbei kann es sich um einen \textit{Unicast} oder \textit{Broadcast} handeln. Beim Typ \textit{Broadcast} müssen zudem alle Bits des \textit{Destination \acrshort{app} address} Feld der Nachricht gesetzt sein. Diese Adresse ist systemweit als Broadcastadresse reserviert und darf nur für diesen Zweck verwendet werden.
Weitere Kommunikationsformen können bei Bedarf hinzugefügt werden.
\par
Das \textit{Flags} Feld einer Nachricht definiert zusätzlich Flags, welche eine Nachricht genauer charakterisieren könne. Beispielsweise kann ein fest definiertes Flag angeben, ob der Datenbereich einer Nachricht verschlüsselt ist.
\par
Das \textit{Reserved} Feld einer Nachricht ist für zukünftige Erweiterungen reserviert und sollte nicht ohne triftigen Grund benutzt werden.
\par
Das \textit{Source \acrshort{app} address} Feld einer Nachricht enthält die eindeutige Adresse der Sender"=\gls{app}.
\par
Das \textit{Destination \acrshort{app} address} Feld einer Nachricht enthält die eindeutige Adresse der Empfänger"=\gls{app}.
\par
Das \textit{Service type} Feld einer Nachricht charakterisiert den Typ der übermittelnden Daten im Payload"=Bereich einer Nachricht.
\par
Das \textit{Length} Feld einer Nachricht gibt die Länge in Byte der zu übermittelnden Daten im Payload"=Bereich einer Nachricht an.
\par
Das \textit{Data} Feld einer Nachricht enthält die zu übermittelnden Daten einer Nachricht. Die kleinste zu übermittelnde Dateneinheit beträgt dabei ein Byte. Die Länge in Byte einer Nachricht muss deshalb einem Vielfachen von einem Byte entsprechen (byte aligned).
\par
Das \textit{Checksum} Feld einer Nachricht enthält die generierte Prüfsumme, welche über die gesamte Nachricht (Header"= und Payload"=Bereich) berechnet wurde.
Als Verfahren zur Prüfsummenberechnung kommt \gls{crc} zum Einsatz. \gls{crc} ist ein Hashverfahren, welches die Polynomdivision für die Berechnung der Prüfsumme verwendet. Ein Hashverfahren bildet eine Nachricht mit beliebiger Länge auf eine Prüfsumme mit fest definierter Länge ab. Bei der \gls{crc}"=Berechnung hängt die Länge der Prüfsumme von dem verwendeten Generatorpolynom ab. Für die Prüfsumme einer Nachricht wird das \gls{crc}"=CCITT Polynom $x^{16} + x^{12} + x^{5} + 1$ verwendet \cite[74\psq]{cite:hufschmid:infocom}.
Dieses Polynom weist eine minimale Hamming"=Distanz von $d_{min} = 4$ auf. Die minimale Hamming"=Distanz gibt dabei an, in wie vielen Bits sich zwei verschiedene Nachrichten mindestens unterscheiden. Bei einer minimalen Hamming"=Distanz von $d_{min}$ können $d_{min} - 1$ Bitfehler einer Nachricht sicher erkannt werden. Zudem ist bei geradem $d_{min}$ eine Fehlerkorrektur möglich, bei welcher weniger oder gleich $\frac{d_{min} - 2}{2}$ Bitfehler aufgetreten sind \cite[48\psqq]{cite:hufschmid:infocom}. Neben den ein und zwei Bitfehlern können \gls{crc}"=Prüfsummen auch \glqq Büschelfehler mit einer Länge kleiner gleich dem Grad des Polynoms erkennen\grqq\ \cite[75]{cite:hufschmid:infocom}.
Aus diesem Grund wird hierbei ein Generatorpolynom mit dem Grad $16$ benutzt, um größere Büschelfehler im Gegensatz zu einem Polynom mit Grad $8$ zu erkennen.
\par
Das \textit{Padding} Feld einer Nachricht enthält optional aufgefüllte Nullbyte zum Schluss einer Nachricht. Diese können je nach Funktion des Übertragungsmediums nötig sein, um eine Übertragung zu ermöglichen.