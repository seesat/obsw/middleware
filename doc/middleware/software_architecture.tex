%------------------------------------------------------------------------------
% software_architecture.tex - explains the software architecture
%------------------------------------------------------------------------------
% author : Robin Windey
% date   : 01.05.2017
% project: Student research for the SeeSat project: Middleware 2016/2017
%------------------------------------------------------------------------------

\subsection{Softwarearchitektur}

\subsubsection{Kontextsicht}
\label{subsubsec:software_kontextview}
Die Kontextsicht betrachtet die Middleware als Blackbox innerhalb des Gesamtsystems, um so die Schnittstellen zwischen Middleware 
und außenliegendem System abstrakt definieren zu können. \autoref{fig:dargento:interfacediagram} zeigt ein logisches 
Schnittstellendiagramm des SeeSat Projekts. In grün dargestellt sieht man den \gls{obc}, auf welchem die \glspl{app}
später einmal ausgeführt werden. Innerhalb des lila eingefärbten Blocks sind verschiedene Sensoren, wie z.B.
ein \acrshort{gps}"=Sensor, ein Magnetometer oder ein Sonnensensor, dargestellt. Um den Anwendungen des Systems Zugriff
auf die Sensordaten gewähren zu können sollte die Middleware folglich eine einheitliche Schnittstelle anbieten, um diese
auslesen zu können. Im Falle des Einsatzes von Aktoren sollten die \glspl{app} außerdem noch die Möglichkeit haben,
diese entsprechend steuern zu können. \\
Der dunkelgelbe Block zeigt die \gls{tmtc} Einheit. Diese stellt über einen entsprechenden Datenlink die Verbindung
zur Bodenstation her und ist somit in der Lage sowohl Daten an diese zu liefern, als auch Befehle von ihr entgegenzunehmen
und mit Hilfe des \acrshort{obc}'s an den korrekten Empfänger weiterzuleiten. 
\par
Da zum gegenwärtigen Zeitpunkt noch keine explizite Festlegung der Hardware stattfand kann auf die konkrete Umsetzung der
Kommunikation zwischen den Sensoren, bzw. Aktoren des Systems, dem \gls{tmtc}"=Block und der Middleware an dieser Stelle nicht genauer eingegangen
werden. Es bleibt lediglich festzuhalten, dass die Middleware eine möglichst generische Zugriffsstruktur auf den Sensor"= bzw. 
Aktor"=Block für die einzelnen \glspl{app} bereitstellen sollte. Ebenso sollte eine Funktionalität zur Kommunikation mit der
Bodenstation zumindest für ausgewählte \glspl{app} verfügbar sein.
%Was befindet sich außerhalb unserer Middleware? \newline
%Was spielt sich außerhalb vom OBC ab?
%Einbezug von Dennis Systemarchitekturdiagramms:
\newcommand{\measurepage}{\dimexpr\pagegoal-\pagetotal-\baselineskip\relax}
\begin{landscape}
\begin{figure}[ht]
    \centering
    \includegraphics[height=\measurepage,width=\textwidth,keepaspectratio]
        {img/middleware/seesat_interface_diagram.pdf}
    \caption[SeeSat Schnittstellendiagramm]{SeeSat Schnittstellendiagramm 
    \cite{cite:dargento:interfacediagram}}
    \label{fig:dargento:interfacediagram}
\end{figure}
\end{landscape}

\subsubsection{Bausteinsicht}
Die Bausteinsicht soll kären, welche Architekturbausteine das System der Middleware 
beinhaltet. Sie gliedert sich allgemein in Subsysteme, Komponenten und Schnittstellen
und soll einerseits als Referenz für die Software"=Entwickler dienen, andererseits aber
auch einen groben Gesamtüberblick über die einzelnen Komponenten der Software
geben um beispielsweise Projektleitern oder Auftraggebern eine bessere
Projektüberwachung zu ermöglichen.
\cite[86]{cite:gernot:effektivesoftware}
\par
Wie in \autoref{fig:mw_components_diagram} dargestellt, unterteilt sich die Middleware 
in die drei Hauptkomponenten \textit{Communication}, \textit{Init} und \textit{\acrshort{io}}, wobei
letztere als optional zu verstehen ist (hierauf wird in \autoref{sec:middlewareapi} noch
genauer eingegangen). Die Kommunikation (Vgl. \autoref{subsec:middlewarecom}) unterteilt sich des weiteren in die
\textit{Nachrichtenverarbeitung} und die \textit{Nachrichtenübermittlung}, bzw.
den Transfer von Daten. Innerhalb der Initialisierungskomponente wird die Middleware
beim Start der jeweiligen \gls{app} initialisiert.
\par
Jede \gls{app} besitzt außerdem eine individuelle Konfiguration, welche im Wesentlichen
aus einer eindeutigen \gls{id}, sowie einer Adresse besteht, unter der die \gls{app} 
erreichbar ist. Da die Konfiguration \gls{app}"=spezifisch ist liegt diese
außerhalb der Middleware und kann somit individuell festgelegt werden. Die Schnittstelle
\textit{conf} in \autoref{fig:mw_components_diagram} zeigt den Zugriff der
Middleware auf die Konfigurations"=Komponente. Primär macht die \textit{Init}"=Subkomponente
der Middleware von dieser Schnittstelle gebrauch um z.B. eine neu hinzugekommene
\gls{app} und deren Adresse global propagieren zu können. Aber auch andere Komponenten, wie
beispielsweise die \textit{Communication}"=Subkomponente verwenden die Daten der Konfigurationen 
im laufenden Betrieb, weshalb die \textit{conf}"=Schnittstelle als generelle Schnittstelle
der Middleware hin zur Konfiguration dargestellt ist.

\begin{figure}[ht]
	\centering
	\resizebox{\linewidth}{!}{ 
	\begin{tikzpicture}
		% Components
		\umlbasiccomponent{Configuration}
		\begin{umlcomponent}[y=-6, x=-3]{Middleware}
			\begin{umlcomponent}{Communication}
				\umlbasiccomponent{Message Processing}
				\umlbasiccomponent[x=5]{Message Transfer}
			\end{umlcomponent}
			\umlbasiccomponent[x=10]{IO - optional}
			\umlbasiccomponent [x=14] {Init}  
		\end{umlcomponent}
		
		% Connectors
		\umlVHVassemblyconnector[with port, interface=conf]{Middleware}{Configuration} 
	\end{tikzpicture}
	}
	\caption{Komponentendiagramm der Middleware mit außenliegender Konfiguration der App}
	\label{fig:mw_components_diagram}
\end{figure}

\subsubsection{Abbildungssicht}
\label{subsubsec:abbsicht}
Die Abbildungssicht soll klären, wie die Middleware auf die Hardware verteilt wird
und in welchen Bausteinen Middleware und \acrshort{osal} in die \glspl{app} eingepflegt werden. 
\citeparagraph{6.4.4}{cite:posch:softwarearchitektur}.
\par
Da die Middleware im SeeSat Projekt zum gegenwärtigen Zeitpunkt lediglich auf \textit{einem} 
Rechner ausgeführt wird (Vgl. \autoref{fig:dargento:interfacediagram}, \acrshort{obc}) beschränkt sich
die Frage nach der Ausrollung der Binaries auf dieses eine System.
\autoref{fig:middlewareAssemblies} zeigt eine mögliche Ansicht wie Middleware und \acrshort{osal}
in jeweils eigenen Assemblies in jede \gls{app} einkompiliert werden. Bei dieser Herangehensweise
ist sicherzustellen, dass alle \glspl{app} stets den selben Softwarestand von Middleware und \acrshort{osal}
verwenden, da es ansonsten zu unvorhersehbaren Fehlern innerhalb des Systems kommen kann. 
Das statische Einkompilieren der Komponenten birgt außerdem den Nachteil, dass bei etwaigen Änderungen
innerhalb der Middleware"=Komponente der komplette Softwarestand mit allen
zu diesem Zeitpunkt verwendeten \glspl{app} neu kompiliert werden muss. Vorteil bei diesem Ansatz ist,
dass eine \gls{app} nicht kompilierbar ist, sobald sie ein strukturell verändertes Binary verwendet.
Würde man beispielsweise eine Funktionssignatur innerhalb der Middleware ändern würde dies sofort auffallen,
da eine auf diese Änderung nicht angepasste \acrshort{app} nicht mehr kompiliert werden könnte.
\par
Eine weitere Möglichkeit wäre die Binaries als dynamische Systembibliothek innerhalb des \acrshort{obc}'s
zur Verfügung zu stellen. Dies würde die genannten Nachteile von Versionsunterschieden und stetigem
Neukompilieren kompensieren. Allerdings ist nicht sichergestellt, dass jede \acrshort{app} 
auf strukturelle Veränderungen innerhalb der Middleware angepasst wurde, was wiederum zu
Laufzeitfehler führen kann. Ebenfalls muss sichergestellt werden, dass die Library auf dem
Zielsystem überhaupt verfügbar ist. Wäre dies nicht der Fall würde das den Absturz der Software"=
Komponente bedeuten, was in einem \gls{rtos} nicht passieren darf.
\par
Da zum gegenwärtigen Zeitpunkt noch kein Betriebssystem für
das Projekt definiert wurde ist außerdem zu klären, inwiefern System"=Libraries unterstützt werden
und was dafür getan werden muss, um die Middleware als eine solche zur Verfügung zu stellen.  \\
Generell wird für die weitere Vorgehensweise an dieser Stelle jedoch der in \autoref{fig:middlewareAssemblies}
dargestellte Ansatz des \textit{statischen Einkompilierens} der Middleware in die einzelnen
\glspl{app} vorgeschlagen, da dieses Konzept im Allgemeinen eine höhere Integrität und Sicherheit
des Systems gewährleistet, sofern man den angesprochenen Nachteilen adäquat entgegentreten kann.

	\begin{figure}[ht]
    \centering
    \begin{tikzpicture}[
    	minheight/.style={minimum height = 0.75cm},
   		bgbox/.style={minheight,rounded corners=2.5mm,fill=gray!20, rectangle, node distance = 1mm},
   		inappbox/.style={minheight,minimum width = (\textwidth / 3.5) - 2 * 2.5mm, draw = black, rectangle, align = center, node distance = 2.5mm and 5mm},
		oshardware/.style={minheight,minimum width = (\textwidth / 3.5) * 3 - 2 * 2.5mm, draw = black, rectangle, align=center, node distance = 2.5mm},        
        font = \footnotesize
    ]

    % inner MW / OSAL nodes
	\node[inappbox,fill=orange!20] (mw1) {\texorpdfstring{\acrshort{mw}}{MW}};
	\node[inappbox,fill=green!20, below = of mw1] (osal1) {\texorpdfstring{\acrshort{osal}}{OSAL}};
	
	\node[inappbox,fill=orange!20, right = of mw1] (mw2) {\texorpdfstring{\acrshort{mw}}{MW}};
	\node[inappbox,fill=green!20, below = of mw2, right = of osal1] (osal2) {\texorpdfstring{\acrshort{osal}}{OSAL}};
	
	\node[inappbox,fill=orange!20, right = of mw2] (mwn) {\texorpdfstring{\acrshort{mw}}{MW}};
	\node[inappbox,fill=green!20, below = of mwn, right = of osal2] (osaln) {\texorpdfstring{\acrshort{osal}}{OSAL}};
	
	% hardware and OS nodes (for all apps)
	\node[oshardware, fill=cyan!20] (os) at ($(osal2.south) - (0,7mm)$) {\texorpdfstring{\acrshort{os}}{OS}};
	\node[oshardware, fill = gray!20, below = of os] (hw) {Hardware};
	
	% headers (Apps)
	\node[] (app1heading) at ($(mw1.north) + (0,15mm)$) {\texorpdfstring{\acrshort{app}}{App}~1};	
	\node[] (app2heading) at ($(mw2.north) + (0,15mm)$) {\texorpdfstring{\acrshort{app}}{App}~2};
	\node[] (appnheading) at ($(mwn.north) + (0,15mm)$) {\texorpdfstring{\acrshort{app}}{App}~$n$};

	% app boxes
	\begin{pgfonlayer}{background}
  		\node[bgbox] [fit = (app1heading) (osal1)] {};
	\end{pgfonlayer}	
	\begin{pgfonlayer}{background}
  		\node[bgbox] [fit = (app2heading) (osal2)] {};
	\end{pgfonlayer}
	\begin{pgfonlayer}{background}
  		\node[bgbox] [fit = (appnheading) (osaln)] {};
	\end{pgfonlayer}

    \end{tikzpicture}
    \caption{Assembly-Ansicht von Middleware und OSAL bei statischem Einkompilieren in jede \acrshort{app}}
    \label{fig:middlewareAssemblies}
\end{figure}

\subsubsection{Laufzeitsicht}
Die Laufzeitsicht beschreibt im Allgemeinen welche Komponenten des Systems zur Laufzeit 
existieren und wie diese miteinander interagieren. Für Echtzeitsysteme klärt sie
außerdem die Abbildung der Bestandteile des Systems auf Prozesse, Tasks oder Threads 
\cite[92]{cite:gernot:effektivesoftware}. Der folgende Abschnitt soll für eine
konkrete Umsetzung der Middleware richtungsweisende Überlegungen für Entwurfsentscheidungen 
bezüglich der Implementierung von Betriebsabläufen liefern.
\par
Für die Middleware stellt sich zuerst generell die Frage, wie die einzelnen 
\glspl{app} ausgeführt werden. Jede \gls{app} besitzt zunächst einen eigenen
Prozess im Betriebssystem, welcher optimalerweise durch das in \autoref{subsec:timeandspace} besprochene
\textit{Time and Space Partitioning} von Prozessen anderer \glspl{app} 
abgeschottet ist. Wählt man den in \ref{subsubsec:abbsicht} erläuterten
Ansatz des \textit{statischen Einkompilierens} der Middleware in jede \gls{app},
entspricht die Anzahl der Instanzen der Middleware zur Laufzeit der Anzahl der \glspl{app},
welche zu diesem Zeitpunkt ausgeführt werden. \newline
Pro \gls{app} lassen sich außerdem zwei grundsätzliche Herangehensweisen unterscheiden,
wie die Middleware ihr Aufgaben jeweils abarbeitet. Zum einen kann man einen \textbf{streng sequenziellen}
Ansatz verfolgen, welcher pro \gls{app} nur einen Hauptprozess ohne Subprozesse oder
Threads vorsieht. Hierbei werden Funktionen nacheinander abgearbeitet, was bedeutet,
dass z.B. auf blockierende Funktionen gewartet werden muss, bis diese abgeschlossen sind,
bevor eine neue Aufgabe begonnen werden kann. Betrachtet man als Beispiel das Auslesen und
Interpretieren einer Lageregelung des Sattelits wird schnell klar, dass die Ausführungszeit
des Systems hierbei unter anderem stark von externen Komponenten beeinflusst wird (Vgl. auch \autoref{subsec:types_mw}).
Dies kann unter Umständen zum etwaigen Stillstand des Systems führen. \newline
Da nur ein Prozess besteht entfallen Synchronisationsmechanismen, was ein Vorteil
dieses Ansatzes ist. Außerdem ist ein prozessinternes Scheduling nicht notwendig,
da keine Threads oder dergleichen bestehen, deren Ausführungsplan verwaltet werden müsste.
\par
Alternativ lässt sich ein Ansatz durch mehrere \textbf{Threads} innerhalb des
jeweiligen \gls{app}"=Prozesses umsetzen. Hierbei könnte die Abarbeitung von blockierenden
Funktionen innerhalb separater Threads durchgeführt werden und würde somit nicht die
Gesamtperformance der Software beeinflussen. Durch den Einsatz von Threads muss
allerdings die Synchronisation an gewissen Programmstellen vorgenommen werden.
Auch konkurrierende Funktionen innerhalb verschiedener Threads müssen berücksichtigt,
und beispielsweise durch entsprechende Sperrmechanismen abgesichert werden. \newline
Der parallele Ansatz sollte nicht mit einem \textit{event"=basierten System} verwechselt werden.
Betrachtet man wieder das Beispiel des Auslesens einer Lageregelung, so könnte man
umgekehrt auch von dieser fordern, dass sie sich bei der Middleware meldet,
sobald sich deren Daten geändert haben. Da dieses \textit{Event} jedoch zu einem
willkürlichen Zeitpunkt eintreten kann ist ein Determinismus nicht mehr gegeben
und dieser Ansatz somit innerhalb eines Echtzeit"=Systems schwer umsetzbar.