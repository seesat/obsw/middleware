\documentclass[12pt,a4paper]{reportdhbw}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}

%------------------------------------------------------------------------------
% variables
%------------------------------------------------------------------------------
\newcommand{\projecttitle}{Entwurf und Konzeption einer Middleware für
                           Raumfahrtsysteme}
\newcommand{\projectauthorone}{Manuel Bentele}
\newcommand{\projectauthoronematnumber}{6895818}
\newcommand{\projectauthortwo}{Dominik Schwarzbauer}
\newcommand{\projectauthortwomatnumber}{9865741}
\newcommand{\projectauthorthree}{Robin Windey}
\newcommand{\projectauthorthreematnumber}{7301319}
\newcommand{\projectcompany}{Konzept Informationssysteme GmbH}
\newcommand{\projectlocation}{Friedrichshafen}
\newcommand{\projecttype}{Studienarbeit SeeSat}
\newcommand{\projectperioddetail}{09.11.2016 - 12.07.2017}
\newcommand{\projecthandoverdate}{12.07.2017}
\newcommand{\projectadvisor}{M.\,Sc. Julian Bayer}

% Cite for paragraphs
\newcommand{\citeparagraph}[2]{\cite[Abschnitt #1]{#2}}

\title{\projecttitle}
\author{\projectauthorone, \projectauthortwo, \projectauthorthree}

\newcommand{\softwaredocfile}{./../bin/api/latex/refman.pdf}
\newcommand{\softwaredocappendixname}{osal-mw-api-doc.pdf}

%------------------------------------------------------------------------------
% packages
%------------------------------------------------------------------------------
\usepackage{csquotes}
\usepackage[backend=biber,bibencoding=utf8,natbib=true,defernumbers=true,
    style=numeric,firstinits=true,sorting=nty]{biblatex}
\addbibresource{literature.bib}
\addbibresource{pictures.bib}

\usepackage{bookmark}
\usepackage[toc,acronym,nomain,nonumberlist,nopostdot]{glossaries}
\makeglossaries

% package for drawing pictures
\usepackage{tikz}
\usetikzlibrary{positioning,calc}
\usepackage{tikz-uml}

% package for subfigures
\usepackage{subcaption}

% color packages
\usepackage{color}
\usepackage{xcolor}

% package to draw message structures from communication protocols
\usepackage{bytefield}

% package for PDF landscape mode
\usepackage{pdflscape}

% package for printing directory tree
\usepackage{dirtree}

% package for display code fragments
\usepackage{listings}
% setup the code higlighting
\lstset{
    frame=b,
    breaklines=true,
    language=C,
    captionpos=t,
    rulecolor=\color{gray},
    %aboveskip=12pt,
    belowskip=0pt,
    xleftmargin=5mm,
    linewidth=\linewidth
}

% caption package to modify the caption look
\usepackage{caption}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{
                   \parbox{\dimexpr\textwidth-4\fboxsep\relax}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}

% counter for the roman page numbering
\newcounter{romanpagecounter}

% define new translations for the dictionary
\addto{\extrasngerman}
{%
    \renewcommand*{\figureautorefname}{Abbildung}%
    \renewcommand*{\tableautorefname}{Tabelle}%
    \renewcommand*{\partautorefname}{Abschnitt}%
    \renewcommand*{\appendixautorefname}{Anhang}%
    \renewcommand*{\equationautorefname}{Gleichung}%
    \renewcommand*{\Itemautorefname}{Element}%
    \renewcommand*{\chapterautorefname}{Kapitel}%
    \renewcommand*{\sectionautorefname}{Abschnitt}%
    \renewcommand*{\subsectionautorefname}{Abschnitt}%
    \renewcommand*{\subsubsectionautorefname}{Abschnitt}%
    \renewcommand*{\paragraphautorefname}{Absatz}%
    \renewcommand*{\Hfootnoteautorefname}{Fußnote}%
    \renewcommand*{\AMSautorefname}{Gleichung}%
    \renewcommand*{\theoremautorefname}{Theorem}%
    \renewcommand*{\pageautorefname}{Seite}%
    %\renewcommand*{\listingautorefname}{Listing}%
}

% package to convert pictures from the eps to the pdf format
\usepackage{epstopdf}
\epstopdfsetup{outdir=./../bin/doc/}

\usepackage{array}

% package to attach external files into the pdf
\usepackage{embedfile}
\usepackage{hypgotoe}

\begin{document}

    % change the page numbering style to little roman
    \pagenumbering{roman}

    % include the titlepage
    \thispdfpagelabel{Titelseite}
    \include{titlepage}
    \clearpage

    % change the page numbering style to big roman
    \pagenumbering{Roman}
    \pagestyle{list}

    % include the assertion
    \include{assertion}
    \clearpage

    % include the abstract
    \include{abstract}
    \clearpage

    % include the table of contents
    \pdfbookmark[section]{\contentsname}{toc}
    \tableofcontents
    \clearpage

    % include the abbreviations
    \include{abbreviations}
    \clearpage

    % store the current page number of the roman style
    \setcounter{romanpagecounter}{\value{page}}
    % change the page numbering to the arabic style and begin from 1
    \pagenumbering{arabic}
    \pagestyle{normal}

    % include the introduction
    \include{introduction}
    \clearpage

    % include information about the environment of this project
    \include{project_environment}
    \clearpage

    % include details about this project
    \include{project_work}
    \clearpage

    % include the main part of the project
    \include{middleware}
    \clearpage
    
    % include the description of the osal and mw interface
    \include{api}
    \clearpage

    % include overview of required apps for a general mission
    \include{apps}
    \clearpage
    
    % include short description of the development environment
    \include{development_environment}
    \clearpage

    % include the conclusion
    \include{conclusion}
    \clearpage

    % change the page numbering to the roman style
    \pagenumbering{Roman}
    \pagestyle{list}
    % restore the old roman page number counter
    \setcounter{page}{\value{romanpagecounter}}

    % bibliography list
    \section*{Bibliografie}
    \addcontentsline{toc}{section}{Bibliografie}

    \defbibheading{lit}{\subsection*{Literaturquellen}}
    %\addcontentsline{toc}{subsection}{Literaturquellen}
    \printbibliography[notkeyword=picture,heading=lit]

    \defbibheading{pic}{\subsection*{Bildquellen}}
    %\addcontentsline{toc}{subsection}{Bildquellen}
    \printbibliography[keyword=picture,heading=pic]
    \clearpage

    % figure list
    \listoffigures
    \clearpage
    
    % appendix of the document if the software documentation exists
    \IfFileExists{\softwaredocfile}{
        \include{appendix}
    }{
        \typeout{The software documentation was not built and can't attached to
                 the pdf.}
    }

\end{document}
