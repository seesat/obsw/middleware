#------------------------------------------------------------------------------
# makefile - main makefile to build the software and the latex documentation
#------------------------------------------------------------------------------
# author : Manuel Bentele
# date   : 11.12.2016
# project: Student research for the SeeSat project: Middleware 2016/2017
#------------------------------------------------------------------------------

# define the build tools
TEX     = pdflatex
TFLAGS  = -interaction=nonstopmode -shell-escape
INDEX   = makeindex
GLOSSAR = makeglossaries
BIB     = biber

DOCGEN  = doxygen

CPPCHCK = cppcheck
CPFLAGS = 
SPLINT  = splint
SPFLAGS = -fixedformalarray

# define the directories
DIR_BIN = bin
DIR_DOC = doc
DIR_SRC = src
DIR_INC = $(DIR_SRC)/include

# function to find files recursively
# $1: directory name
# $2: pattern match
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter \
              $(subst *,%,$2),$d))

# path list of all header files
HEADERS=$(call rwildcard, $(DIR_INC), *.h)

# target to build the entire project
.PHONY: all
all: softwaredoc documentation

# target to build the latex documentation
.PHONY: documentation
documentation:
	@echo "Build the latex documentation."
	@mkdir -p $(DIR_BIN)/doc
	@cd $(DIR_DOC) && \
	$(TEX) $(TFLAGS) -draftmode -output-directory ../$(DIR_BIN)/doc \
		main > /dev/null && \
	echo "Build the bibliography." && \
	$(BIB) --input-directory ../$(DIR_BIN)/doc \
		--output-directory ../$(DIR_BIN)/doc main > /dev/null && \
	echo "Build the glossary." && \
	$(GLOSSAR) -d ../$(DIR_BIN)/doc main > /dev/null && \
	$(TEX) $(TFLAGS) -draftmode -output-directory ../$(DIR_BIN)/doc \
		main > /dev/null && \
	echo "Build the final PDF document." && \
	$(TEX) $(TFLAGS) -output-directory ../$(DIR_BIN)/doc \
		main

# target to build the software documentation
.PHONY: softwaredoc
softwaredoc:
	@echo "Build the software documentation."
	@mkdir -p $(DIR_BIN)/api
	@echo "Generate documentation."
	@$(DOCGEN) doxygen.conf > /dev/null
	@echo "Compile PDF documentation."
	@make -C $(DIR_BIN)/api/latex > /dev/null 2>&1

# target to make static code analyze
.PHONY: codeanalyze
codeanalyze: $(HEADERS)
	@echo "Run CPPCHECK."
	$(CPPCHCK) $(CPFLAGS) -I $(DIR_INC) $^
	@echo "Run SPLINT."
	$(SPLINT) $(SPFLAGS) -I $(DIR_INC) $^

# target to clean the entire project
.PHONY: clean
clean:
	@echo "Clean the entire project."
	@rm -rf $(DIR_BIN)/*
